#define _CRT_SECURE_NO_WARNINGS 1
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <cmath>
using namespace std;

void ParameterParsing()
{
    vector<string> sarr;
    string line;
    getline(cin, line);
    for (int i = 0; i < line.size(); i++)
    {
        string temp;
        if (line[i] == '"')
        {
            i++;
            while (line[i] != '"')
            {
                temp.push_back(line[i++]);
            }
            i++;
        }
        else
        {
            while (i < line.size() && line[i] != ' ')
            {
                temp.push_back(line[i++]);
            }
        }
        sarr.push_back(temp);
    }

    cout << sarr.size() << endl;
    for (auto it : sarr)
    {
        cout << it << endl;
    }
}

vector<int> GetNum(int n, int m)
{
    vector<int> tempArr;
    for (int i = 2; i <= sqrt(n); i++)
    {
        if (n % i == 0 && n + i <= m)
        {
            tempArr.push_back(i);
            if (n / i != i && n + n / i <= m)
                tempArr.push_back(n / i);
        }
    }
    return tempArr;
}

void StoneJumping()
{
    int N, M;
    cin >> N >> M;
    vector<int> varr;
    varr.resize(M + 1, INT_MAX);
    varr[N] = 0;
    for (int i = N; i < M; i++)
    {
        if (varr[i] != INT_MAX)
        {
            vector<int> tempArr = GetNum(i, M);
            for (auto it : tempArr)
            {
                varr[i + it] = min(varr[i + it], varr[i] + 1);
            }
        }
    }
    varr[M] = (varr[M] == INT_MAX) ? -1 : varr[M];
    cout << varr[M] << endl;
}

//int tar = 0;
//
//int Judge(vector<int>& v)
//{
//    int num = 0, product = 1;
//    for (auto it : v)
//    {
//        num += it;
//        product *= it;
//    }
//    // if(num > product)
//    //     cout << num << " " << product << endl;
//    return num > product;
//}
//
//void Work(vector<int>& v, int n, int index, vector<int>& temp)
//{
//    if (v.size() == index && n == 0)
//    {
//        for (auto it : temp)
//        {
//            cout << it << " ";
//        }
//        cout << endl;
//        tar += Judge(temp);
//        return;
//    }
//    // 0 : 移除
//    // 1 : 保留
//    for (int i = 0; i < 2; i++)
//    {
//        if (i == 0)
//        {
//            if (n == 0) continue;
//            Work(v, n - 1, index + 1, temp);
//        }
//        else if(n < v.size() - index)
//        {
//            temp.push_back(v[index]);
//            Work(v, n, index + 1, temp);
//            temp.resize(temp.size() - 1);
//        }
//    }
//}
//
//void BugLuck()
//{
//    int n = 0;
//    vector<int> varr;
//    cin >> n;
//    varr.resize(n);
//    for (int i = 0; i < n; i++)
//        cin >> varr[i];
//    for (int i = 0; i < varr.size(); i++)
//    {
//        vector<int> temp;
//        cout << "--" << i << "--" << endl;
//        Work(varr, i, 0, temp);
//    }
//    cout << tar << endl;
//}

int monthArr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };

// 闰年2月份29天，平年28天
void Judge(int year)
{
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
    {
        monthArr[2] = 29;
    }
    else
    {
        monthArr[2] = 28;
    }
}

int Work(int month, int day)
{
    int count = 0;
    for (int i = 1; i < month; i++)
    {
        count += monthArr[i];
    }
    count += day;
    return count;
}

//int main()
//{
//    int year, month, day;
//    cin >> year >> month >> day;
//    Judge(year);
//    cout << Work(month, day) << endl;
//
//    return 0;
//}

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// getLuckyPacket:从当前位置开始搜索符合要求的组合，一直搜索到最后一个位置结束
// pos：当前搜索的位置
// sum：到目前为止的累加和
// muti：到目前为止的累积值

int getLuckyPacket(vector<int>& v, int pos, int sum, int multi)
{
    int count = 0;

    for (int i = pos; i < v.size(); i++)
    {
        sum += v[i];
        multi *= v[i];
        if (sum > multi)
        {
            // 找到符合要求的组合，加1，继续累加后序的值，看是否符合要求
            count += 1 + getLuckyPacket(v, pos + 1, sum, multi);
        }
        else if (v[i] == 1)
        {
            // 如果不符合要求，且当前元素为1，继续向下走
            count += 1 + getLuckyPacket(v, pos + 1, sum, multi);
        }
        else
        {
            // 如果sum 大于 mutil后面的值就不需要再继续算了
            break;
        }

        // 要搜索下一个位置之前，首先恢复sum和mutil
        sum -= v[i];
        multi /= v[i];

        // 数字相同的球没有区别直接跳过
        while (i < v.size() - 1 && v[i] == v[i + 1]) i++;
    }

    return count;
}

void BugLuck()
{
    int n = 0;
    vector<int> v;
    cin >> n;
    v.resize(n);
    for (int i = 0; i < n; i++)
    {
        cin >> v[i];
    }
    sort(v.begin(), v.end());
    cout << getLuckyPacket(v, 0, 0, 1) - 1 << endl;
}

void Func(char str[100])
{
    printf("%d\n", sizeof(str));
}

void test()
{
    /*int i = 5, k = 0;
    k = (++i) + (++i) + (i++);
    printf("%d", k);*/
    unsigned char* p1;
    unsigned long* p2;
    p1 = (unsigned char*)0x801000;
    p2 = (unsigned long*)0x810000;
    printf("%x\n", p1 + 5);
    printf("%x\n", p2 + 5);
}

int main()
{

    /*char str[] = "Hello";
    printf("%d\n", sizeof(str));
    printf("%d\n", strlen(str));
    char* p = str;
    printf("%d\n", sizeof(p));
    Func(str);*/
    test();

    return 0;
}

//int main()
//{
//    BugLuck();
//
//    return 0;
//}
// 64 位输出请用 printf("%lld")

//int main()
//{
//	//cout << "\\\\" << endl;
//	//cout << "c:\\\\" << endl;
//    //BugLuck();
//
//	return 0;
//}