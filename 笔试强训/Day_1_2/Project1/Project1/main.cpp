#define _CRT_SECURE_NO_WARNINGS 1
#include <algorithm>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

// 100449-组队竞赛
// 方法1：
//long long Work()
//{
//    int n, a_i;
//    cin >> n;
//    int* arr = new int[n * 3];
//    for (int i = 0; i < n * 3; i++)
//    {
//        cin >> arr[i];
//    }
//    sort(arr, arr + (n * 3));
//    long long max = 0;
//    int left = 0, right = n - 1;
//    while (left < right)
//    {
//        right--;
//        max += arr[right];
//        right--;
//        left++;
//    }
//    return max;
//}
//
//// 方法2：
//long long Work1()
//{
//    long long max = 0;
//    int n = 0;
//    cin >> n;
//    vector<int> v;
//    v.resize(n * 3);
//    for (int i = 0; i < n * 3; i++)
//    {
//        cin >> v[i];
//    }
//    for (int i = 0; i < n; i++)
//    {
//        max += v[v.size() - 2 * (i + 1)];
//    }
//    return max;
//}
//
//int main() {
//    
//    cout << Work1() << endl;
//
//    return 0;
//}

// 删除公共字符

//string Work(string& sin, string& scmp)
//{
//    string temp;
//    int hashtable[300] = { 0 };
//    for (auto it : scmp)
//    {
//        hashtable[it]++;
//    }
//    for (auto it : sin)
//    {
//        if (hashtable[it] != 1)
//            temp.push_back(it);
//    }
//    return temp;
//}
//
//int main() {
//    string sin, scmp;
//    // 注意不能使用cin接收，遇到空格就结束了
//    getline(cin, sin);
//    getline(cin, scmp);
//    cout << Work(sin, scmp) << endl;
//
//    return 0;
//}

//void Work(string& str, string& tar)
//{
//    int pos = str.size();
//    while (true)
//    {
//        int temp = str.find_last_of(' ', pos);
//        if (temp == string::npos)
//        {
//            tar.append(str.begin(), str.begin() + pos);
//            break;
//        }
//        else
//        {
//            if(pos < str.size() && str[pos+1] == ' ') pos++;
//            tar.append(str.begin() + temp + 1, str.begin() + pos);
//            tar += " ";
//            pos = temp - 1;
//        }
//    }
//}
//
//int main() {
//    string str;
//    getline(cin, str);
//
//    string tar;
//    Work(str, tar);
//    cout << tar << endl;
//
//    return 0;
//}

//int main()
//{
//    string s1, s2;
//    cin >> s1;
//    while (cin >> s2)
//    {
//        s1 = s2 + " " + s1;
//    }
//    cout << s1 << endl;
//    return 0;
//}

//#define log 1000000007
//int num = 0;
//
//int Change(char x, char y)
//{
//    return (x - '0') * 10 + (y - '0');
//}
//
//void Work(string& s, int index, int n)
//{
//    if (index == n)
//    {
//        num = num % log + 1;
//        return;
//    }
//    if (s[index] == '0') return;
//    for (int i = 1; i <= 2; i++)
//    {
//        if (i == 1)
//            Work(s, index + i, n);
//        if ((i == 2 && index + 1 < n) && Change(s[index], s[index + 1]) <= 26)
//            Work(s, index + i, n);
//    }
//}
//
//int main()
//{
//    int n = 0;
//    cin >> n;
//    string s;
//    cin >> s;
//    Work(s, 0, n);
//    cout << num << endl;
//
//    return 0;
//}

#define ADD 1
#define SUB 2
#define SAM 3

int Work(vector<int>& v)
{
    if (v.size() == 1 || v.size() == 2) return 1;
    int flag = 0;
    int num = 0;
    for (int i = 0; i < v.size(); i++)
    {
        if (v[i + 1] == 0) break;
        if (flag == 0)
        {
            num++;
            flag = (v[i] > v[i + 1]) ? SUB : ADD;
            flag = (v[i] == v[i + 1]) ? SAM : flag;
        }
        else if (v[i] != v[i + 1] && flag == SAM)
        {
            flag = (v[i] > v[i + 1]) ? SUB : ADD;
        }
        else if ((v[i] > v[i + 1] && flag == ADD) || (v[i] < v[i + 1] && flag == SUB))
        {
            flag = 0;
        }
    }
    if (flag == 0) num++;
    return num;
}

int main() {
    int n = 0;
    cin >> n;
    vector<int> v;
    v.resize(n);
    for (int i = 0; i < n; i++)
    {
        cin >> v[i];
    }
    v.push_back(0);
    cout << Work(v) << endl;
    return 0;
}