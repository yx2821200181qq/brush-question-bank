#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <stack>
#include <vector>
#include <string>
using namespace std;

// 求最大公约数
int gcd(int a, int b)
{
    int r;
    while (r = a % b) // 碾转相除法
    {
        a = b;
        b = r;
    }
    return b;
}

void MinCommon()
{
    int a, b;
    cin >> a >> b;
    // 最小公倍数 = 两数之积除以最大公约数
    cout << a * b / gcd(a, b) << endl;
}

bool Judge_LengthSort(vector<string>& vstr)
{
    int len = vstr[0].size();
    for (int i = 1; i < vstr.size(); i++)
    {
        if (len > vstr[i].size()) return false;
        len = vstr[i].size();
    }
    return true;
}

bool Judge_dictSort(vector<string>& vstr)
{
    string len = vstr[0];
    for (int i = 1; i < vstr.size(); i++)
    {
        if (len > vstr[i]) return false;
        len = vstr[i];
    }
    return true;
}

string TowSort()
{
    int N = 0;
    cin >> N;
    vector<string> vstr;
    vstr.resize(N);
    bool flag[2] = { false };
    for (int i = 0; i < N; i++)
        cin >> vstr[i];
    flag[0] = Judge_LengthSort(vstr);
    flag[1] = Judge_dictSort(vstr);
    if (flag[0] == flag[1])
    {
        if (flag[0]) return "both";
        else return "none";
    }
    if (flag[0]) return "lengths";
    return "lexicographically";
}

bool chkParenthesis(string A, int n) {
    // write code here
    stack<char> st;
    for (auto it : A)
    {
        if (it == '(') st.push(it);
        else if (it == ')')
        {
            if (st.empty()) return false;
            st.pop();
        }
        else return false;
    }
    return st.empty();
}

int FibncArr()
{
    int N = 0;
    cin >> N;
    int a1 = 0;
    int a2 = 1;
    if (N == a1 || N == a2) return 0;
    while (a2 < N)
    {
        int temp = a2;
        a2 = a1 + a2;
        a1 = temp;
    }
    return min(a2 - N, N - a1);
}

int main()
{


	return 0;
}