#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
using namespace std;

const char flag[2] = { '(',')' };
int Left = 0, Right = 0;

void Work(int n, string& str, int index)
{
	if (index == n * 2)
	{
		cout << str << endl;
		return;
	}
	for (int i = 0; i < 2; i++)
	{
		if (i == 0)
		{
			if (Left < n)
			{
				str[index] = flag[i];
				Left++;
				Work(n, str, index+1);
				Left--;
			}
		}
		else
		{
			if (Right < n && Left > Right)
			{
				str[index] = flag[i];
				Right++;
				Work(n, str, index+1);
				Right--;
			}
		}
	}
}

int main()
{
	int n = 0;
	cin >> n;
	string str;
	str.resize(n * 2);
	Work(n, str, 0);

	return 0;
}

//void Swap(int& left, int& right) // Swap(int* const left, int* const right)
//{
//	int temp = left;
//	right;
//	right = temp;
//}
//
//int main()
//{
//	int a = 10;
//	int* p = nullptr;
//	Swap(a, *p);
//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int& b = a;
//	int c = 1;
//	b = c;
//	cout << b << " " << a << endl;
//	return 0;
//}