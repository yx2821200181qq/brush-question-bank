#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

// 测试C/C++下const修饰变量的区别
int main(void)
{
	const int a = 10;
	int* p = (int*)(&a);
	*p = 20;
	//cout << "a = " << a << ", *p = " << *p << endl;
	printf("a = %d, *p = %d\n", a, *p);
	return 0;
}