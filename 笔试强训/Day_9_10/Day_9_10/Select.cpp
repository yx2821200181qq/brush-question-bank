#define _CRT_SECURE_NO_WARNINGS 1
// 前缀树，存储的是由数个小写英文字母组成的字符串路径
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(void)
{
	const int a = 10;
	int* p = (int*)(&a);
	*p = 20;
	cout << "a = " << a << ", *p = " << *p << endl;
	return 0;
}

//int main()
//{
//	int** a = (int**)malloc(4 * 4);
//	for (int i = 0; i < 4; i++)
//	{
//		a[i] = nullptr;
//	}
//	int* arr = (int*)malloc(4 * 4);
//	for (int i = 0; i < 4; i++)
//	{
//		arr[i] = i;
//	}
//
//	return 0;
//}

//class A
//{
//public:
//	static void Print()
//	{
//		cout << _a << endl;
//	}
//private:
//	const static int _a = 2;
//};
//
//const int A::_a = 5;
//
//int main()
//{
//	A* a = nullptr;
//	a->Print();
//
//	return 0;
//}

//class TrieNode
//{
//public:
//    int pass;
//    int end;
//    //vector<TrieNode*> nexts;
//    TrieNode** nexts;
//    TrieNode()
//    {
//        pass = 0;
//        end = 0;
//        //nexts.resize(26);
//        nexts = (TrieNode**)malloc(4 * 26);
//        for (int i = 0; i < 26; i++)
//        {
//            nexts[i] = nullptr;
//        }
//    }
//    ~TrieNode()
//    {
//        pass = 0;
//        end = 0;
//    }
//};
//
//class Trie
//{
//public:
//    Trie() {}
//    //插入一个字符串的路径
//    bool insert(std::string word)
//    {
//        char cArr[1024] = { 0 };
//        sprintf(cArr, word.c_str(), word.size());
//        // 根节点的pass++
//        TrieNode* node = &root;
//        (*node).pass++;
//
//        // 表示字母的下标索引，例如a就是0，z就是25
//        int index;
//        // 计算字符索引，并将其pass++
//        for (int i = 0; i < word.size(); i++)
//        {
//            //小写字母的ascii码-a的ascii码
//            index = cArr[i] - 'a';
//            if ((*node).nexts[index] == nullptr) {
//                (*node).nexts[index] = new TrieNode();
//            }
//            node = (*node).nexts[index];
//            (*node).pass++;
//        }
//
//        // 最后一个字符所代表的节点的end++
//        root.end++;
//        return true;
//    }
//    //查找一个字符串的路径在前缀树中插入了几次
//    int search(string word) {
//        char cArr[1024] = { 0 };
//        sprintf(cArr, word.c_str(), word.size());
//        TrieNode node = root;
//        int index = -1;
//        for (int i = 0; i < word.size(); i++) {
//            index = cArr[i] - 'a';
//            if (node.nexts[index] == nullptr) {
//                return 0;
//            }
//            cout << cArr[i];
//            node = *node.nexts[index];
//        }
//        return root.end;
//    }
//    int Delete(string word)
//    {
//        char cArr[1024] = { 0 };
//        sprintf(cArr, word.c_str(), word.size());
//        TrieNode* node = &root;
//        //
//        vector<TrieNode*> vAddress;
//        int index = -1;
//        for (int i = 0; i < word.size(); i++) 
//        {
//            //拿出字符的下标
//            index = cArr[i] - 'a';
//            //没有找到字符串路径
//            if ((*node).nexts[index] == nullptr) 
//            {
//                return 0;
//            }
//            //(new)
//            //存储pass值为0的字符节点
//            vAddress.push_back((*node).nexts[index]);
//            //pass不为0，跳转到下一个字符节点进行处理
//            node = (*node).nexts[index];
//        }
//        for (int i = vAddress.size() - 1; i >= 0; i--)
//        {
//            //取出要析构的字符节点指针
//            TrieNode* node = vAddress[i];
//            //?
//            //for (int i = 0; i < 26; i++)
//            //{
//            //    if ((*node).nexts[i] != nullptr) return 1;
//            //}
//            delete node;
//            //？
//            index = cArr[i] - 'a';
//            //不是最后一个要析构的字符节点
//            if (i != 0) 
//            {
//                //当前要析构的路径上的字符结点的父节点的指针指向的
//                vAddress[i - 1]->nexts[index] = nullptr;
//            }
//            else root.nexts[index] = nullptr;
//        }
//        return 0;
//    }
//private:
//    TrieNode root;
//};
//
//int main() {
//    Trie tn;
//    tn.insert("abcd");
//    tn.insert("ertfrfrf");
//
//
//    cout << tn.Delete("abcd") << endl;
//    cout << tn.Delete("ertfrfrf") << endl;
//
//    return 0;
//}