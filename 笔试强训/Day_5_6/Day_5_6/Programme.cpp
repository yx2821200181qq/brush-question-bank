#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;

class Solution {
public:
    int StrToInt(string str) {
        if (str.empty()) return 0;
        int flag = 0;
        if (str[0] == '-') flag = -1;
        if (str[0] == '+') flag = 1;
        int number = 0;
        for (int i = 0; i < str.size(); i++)
        {
            if (i == 0 && flag != 0) continue;
            if (str[i] < '0' || str[i] > '9') return 0;
            number = number * 10 + str[i] - '0';
        }
        if (flag != 0) number *= flag;
        return number;
    }
};

void MaxSum()
{
    int n, tar = INT_MIN;
    vector<int> varr;
    cin >> n;
    varr.resize(n);
    for (int i = 0; i < n; i++)
        cin >> varr[i];
    int num = varr[0];
    for (int i = 0; i < n; i++)
    {
        if (i != 0) num += varr[i];
        if (num < varr[i]) num = varr[i];
        if (num > tar) tar = num;
    }
    cout << tar << endl;
}

void Work1()
{
    int count = 0;
    string sa, sb;
    cin >> sa >> sb;
    for (int i = 0; i <= sa.size(); i++)
    {
        string temp1, temp2;
        for (int j = 0; j < i; j++)
        {
            temp1 += sa[j];
        }
        temp1 += sb;
        for (int j = i; j < sa.size(); j++)
        {
            temp1 += sa[j];
        }
        temp2 = temp1;
        reverse(temp1.begin(), temp1.end());
        // cout << temp1 << endl;
        // cout << temp2 << endl;
        if (temp1 == temp2) count++;
    }
    cout << count << endl;
}

bool IsCircleText(const string& s)
{
    size_t begin = 0;
    size_t end = s.size() - 1;
    while (begin < end)
    {
        if (s[begin] != s[end]) return false;
        begin++;
        end--;
    }
    return true;
}

void Work2()
{
    int count = 0;
    string sa, sb;
    cin >> sa >> sb;
    for (int i = 0; i <= sa.size(); i++)
    {
        string temp;
        for (int j = 0; j < i; j++)
        {
            temp += sa[j];
        }
        temp += sb;
        for (int j = i; j < sa.size(); j++)
        {
            temp += sa[j];
        }
        if (IsCircleText(temp)) count++;
    }
    cout << count << endl;
}

int main() {
    Work1();

    return 0;
}