#define _CRT_SECURE_NO_WARNINGS 1
// 假设一个固定窗口，大小为W，依次划过数组arr，返回每一次滑动后的窗口的最大值
// 例如 arr=[4,3,5,4,3,3,6,7],W=3
// 返回 [5,5,5,4,6,7]
#include <deque>
#include <vector>
#include <string>
#include <iostream>
#include <ctime>
std::vector<int> getMaxWindow(const int* arr, int arrlength, int w)
{
    // 存放返回的结果
    std::vector<int> res;
    if (arr == nullptr || w < 1 || w > arrlength)
    {
        exit(EXIT_FAILURE);
    }
    // 双端队列deque,存放数组元素的下标
    // 且设定它的头部到尾部元素，所对应的arr内数组元素大小是从大到小的
    std::deque<int> qmax;
    for (int R = 0; R < arrlength; R++)
    {
        // 弹出所有比新加入的值小或等于的数
        while (!qmax.empty() && qmax.back() <= arr[R])
        {
            qmax.pop_back();
        }
        // 将新的值插入到队列尾部
        qmax.push_back(R);
        // 从队列中弹出因窗口变化而被移出的值，因为窗口大小固定，所以R-w为这个值的下标
        if (qmax.front() == R - w)
        {
            qmax.pop_front();
        }
        // 将窗口变化后，窗口内的最大值加入到res中
        // 队头的元素对应arr数组内元素的值是窗口内最大的
        // 当元素的值大于等于窗口的大小w时，才开始记录窗口内最大值
        if (R >= w - 1)
        {
            res.push_back(qmax.front());
        }
    }
    return res;
}
// 获取控制台输入的数组元素，存入arr,返回数组arr的元素个数
int getArrFromStdin(int* arr)
{
    // 接收getchar()从stdin获取的字符
    char c;
    // 数组下标
    int i = 0;
    // 接收从stdin中输入的数字组合
    std::string ss = "";
    // 判断获取到的字符是否为回车,若为回车则结束
    while ((c = getchar()) != '\n')
    {
        // 是否为数字
        if (c < '0' || c > '9')
        {
            if (!ss.empty())
            {
                arr[i++] = atoi(ss.c_str());
                ss.clear();
            }
            continue;
        }
        ss += c;
    }
    // 将最后一个数填进数组
    arr[i++] = atoi(ss.c_str());
    return i;
}
int main()
{
    // 数组arr
    int arr[1024] = { 0 };
    int arrLength = 0;
    arrLength = getArrFromStdin(arr);
    std::cout << arrLength << std::endl;
    for (int i = 0; i < arrLength; i++)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
    // auto vec = getMaxWindow(arr, arrLength, 3);
    // for(auto v:vec){
    //     std::cout<<v<<" ";
    // }
    return 0;
}

//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int arr[][3] = { 10,20,30,40,50,60 };
//	int (*ptr)[3] = arr;
//
//	//char arr[] = "wang\0miao";
//	//cout << strlen(arr) << endl;
//	//cout << sizeof(arr) << endl;
//
//	return 0;
//}