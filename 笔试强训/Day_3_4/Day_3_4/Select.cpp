#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

#include <stdio.h>
int cnt = 0;
int fib(int n) {
	cnt++;
	if (n == 0)
		return 1;
	else if (n == 1)
		return 2;
	else
		return fib(n - 1) + fib(n - 2);
}
void main() {
	for (int i = 0; i < 10; i++)
	{
		cout << i << " : " << fib(i) << endl;
	}
	printf("%d", cnt);
}

//int main()
//{
//	int a[] = { 1, 2, 3, 4 };
//	int* b = a;
//	*b += 2;
//	*(b + 2) = 2;
//	b++;
//	printf("%d,%d\n", *b, *(b + 2));
//
//	return 0;
//}