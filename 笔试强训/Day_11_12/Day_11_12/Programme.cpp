#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

bool ISRight(int a)
{
    for (int i = 2; i * i <= a; i++)
    {
        if (a % i == 0) return false;
    }
    return true;
}

void Work()
{
    int a = 0;
    cin >> a;
    for (int i = a / 2; i >= 1; i--)
    {
        if (ISRight(i) && ISRight(a - i))
        {
            cout << i << endl;
            cout << a - i << endl;
            return;
        }
    }
}

class LCA {
public:
    int getLCA(int a, int b) {
        // write code here
        while (a != b)
        {
            if (a > b)
            {
                a /= 2;
            }
            else
            {
                b /= 2;
            }
        }
        return a;
    }
};

int binInsert(int n, int m, int j, int i) {
    // m����jλ
    m <<= j;
    return n | m;
}

void Work()
{
    int a;
    cin >> a;
    int max = 0, num = 0;
    // cout << (3 & 4) << endl;
    for (int i = 0; i < 32; i++)
    {
        // cout << (1 << i) << endl;
        if ((a & (1 << i)) != 0) num++;
        else num = 0;
        if (num > max) max = num;
    }
    cout << max << endl;
}