#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
#include<vector>
using namespace std;

class Node {
public:
    int val;
    Node* next;
    Node* random;

    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};

class Solution {
public:
    Node* copyRandomList(Node* head) {
        map<Node*, Node*> Nodekv;
        Node* cur = head;
        Node* tail = nullptr;
        while (cur)
        {
            Node* temp = new Node(cur->val);
            if (tail != nullptr) tail->next = temp;
            tail = temp;
            Nodekv.insert(make_pair(cur, tail));
            cur = cur->next;
        }
        for (auto it : Nodekv)
        {
            if (!it.first->random)
            {
                it.second->random = nullptr;
            }
            else
            {
                it.second->random = Nodekv[it.first->random];
            }
        }
        return Nodekv[head];
    }
};

//int pivotInteger(int n) {
//    if (n == 1) return 1;
//    int begin = 1;
//    int end = n;
//    while (begin < end)
//    {
//        int mid = begin + ((end - begin) >> 1);
//        int leftNum = mid + mid * (mid - 1) / 2;
//        int rightNum = (n - mid + 1) * mid + (n - mid + 1) * ((n - mid + 1) - 1) / 2;
//        if (leftNum == rightNum) return mid;
//        else if (leftNum < rightNum) begin = mid + 1;
//        else end = mid;
//    }
//    return -1;
//}

int Sum(int a, int n)
{
    return a * (n - a + 1) + (n - a + 1) * ((n - a)) / 2;
}
int pivotInteger(int n) {
    if (n == 1) return 1;
    int begin = 1;
    int end = n;
    while (begin < end)
    {
        int mid = begin + ((end - begin) >> 1);
        int leftNum = Sum(1, mid);
        int rightNum = Sum(mid, n);
        if (leftNum == rightNum) return mid;
        else if (leftNum < rightNum) begin = mid + 1;
        else end = mid;
    }
    return -1;
}

int searchInsert(vector<int>& nums, int target) {
    int left = 0, right = nums.size();
    while (left < right)
    {
        int mid = left + ((right - left) >> 1);
        if (nums[mid] == target) return mid;
        else if (nums[mid] > target) right = mid;
        else left = mid + 1;
    }
    return left;
}

int main()
{
    cout << pivotInteger(4) << endl;

	return 0;
}