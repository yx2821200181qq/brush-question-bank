#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<queue>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
 

vector<vector<int>> levelOrder(TreeNode* root) {
    vector<vector<int>> vv;
    queue<TreeNode*> q;
    if (!root) return vv;
    q.push(root);
    while (!q.empty())
    {
        vector<int> v;
        int size = q.size();
        for (int i = 0; i < size; i++)
        {
            TreeNode* temp = q.front();
            q.pop();
            v.push_back(temp->val);
            if (temp->left) q.push(temp->left);
            if (temp->right) q.push(temp->right);
        }
        vv.push_back(v);
    }
    return vv;
}