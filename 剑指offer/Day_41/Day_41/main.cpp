#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<set>
using namespace std;

typedef pair<string, int> PSI;
class compare {
public:
    bool operator()(const PSI& kv1, const PSI& kv2)
    {
        return kv1.second > kv2.second || (kv1.second == kv2.second && kv1.first < kv2.first);
    }
};
class Solution {
public:
    vector<string> topKFrequent(vector<string>& words, int k) {
        priority_queue<PSI, vector<PSI>, compare> pq;
        map<string, int> wordMap;
        vector<string> v(k, string());

        for (auto it : words)
        {
            wordMap[it]++;
        }
        for (auto it : wordMap)
        {
            cout << it.first << ":" << it.second << endl;
        }
        for (auto it : wordMap)
        {
            if (pq.size() != k)
            {
                pq.push(it);
            }
            else
            {
                if (wordMap[pq.top().first] < wordMap[it.first])
                {
                    pq.pop();
                    pq.push(it);
                }
            }
        }
        int index = k - 1;
        while (!pq.empty())
        {
            v[index--] = pq.top().first;
            pq.pop();
        }

        return v;
    }
};

typedef pair<int, int> PII;
class compare {
public:
    bool operator()(const PII& kv1, const PII& kv2)
    {
        return kv1.second > kv2.second || (kv1.second == kv2.second && kv1.first < kv2.first);
    }
};
class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        map<int, int> countmap;
        priority_queue<PII, vector<PII>, compare> pq;
        vector<int> v(k, int());
        for (auto it : nums)
        {
            countmap[it]++;
        }
        for (auto it : countmap)
        {
            if (pq.size() < k)
            {
                pq.push(make_pair(it.first, it.second));
            }
            else
            {
                if (pq.top().second < it.second)
                {
                    pq.pop();
                    pq.push(make_pair(it.first, it.second));
                }
            }
        }
        int index = k - 1;
        while (pq.size() != 0)
        {
            v[pq.size() - 1] = pq.top().first;
            pq.pop();
        }
        return v;
    }
};

vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
    set<int> num1set;
    set<int> num2set;
    vector<int> v;
    for (auto it : nums1)
    {
        num1set.insert(it);
    }
    for (auto it : nums2)
    {
        num2set.insert(it);
    }
    for (auto it : num2set)
    {
        if (num1set.find(it) != num1set.end())
        {
            v.push_back(it);
        }
    }
    return v;
}

vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
    set<int> num1set;
    set<int> num2set;
    for (auto it : nums1)
    {
        num1set.insert(it);
    }
    for (auto it : nums2)
    {
        if (num1set.find(it) != num1set.end()) num2set.insert(it);
    }
    vector<int> v(num2set.begin(), num2set.end());
    return v;
}

vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
    vector<int> v;
    sort(nums1.begin(), nums1.end());
    auto it1 = unique(nums1.begin(), nums1.end());
    nums1.resize(distance(nums1.begin(), it1));
    sort(nums2.begin(), nums2.end());
    auto it2 = unique(nums2.begin(), nums2.end());
    nums2.resize(distance(nums2.begin(), it2));
    int index1 = 0, index2 = 0;
    cout << nums1.size() << ":" << nums2.size() << endl;
    while (index1 != nums1.size() && index2 != nums2.size())
    {
        if (nums1[index1] == nums2[index2])
        {
            v.push_back(nums1[index1]);
            index1++;
            index2++;
        }
        else if (nums1[index1] < nums2[index2]) index1++;
        else index2++;
    }
    return v;
}

int main()
{

}