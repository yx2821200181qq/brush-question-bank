#define _CRT_SECURE_NO_WARNINGS 1
#include<queue>
#include<vector>
#include<iostream>
#include<functional>
using namespace std;

class KthLargest {
private:
    priority_queue<int, vector<int>, greater<int>> pqueue;
    int key;
public:
    KthLargest(int k, vector<int>& nums) {
        key = k;
        for (int i = 0; i < nums.size(); i++)
        {
            if (pqueue.size() < k) pqueue.push(nums[i]);
            else if (pqueue.top() < nums[i])
            {
                pqueue.pop();
                pqueue.push(nums[i]);
            }
        }
    }

    int add(int val) {
        if (pqueue.size() < key) pqueue.push(val);
        else if (pqueue.top() < val)
        {
            pqueue.pop();
            pqueue.push(val);
        }
        return pqueue.top();
    }
};

int main()
{

	return 0;
}