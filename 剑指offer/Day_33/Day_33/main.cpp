#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

ListNode* reverseList(ListNode* head)
{
    ListNode* guard = new ListNode(0);
    ListNode* cur = head;
    while (cur)
    {
        ListNode* temp = cur;
        cur = cur->next;
        if (!guard->next)
        {

            guard->next = temp;
            temp->next = nullptr;
        }
        else
        {
            temp->next = guard->next;
            guard->next = temp;
        }
    }
    return guard->next;
}
bool isPalindrome(ListNode* head) {
    ListNode* left = head, * right = head;
    while (right && right->next)
    {
        left = left->next;
        if (!left->next)
            return head->val == left->val;
        right = right->next->next;
    }
    if (!right) right = reverseList(left);
    else right = reverseList(left->next);
    left = head;
    while (right)
    {
        if (left->val != right->val) return false;
        left = left->next;
        right = right->next;
    }
    return true;
}

class Node {
public:
    int val;
    Node* next;
    Node() {}

    Node(int _val) {
        val = _val;
        next = NULL;
    }

    Node(int _val, Node* _next) {
        val = _val;
        next = _next;
    }
};

Node* insert(Node* head, int insertVal) {
    Node* tar = new Node(insertVal);
    if (!head)
    {
        tar->next = tar;
        return tar;
    }
    if (head == head->next)
    {
        tar->next = head->next;
        head->next = tar;
        return head;
    }
    Node* cur = head;
    while (true)
    {
        int num1 = cur->val, num2 = cur->next->val;
        if ((num1 < insertVal && num2 > insertVal) || (num1 > num2 && (insertVal > num1 || num2 > insertVal)) || cur->next == head || num2 == insertVal)
        {
            tar->next = cur->next;
            cur->next = tar;
            return head;
        }
        cur = cur->next;
    }

    return head;
}

//class Node {
//public:
//    int val;
//    Node* prev;
//    Node* next;
//    Node* child;
//};
//
//Node* dfsNode(Node* last, Node* tail)
//{
//    while (tail->next != nullptr || tail->child)
//    {
//        if (tail->child)
//        {
//            Node* temp = tail->next;
//            last = tail;
//            tail = tail->child;
//            last->next = tail;
//            tail->prev = last;
//            last->child = nullptr;
//            tail = dfsNode(last, tail);
//            tail->next = temp;
//            if (temp) temp->prev = tail;
//        }
//        last = tail;
//        if (tail->next) tail = tail->next;
//    }
//    return tail;
//}
//Node* flatten(Node* head) {
//    if (head) dfsNode(head, head);
//    return head;
//}

//Node* dfsNode(Node* last, Node* tail)
//{
//    while (tail->next != nullptr)
//    {
//        if (tail->child)
//        {
//            Node* temp = tail->next;
//            last = tail;
//            tail = tail->child;
//            last->next = tail;
//            tail->prev = last;
//            last->child = nullptr;
//            tail = dfsNode(last, tail);
//            tail->next = temp;
//            temp->prev = tail;
//        }
//        last = tail;
//        tail = tail->next;
//    }
//    return tail;
//}
//Node* flatten(Node* head) {
//    if (head) dfsNode(head, head);
//    return head;
//}

class A {
    int _a;
    A* _next;
public:
    A(int a)
    {
        _a = a;
    }
    A(int a, A* next)
    {
        _a = a;
        _next = next;
    }
};

int main()
{
    //Node* temp = new Node(2);
    A* temp = new A(22);
    A* temp1 = new A(22, temp);

	return 0;
}