#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

//删除链表中的结点——哨兵位
ListNode* deleteNode(ListNode* head, int val) {
    ListNode* Head = new ListNode(1);
    Head->next = head;
    ListNode* cur = head;
    ListNode* first = Head;
    while (cur)
    {
        if (cur->val == val)
        {
            first->next = cur->next;
            delete cur;
            break;
        }
        cur = cur->next;
        first = first->next;
    }
    cur = Head->next;
    delete Head;
    return cur;
}

//删除链表中的结点——无哨兵位
ListNode* deleteNode(ListNode* head, int val) {
    if (!head) return head;
    if (head->val == val) return head->next;
    ListNode* pre = head;
    ListNode* cur = head->next;
    while (cur)
    {
        if (cur->val == val)
        {
            pre->next = cur->next;
            break;
        }
        pre = cur;
        cur = cur->next;
    }
    return head;
}

//查找链表中倒数第K个结点
ListNode* getKthFromEnd(ListNode* head, int k) {
    if (head == nullptr) return head;
    ListNode* pre = head, * cur = head;
    while (k--) cur = cur->next;
    while (cur)
    {
        pre = pre->next;
        cur = cur->next;
    }
    return pre;
}

int Lenth(ListNode* l)
{
    int n = 0;
    while (l)
    {
        l = l->next;
        n++;
    }
    return n;
}
void AddL(ListNode* la, ListNode* lb, int n)
{
    int flag = 0;
    for (int i = 0; i < n; i++)
    {
        la->val = lb->val + la->val + flag;
        flag = la->val / 10;
        la->val = la->val % 10;
        if (la->next == nullptr && lb->next == nullptr && flag != 0)
        {
            ListNode* temp = new ListNode(flag);
            la->next = temp;
            return;
        }
        la = la->next;
        lb = lb->next;
    }
    while (la)
    {
        la->val = la->val + flag;
        flag = la->val / 10;
        la->val = la->val % 10;
        if (la->next == nullptr && flag != 0)
        {
            ListNode* temp = new ListNode(flag);
            la->next = temp;
            return;
        }
        la = la->next;
    }
}
ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    ListNode* cur1 = l1, * cur2 = l2;
    int len1 = Lenth(l1);
    int len2 = Lenth(l2);
    if (len1 > len2)
    {
        AddL(l1, l2, len2);
        return l1;
    }
    else
    {
        AddL(l2, l1, len1);
        return l2;
    }
}

//链表两数之和——递归算法
ListNode* Add(ListNode* l1, ListNode* l2, int flag)
{
    if (!l1 && !l2 && flag == 0)
        return nullptr;
    int a = 0, b = 0;
    if (l1)
    {
        a = l1->val;
        l1 = l1->next;
    }
    if (l2)
    {
        b = l2->val;
        l2 = l2->next;
    }
    ListNode* node = new ListNode((a + b + flag) % 10);
    node->next = Add(l1, l2, (a + b + flag) / 10);
    return node;
}
ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    return Add(l1, l2, 0);
}

//链表两数之和——创建新链表
ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    ListNode* head = nullptr, * tail = nullptr;
    if (!l1) return l2;
    if (!l2) return l1;
    int flag = 0;
    while (l1 || l2)
    {
        int temp = 0;
        if (l1)
        {
            temp += l1->val;
            l1 = l1->next;
        }
        if (l2)
        {
            temp += l2->val;
            l2 = l2->next;
        }
        temp += flag;
        if (!head) head = tail = new ListNode(temp % 10);
        else
        {
            tail->next = new ListNode(temp % 10);
            tail = tail->next;
        }
        flag = temp / 10;
    }
    if (flag != 0)
        tail->next = new ListNode(flag);
    return head;
}

int main()
{
    ListNode* head = new ListNode(-3);
    ListNode* temp = new ListNode(5);
    head->next = temp;
    ListNode* temp1 = new ListNode(-99);
    temp->next = temp1;
    deleteNode(head, -3);

	return 0;
}