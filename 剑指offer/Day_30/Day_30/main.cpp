#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;



vector<int> countBits(int n) {
    vector<int> v(n + 1, 0);
    v[0] = 0;
    if (n == 0) return v;
    v[1] = 1;
    if (n == 1) return v;
    v[2] = 1;
    if (n == 2) return v;
    int flag = 2;
    for (int i = 3; i <= n; i++)
    {
        if (flag * 2 == i)
        {
            v[i] = 1;
            flag = flag * 2;
        }
        else
        {
            int ind = i - flag;
            v[i] = v[ind] + 1;
        }
    }

    return v;
}

int main()
{


    return 0;
}