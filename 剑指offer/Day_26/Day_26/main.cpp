#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

//剑指 Offer 14- I. 剪绳子——暴力枚举
int Max = 0;
int Work(int n, int num,int i)
{
    if (n == 1) return 1;
    for (; i <= n; i++)
    {
        num *= i;
        Work(n - i, num, i);
        if (num > Max) Max = num;
        num /= i;
    }
    return num;
}
int cuttingRope1(int n) {
    if (n <= 3) return n-1;
    Work(n, 1, 1);
    return Max;
}

//剑指 Offer 14- I. 剪绳子——动态规划
//int cuttingRope(int n) {
//    if (n <= 3) return n - 1;
//    vector<int> f(n + 1, 0);
//    f[1] = 1, f[2] = 2, f[3] = 3;
//    for (int i = 4; i <= n; i++)
//    {
//        for (int j = 1; j <= i / 2; j++)
//        {
//            f[i] = max(f[i], f[j] * f[i - j]);
//        }
//    }
//    return f[n];
//}

long long cuttingRope(int n) {
    if (n <= 3) return n - 1;
    vector<long long> f(n + 1, 0);
    f[1] = 1, f[2] = 2, f[3] = 3;
    for (int i = 4; i <= n; i++)
    {
        for (int j = 1; j <= i / 2; j++)
        {
            f[i] = max(f[i], f[j] * f[i - j]);
        }
    }
    return f[n];
}

vector<int> Work(int left, int right)
{
    vector<int> v;
    for (int i = left; i <= right; i++)
    {
        v.push_back(i);
    }
    return v;
}

//剑指 Offer 57 - II. 和为s的连续正数序列
vector<vector<int>> findContinuousSequence(int target) {
    vector<vector<int>> vv;
    vector<int> v;
    v.push_back(0);
    int num = 0;
    for (int i = 1; i <= target / 2 + target % 2; i++)
    {
        num += i;
        v.push_back(num);
    }
    for (int i = v.size() - 1; v[i] >= target; i--)
    {
        if (v[i] == target)
            vv.push_back(Work(1, i));
        else
        {
            for (int j = i; v[i] - v[j - 1] <= target; j--)
            {
                if (v[i] - v[j - 1] == target)
                {
                    vv.push_back(Work(j, i));
                }
            }
        }
    }
    reverse(vv.begin(), vv.end());
    return vv;
}

//剑指 Offer 57 - II. 和为s的连续正数序列
vector<vector<int>> findContinuousSequence(int target) {
    vector<vector<int>> vv;
    int Num = 0, flag = 1;
    for (int i = 1; i < target; i++)
    {
        while (Num < target)
        {
            Num += flag;
            flag++;
        }
        if (Num == target)
        {
            vector<int> v;
            for (int j = i; j < flag; j++)
            {
                v.push_back(j);
            }
            vv.push_back(v);
        }
        Num -= i;
    }
    return vv;
}

//剑指 Offer 58 - I. 翻转单词顺序
void Work(string& tar, string& s, int left, int right)
{
    for (int i = left; i <= right; i++)
    {
        tar += s[i];
    }
    tar += ' ';
}
string reverseWords(string s) {
    string tar;
    int left, right;
    bool flag = false;
    for (int i = s.size() - 1; i >= 0; i--)
    {
        if (!flag && s[i] != ' ')
        {
            right = i;
            flag = true;
        }
        if (flag && s[i] == ' ')
        {
            Work(tar, s, i + 1, right);
            flag = false;
        }
        if (i == 0 && s[i] != ' ')
            Work(tar, s, i, right);
    }
    tar.pop_back();
    return tar;
}

//剑指 Offer 25. 合并两个排序的链表
struct ListNode {
    int val;
    ListNode* next;
    ListNode(int x) : val(x), next(NULL) {}
};
ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
    if (!l1) return l2;
    if (!l2) return l1;
    ListNode Head(0);
    ListNode* cur = &Head;
    while (l1 && l2)
    {
        if (l1->val < l2->val)
        {
            cur->next = l1;
            l1 = l1->next;
        }
        else
        {
            cur->next = l2;
            l2 = l2->next;
        }
        cur = cur->next;
    }
    if (!l1) cur->next = l2;
    else cur->next = l1;
    return Head.next;
}

int main()
{
    findContinuousSequence(9);
    //vector<long long> v(110, 0);
    //for (int i = 2; i <= 100; i++)
    //{
    //    v[i] = cuttingRope(i);
    //    cout << "[" << i << "]:\t" << v[i] << "\n";
    //}
    //cout << endl;
	return 0;
}