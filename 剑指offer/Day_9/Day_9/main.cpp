#define _CRT_SECURE_NO_WARNINGS 1
#include<vector>
#include<queue>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

//二叉树的层序遍历—-队列
vector<int> levelOrder1(TreeNode* root) {
    queue<TreeNode*> q;
    vector<int> tar;
    if (!root)
        return tar;
    q.push(root);
    while (!q.empty())
    {
        TreeNode* temp = q.front();
        tar.push_back(temp->val);
        q.pop();
        if (temp->left)
            q.push(temp->left);
        if (temp->right)
            q.push(temp->right);
    }
    return tar;
}

//按行层序遍历二叉树
//vector<vector<int>> levelOrder(TreeNode* root) {
//    vector<vector<int>> vv;
//    if (root == nullptr)
//        return vv;
//    vv.resize(1, vector<int>());
//    queue<TreeNode*> q;
//    q.push(root);
//    int indX = 0;
//    for (int i = 1; !q.empty(); i++)
//    {
//        TreeNode* temp = q.front();
//        q.pop();
//        if (temp != nullptr)
//        {
//            vv[indX].push_back(temp->val);
//            if (temp->left)
//                q.push(temp->left);
//            else
//                q.push(nullptr);
//            if (temp->right)
//                q.push(temp->right);
//            else
//                q.push(nullptr);
//        }
//        if (i == (1 - pow(2, indX + 1)) / (-1))
//        {
//            indX++;
//            vv.resize(indX + 1, vector<int>());
//        }
//    }
//    while (vv[indX].empty())
//    {
//        vv.pop_back();
//        indX--;
//    }
//    return vv;
//}

//vector<vector<int>> levelOrder(TreeNode* root) {
//    vector<vector<int>> vv;
//    vv.resize(1, vector<int>());
//    queue<TreeNode*> q;
//    q.push(root);
//    int indX = 0;
//    for (int i = 1; !q.empty(); i++)
//    {
//        TreeNode* temp = q.front();
//        q.pop();
//        if (temp != nullptr)
//        {
//            vv[indX].push_back(temp->val);
//            if (temp->left)
//                q.push(temp->left);
//            else
//                q.push(nullptr);
//            if (temp->right)
//                q.push(temp->right);
//            else
//                q.push(nullptr);
//        }
//        if (i == (1 - pow(2, indX + 1)) / (-1))
//        {
//            indX++;
//            vv.resize(indX + 1,vector<int>());
//        }
//    }
//    return vv;
//}

//bool judge(queue<TreeNode*> s)
//{
//    bool flag = false;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (s.front() != nullptr)
//            return flag;
//        s.pop();
//    }
//    return true;
//}
//
//vector<vector<int>> levelOrder(TreeNode* root) {
//    vector<vector<int>> vv;
//    if (root == nullptr)
//        return vv;
//    vv.resize(1, vector<int>());
//    queue<TreeNode*> q;
//    q.push(root);
//    int indX = 0;
//    int flag = 0;
//    for (int i = 1; !q.empty(); i++)
//    {
//        TreeNode* temp = q.front();
//        q.pop();
//        if (temp != nullptr)
//        {
//            vv[indX].push_back(temp->val);
//            if (temp->left)
//            {
//                q.push(temp->left);
//                flag = 0;
//            }
//            else
//                q.push(nullptr);
//
//            if (temp->right)
//            {
//                q.push(temp->right);
//                flag = 0;
//            }
//            else
//                q.push(nullptr);
//        }
//        else
//        {
//            q.push(nullptr);
//            q.push(nullptr);
//            flag = flag + 2;
//        }
//        if (i == (1 - pow(2, indX + 1)) / (-1))
//        {
//            indX++;
//            if (judge(q))
//            {
//                indX--;
//                break;
//            }
//            vv.resize(indX + 1, vector<int>());
//        }
//    }
//    while (vv[indX].empty())
//    {
//        vv.pop_back();
//        indX--;
//    }
//    return vv;
//}
//
//bool judge(queue<TreeNode*> s)
//{
//    bool flag = false;
//    while (!s.empty())
//    {
//        if (s.front() != nullptr)
//            return flag;
//        s.pop();
//    }
//    return true;
//}
//
//vector<vector<int>> levelOrder(TreeNode* root) {
//    vector<vector<int>> vv;
//    if (root == nullptr)
//        return vv;
//    vv.resize(1, vector<int>());
//    queue<TreeNode*> q;
//    q.push(root);
//    int indX = 0;
//    int flag = 0;
//    for (int i = 1; !q.empty(); i++)
//    {
//        TreeNode* temp = q.front();
//        q.pop();
//        if (temp != nullptr)
//        {
//            vv[indX].push_back(temp->val);
//            if (temp->left)
//                q.push(temp->left);
//            else
//                q.push(nullptr);
//
//            if (temp->right)
//                q.push(temp->right);
//            else
//                q.push(nullptr);
//        }
//        else
//        {
//            q.push(nullptr);
//            q.push(nullptr);
//        }
//        if (i == (1 - pow(2, indX + 1)) / (-1))
//        {
//            indX++;
//            if (judge(q))
//            {
//                indX--;
//                break;
//            }
//            vv.resize(indX + 1, vector<int>());
//        }
//    }
//    while (vv[indX].empty())
//    {
//        vv.pop_back();
//        indX--;
//    }
//    return vv;
//}


struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

ListNode* partition(ListNode* pHead, int x) {
    // write code here
    ListNode* head = new ListNode(0);
    ListNode* tail = NULL;
    ListNode* first = NULL;
    ListNode* cur = pHead;
    head->next = pHead;
    tail = first = head;
    while (cur)
    {
        if (cur->val < x)
        {
            if (tail == first)
            {
                first = cur;
                cur = cur->next;
                tail = tail->next;
                continue;
            }
            first->next = cur->next;
            cur->next = tail->next;
            tail->next = cur;
            cur = first->next;
        }
        else {
            first = cur;
            cur = cur->next;
        }
    }
    return head->next;
}

int main()
{
    ListNode* node = new ListNode(2);
    ListNode* node1 = new ListNode(8);
    node->next = node1;
    node1 = new ListNode(1);
    node->next->next = node1;
    node1 = new ListNode(9);
    node->next->next->next = node1;
    partition(node, 4);

    return 0;
}

//int main()
//{
//    TreeNode* root = new TreeNode(1);
//    TreeNode* left = new TreeNode(9);
//    root->left = left;
//    //left = new TreeNode(9);
//    //root->left->left = left;
//    //left = new TreeNode(9);
//    //root->left->left->left = left;
//    //left = new TreeNode(9);
//    //root->left->left->left->left = left;
//
//    levelOrder(root);
//
//	return 0;
//}