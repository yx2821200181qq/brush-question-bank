#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

string replaceSpace(string s) {
    string::iterator it = find(s.begin(), s.end(),' ');
    /*it = find(s.begin(), s.end(), ' ');*/
    for (; it != s.end(); it = find(s.begin(), s.end(), ' '))
    {
        s.erase(it, it+1);
        s.insert(it++, '%');
        s.insert(it++, '2');
        s.insert(it, '0');
    }
    return s;
}

string replaceSpace1(string s) {
    string::iterator it = find(s.begin(), s.end(), ' ');
    it = find(s.begin(), s.end(), ' ');
    for (; it != s.end(); it = find(s.begin(), s.end(), ' '))
    {
        it = s.erase(it, it + 1);
        it = s.insert(it, '%');
        it++;
        it = s.insert(it, '2');
        it++;
        s.insert(it, '0');
    }
    return s;
}

//int main()
//{
//    string a = "We are happy#";
//    string s = replaceSpace(a);
//    cout << s << endl;
//    return 0;
//}

class Solution {
public:
    bool judge[10];
    void Work(vector<string>& tar, string s, string temp)
    {
        if (temp.size() == s.size())
        {
            tar.push_back(temp);
            return;
        }
        for (int i = 0; i < s.size(); i++)
        {
            if (!judge[i])
            {
                temp.push_back(s[i]);
                judge[i] = true;
                Work(tar, s, temp);
                temp.pop_back();
                judge[i] = false;
            }
        }
    }
    void remove_Same(vector<string>& tar)
    {
        string temp = tar[0];
        for (int i = 1; i < tar.size(); i++)
        {
            if (temp == tar[i])
            {
                tar.erase(tar.begin() + i);
                i--;
            }
            else if (temp != tar[i])
                 temp = tar[i];
        }
    }

    vector<string> permutation(string s) {
        vector<string> tar;
        memset(&judge, 0, 10);
        string temp;
        Work(tar, s, temp);
        sort(tar.begin(), tar.end());
        remove_Same(tar);
        return tar;
    }
};

//int main()
//{
//    Solution s;
//    s.permutation("aba");
//
//    return 0;
//}