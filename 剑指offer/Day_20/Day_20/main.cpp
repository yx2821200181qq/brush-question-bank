#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
#include<queue>
using namespace std;

//数据流中的中位数——快速排序暴力(超出时间限制)
class MedianFinder1 {
public:
    /** initialize your data structure here. */
    MedianFinder1() {}

    void addNum(int num) {
        v.push_back(num);
    }

    double findMedian() {
        sort(v.begin(), v.end());
        if (v.size() % 2 != 0)
            return v[v.size() / 2];
        return (v[v.size() / 2] + v[v.size() / 2 - 1]) / 2;
    }
private:
    vector<double> v;
};

//数据流中的中位数——大小堆(复杂版)
class MedianFinder {
public:
    /** initialize your data structure here. */
    MedianFinder() {}

    void addNum(int num) {
        if (pq_greater.size() == pq_less.size())
        {
            if (pq_less.size() == 0 || pq_less.top() > num) pq_greater.push(num);
            else
            {
                double temp = pq_less.top();
                pq_less.pop();
                pq_less.push(num);
                pq_greater.push(temp);
            }
        }
        else
        {
            if ((pq_less.size() == 0 || num < pq_less.top()) && (num > pq_greater.top() || (pq_less.size() != 0 && num > pq_less.top())))
                pq_less.push(num);
            else if ((pq_less.size() == 0 || num < pq_less.top()) && num < pq_greater.top() || num > pq_greater.top())
            {
                double temp = pq_greater.top();
                pq_less.push(temp);
                pq_greater.pop();
                pq_greater.push(num);
            }
        }
    }
    double findMedian() {
        if (pq_greater.size() > pq_less.size())
            return pq_greater.top();
        return (pq_greater.top() + pq_less.top()) / 2;
    }
private:
    priority_queue<double, vector<double>, less<double>> pq_greater;
    priority_queue<double, vector<double>, greater<double>> pq_less;
};

//数据流中的中位数——大小堆(简易版)
class MedianFinder {
public:
    /** initialize your data structure here. */
    MedianFinder() {}

    void addNum(int num) {
        if (down.empty() || down.top() > num)
        {
            down.push(num);
            if (down.size() > up.size() + 1)
            {
                up.push(down.top());
                down.pop();
            }
        }
        else
        {
            up.push(num);
            if (up.size() > down.size())
            {
                down.push(up.top());
                up.pop();
            }
        }
    }

    double findMedian() {
        if (up.size() < down.size()) return down.top();
        return (up.top() + down.top()) / 2;
    }
private:
    priority_queue<double> down;
    priority_queue<double, vector<double>, greater<double>> up;
};

//最小的数——快速排序
vector<int> getLeastNumbers1(vector<int>& arr, int k) {
    sort(arr.begin(), arr.end());
    vector<int> tar;
    for (int i = 0; i < k; i++)
    {
        tar.push_back(arr[i]);
    }
    return tar;
}

//最小的数——堆排序
void SortK(vector<int>& v, vector<int>& arr, int k)
{
    priority_queue<int> pq;
    for (int i = 0; k && i < arr.size(); i++)
    {
        if ( pq.size() == k && pq.top() > arr[i])
        {
            pq.pop();
            pq.push(arr[i]);
        }
        if (pq.size() < k) pq.push(arr[i]);
    }
    while(!pq.empty())
    {
        v.push_back(pq.top());
        pq.pop();
    }
}
vector<int> getLeastNumbers(vector<int>& arr, int k) {
    vector<int> v;
    SortK(v, arr, k);
    return v;
}

int main()
{
    vector<int> arr(4,4);
    getLeastNumbers(arr, 0);

	return 0;
}