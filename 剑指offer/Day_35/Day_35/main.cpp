#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<stack>
using namespace std;

void s_Exchage_int(string& tar, stack<int>& s)
{
    int num = 0;
    int i = 0;
    if (tar[i] == '-') i = 1;
    for (; i < tar.size(); i++)
    {
        num = num * 10 + tar[i] - '0';
    }
    if (tar[0] == '-') s.push(num * -1);
    else s.push(num);
}
int evalRPN(vector<string>& tokens) {
    stack<int> s;
    int a, b, tar;
    for (auto it : tokens)
    {
        if (it == "+" || it == "-" || it == "*" || it == "/")
        {
            int a = s.top();
            s.pop();
            int b = s.top();
            s.pop();
            if (it == "+") tar = a + b;
            if (it == "-") tar = a - b;
            if (it == "*") tar = a * b;
            if (it == "/") tar = a / b;
            s.push(tar);
        }
        else s_Exchage_int(it, s);
    }
    return s.top();
}

vector<int> asteroidCollision(vector<int>& asteroids) {
    vector<int> v;
    for (int i = 0; i < asteroids.size(); i++)
    {
        if (asteroids[i] < 0)
        {
            if (v.empty() || v.back() < 0)
            {
                v.push_back(asteroids[i]);
            }
            else if (v.back() > -1 * asteroids[i]) continue;
            else
            {
                if (!v.empty() && v.back() != -1 * asteroids[i]) i--;
                v.resize(v.size() - 1);
            }
        }
        else v.push_back(asteroids[i]);
    }
    return v;
}

typedef pair<int, int> PII;
vector<int> dailyTemperatures(vector<int>& temperatures) {
    vector<int> v(temperatures.size(), 0);
    stack<PII> s;
    for (int i = 0; i < temperatures.size(); i++)
    {
        if (s.empty() || s.top().first > temperatures[i])
        {
            PII temp(temperatures[i], i);
            s.push(temp);
            continue;
        }
        while (!s.empty() && s.top().first < temperatures[i])
        {
            v[s.top().second] = i - s.top().second;
            s.pop();
        }
        PII temp(temperatures[i], i);
        s.push(temp);
    }
    return v;
}

int main()
{


	return 0;
}