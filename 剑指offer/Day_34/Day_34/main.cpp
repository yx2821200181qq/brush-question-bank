#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

ListNode* removeNthFromEnd(ListNode* head, int n) {
    if (!head) return head;
    ListNode* temp = new ListNode(0, head);
    ListNode* left = temp, * right = head;
    while (n--)
        right = right->next;
    while (right)
    {
        left = left->next;
        right = right->next;
    }
    left->next = left->next->next;
    return temp->next;
}

//剑指 Offer II 023. 两个链表的第一个重合节点——方法1
int SizeList(ListNode* head)
{
    int num = 0;
    while (head)
    {
        num++;
        head = head->next;
    }
    return num;
}
ListNode* MoveNode(ListNode* head, int size)
{
    while (size--)
        head = head->next;
    return head;
}
ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
    int lengthA = SizeList(headA);
    int lengthB = SizeList(headB);
    if (lengthA > lengthB)
        headA = MoveNode(headA, lengthA - lengthB);
    else
        headB = MoveNode(headB, lengthB - lengthA);
    while (headA != headB)
    {
        headA = headA->next;
        headB = headB->next;
    }
    return headA;
}

//剑指 Offer II 023. 两个链表的第一个重合节点——方法2
ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
    ListNode* curA = headA, * curB = headB;
    int flag = 2;
    while (flag)
    {
        curA = curA->next;
        curB = curB->next;
        if (curA == nullptr)
        {
            curA = headB;
            flag--;
        }
        if (curB == nullptr)
        {
            curB = headA;
            flag--;
        }
    }
    while (curA != curB)
    {
        curA = curA->next;
        curB = curB->next;
    }
    return curA;
}

//剑指 Offer II 022. 链表中环的入口节点
ListNode* detectCycle(ListNode* head) {
    ListNode* slow = head, * fast = head;
    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
        if (fast == slow)
        {
            slow = head;
            while (fast != slow)
            {
                fast = fast->next;
                slow = slow->next;
            }
            return fast;
        }
    }
    return nullptr;
}

int main()
{
    int a = CHAR_MAX;
    cout << a << endl;

	return 0;
}