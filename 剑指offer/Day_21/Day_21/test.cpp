#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void Work(TreeNode* root, int& heigh, int n)
{
    if (!root)
        return;
    if (n > heigh) heigh = n;
    Work(root->left, heigh, n + 1);
    Work(root->right, heigh, n + 1);
}
int maxDepth(TreeNode* root) {
    if (!root) return 0;
    int heigh = 0;
    Work(root, heigh, 0);
    return heigh + 1;
}


int maxDepth(TreeNode* root) {
    if (!root) return 0;

    int left = maxDepth(root->left);
    int right = maxDepth(root->right);

    return left > right ? left + 1 : right + 1;
}

//ƽ�������
int Work(TreeNode* root, bool& flag)
{
    if (!root || !flag) return 0;
    int left = Work(root->left, flag);
    int right = Work(root->right, flag);
    if (abs(left - right) > 1 && flag) flag = false;

    return left > right ? left + 1 : right + 1;
}
bool isBalanced(TreeNode* root) {
    bool flag = true;
    Work(root, flag);
    return flag;
}

//ƽ�������
int Work(TreeNode* root)
{
    if (!root) return 0;
    int left = Work(root->left);
    int right = Work(root->right);

    return left > right ? left + 1 : right + 1;
}
bool Judge(TreeNode* root)
{
    if (!root) return true;
    else if (abs(Work(root->left) - Work(root->right)) > 1) return false;

    if (Judge(root->left) && Judge(root->right)) return true;
    return false;
}
bool isBalanced(TreeNode* root) {
    return Judge(root);
}

int main()
{


	return 0;
}