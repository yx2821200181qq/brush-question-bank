#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

//青蛙跳台阶——递归算法，n超过40时间复杂度过大
void Work(int& num, int n, int sum)
{
    if (sum == n)
    {
        num++;
        num = num % 1000000007;
        return;
    }
    if (sum > n)
        return;
    Work(num, n, sum + 1);
    Work(num, n, sum + 2);
}
int numWays(int n) {
    int num = 0;
    Work(num, n, 0);
    return num;
}

//青蛙跳台阶——动态规划解法
int ret = 1000000007;
int numWays(int n) {
    int f[102] = { 0 };
    f[0] = f[1] = 1;
    for (int i = 2; i <= n; i++)
    {
        f[i] = (f[i - 1] + f[i - 2]) % ret;
    }
    return f[n];
}

//股票的最大利润——暴力算法
int maxProfit1(vector<int>& prices) {
    int Max = INT_MIN;
    for (int i = 0; i < prices.size(); i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (prices[i] - prices[j] > Max)
                Max = prices[i] - prices[j];
        }
    }
    if (Max < 0)
        Max = 0;
    return Max;
}

//股票的最大利润——贪心算法
int maxProfit2(vector<int>& prices) {
    int Min = INT_MAX;
    int res = 0;
    for (auto it : prices)
    {
        Min = min(it, Min);
        res = max(res, it - Min);
    }
    return res;
}

//股票的最大利润——动态规划算法
int maxProfit3(vector<int>& prices) {
    int len = prices.size();
    if (len == 0)
        return 0;
    vector<vector<int>> f(len, vector<int>(2));
    f[0][0] = 0;
    f[0][1] = 0 - prices[0];
    for (int i = 1; i < len; i++)
    {
        f[i][0] = max(f[i - 1][0], f[i - 1][1] + prices[i]);
        f[i][1] = max(f[i - 1][1], 0 - prices[i]);
    }
    return f[len - 1][0];
}

int main()
{


	return 0;
}