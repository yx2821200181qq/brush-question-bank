#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

bool isStraight2(vector<int>& nums) {
    sort(nums.begin(), nums.end());
    int flag = 0, num = 0;
    for (int i = 0; i < 5; i++)
        if (nums[i] == 0) num++;
    for (int i = num + 1; i < 5; i++)
    {
        if (nums[i] == nums[i - 1]) return false;
        if (nums[i] != nums[i - 1] + 1) flag += nums[i] - nums[i - 1] - 1;
    }
    if (flag == num || flag == 0 || num > flag) return true;
    return false;
}

bool isStraight1(vector<int>& nums) {
    sort(nums.begin(), nums.end());
    int flag = 0, num = 0, i = 0;
    for (; nums[i] == 0; i++)
        num++;
    for (; i < 4; i++)
    {
        if (nums[i] == nums[i + 1]) return false;
        if (nums[i] + 1 != nums[i + 1])
        {
            if (num == 0)
                return false;
            else
            {
                nums[i]++;
                num--;
                i--;
            }
        }
    }
    return true;
}

int main()
{


	return 0;
}