#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    //树的子结构
    bool Work(TreeNode* A, TreeNode* B)
    {
        if ((!A && !B) || (A && !B))
            return true;
        if (!A || !B)
            return false;
        if (A->val == B->val)
        {
            if (Work(A->left, B->left) && Work(A->right, B->right))
                return true;
        }
        return false;
    }
    bool Judge1(TreeNode* A, TreeNode* B)
    {
        if (!A)
            return false;
        if ((A->val == B->val) && Work(A, B))
            return true;
        if (Judge1(A->right, B))
            return true;
        if (Judge1(A->left, B))
            return true;
        return false;
    }
    bool isSubStructure(TreeNode* A, TreeNode* B) {
        if (!A || !B)
            return false;
        return Judge1(A, B);
    }
    //镜像二叉树
    TreeNode* Work(TreeNode* root)
    {
        if (!root)
            return nullptr;
        TreeNode* temp = root->left;
        root->left = root->right;
        root->right = temp;
        Work(root->left);
        Work(root->right);
        return root;
    }
    TreeNode* mirrorTree(TreeNode* root) {
        if (!root)
            return root;
        return Work(root);
    }
    //对称的二叉树
    bool Judge(TreeNode* NodeL, TreeNode* NodeR)
    {
        if (!NodeL && !NodeR)
            return true;
        if ((!NodeL && NodeR) || (NodeL && !NodeR) || (NodeL->val != NodeR->val))
            return false;
        return Judge(NodeL->left, NodeR->right) && Judge(NodeL->right, NodeR->left);
    }
    bool isSymmetric(TreeNode* root) {
        if (!root || (!root->left && !root->right))
            return true;
        if ((!root->left && root->right) || (root->left && !root->right))
            return false;
        return Judge(root->left, root->right);
    }

};

int main()
{
    int* a = nullptr;

	return 0;
}