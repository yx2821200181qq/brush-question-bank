#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

//连续子数组的最大和 -- 动态规划算法1
int maxSubArray(vector<int>& nums) {
    int len = nums.size();
    vector<vector<int>> v(len, vector<int>(2));
    v[0][0] = nums[0];
    v[0][1] = 0;
    if (len > 1)
        v[1][1] = v[0][0];
    int flag = 0;
    for (int i = 1; i < len; i++)
    {
        if (nums[i] < 0)
            flag++;
        v[i][0] = max(v[i - 1][0] + nums[i], nums[i]);
        if (i != 1)
            v[i][1] = max(v[i - 1][0], v[i - 1][1]);
    }
    if (nums[0] < 0 && flag + 1 == len && (v[len - 1][0] == 0 || v[len - 1][1] == 0))
    {
        return min(v[len - 1][0], v[len - 1][1]);
    }
    return max(v[len - 1][0], v[len - 1][1]);
}

//连续子数组的最大和 -- 动态规划算法2
int maxSubArray(vector<int>& nums) {
    int res = 0, Max = nums[0];
    for (int i = 0; i < nums.size(); i++)
    {
        res = max(res + nums[i], nums[i]);
        Max = max(Max, res);
    }
    if (nums.size() == 1)
        Max = nums[0];
    return Max;
}

//礼物的最大价值——动态规划
int maxValue(vector<vector<int>>& grid) {
    for (int i = 0; i < grid.size(); i++)
    {
        for (int j = 0; j < grid[i].size(); j++)
        {
            if (i == 0 && j != 0)
            {
                grid[i][j] += grid[i][j - 1];
            }
            else if (j == 0 && i != 0)
            {
                grid[i][j] += grid[i - 1][j];
            }
            else if (i != 0 && j != 0)
            {
                grid[i][j] += max(grid[i - 1][j], grid[i][j - 1]);
            }
        }
    }
    return grid[grid.size() - 1][grid[0].size() - 1];
}

//int f[10];
//int n = 0;
//int tar;
//
//int main()
//{
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        string s;
//        cin >> s;
//        int head = s[0] - '0';
//        int tail = s.back() - '0';
//        f[tail] = max(f[tail], f[head] + 1);
//        tar = max(tar, f[tail]);
//    }
//    cout << n - tar << endl;
//    return 0;
//}

//typedef pair<int, int> PII;
//typedef long long LL;
//int n = 0, tar = 0;
//vector<PII> v;
//
//PII Work(LL a)
//{
//    PII num;
//    if (a < 10)
//    {
//        num.first = a;
//        num.second = a;
//        return num;
//    }
//    num.second = a % 10;
//    while (a > 10)
//    {
//        a = a / 10;
//    }
//    num.first = a;
//    return num;
//}
//
//int main()
//{
//    cin >> n;
//    if (n == 1)
//    {
//        cout << tar << endl;
//        return 0;
//    }
//    for (int i = 0; i < n; i++)
//    {
//        LL temp = 0;
//        cin >> temp;
//        PII flag = Work(temp);
//        v.push_back(flag);
//    }
//    for (int i = 1; i < n; i++)
//    {
//        if (v[i].first != v[i - 1].second)
//        {
//            tar++;
//            v.erase(v.begin() + i);
//            i--;
//            n--;
//        }
//    }
//    cout << tar << endl;
//    return 0;
//}