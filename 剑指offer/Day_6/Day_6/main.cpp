#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

class Solution {
public:
    bool findNumberIn2DArray(vector<vector<int>>& matrix, int target) {
        int x = matrix.size() - 1;
        int y = 0;
        while (x >= 0 && y < matrix[0].size())
        {
            if (matrix[x][y] == target)
                return true;
            else if (matrix[x][y] > target)
                x--;
            else
                y++;
        }
        return false;
    }
    int minArray1(vector<int>& numbers) {
        int temp = numbers[0];
        for (int i = 0; i < numbers.size(); i++)
        {
            if (temp > numbers[i])
                temp = numbers[i];
        }
        return temp;
    }
    vector<int> exchange1(vector<int>& nums) {
        vector<int> oddNum;
        vector<int> evenNum;
        for (auto it : nums)
        {
            if (it % 2 == 0)
                evenNum.push_back(it);
            else
                oddNum.push_back(it);
        }
        oddNum.insert(oddNum.end(), evenNum.begin(), evenNum.end());
        return oddNum;
    }
    vector<int> exchange(vector<int>& nums) {
        int left = 0;
        int right = nums.size() - 1;
        while (left < right-1)
        {
            while (nums[left] % 2 == 1)
                left++;
            while (nums[right] % 2 == 0)
                right--;
            swap(nums[left], nums[right]);
        }
        return nums;
    }
    int majorityElement(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int n = 0;
        int temp = nums[0];
        for (auto it : nums)
        {
            if (temp == it)
                n++;
            if (temp != it)
            {
                temp = it;
                n = 1;
            }
            if (n > nums.size() / 2)
                return temp;
        }
        return -1;
    }
};

int main()
{
    Solution s;
    vector<int> num;
    for (int i = 1; i < 5; i++)
        num.push_back(i);
    s.exchange(num);

	return 0;
}