#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<queue>
using namespace std;

class MovingAverage {
private:
    int _size;
    int _sum;
    queue<int> q;
public:
    MovingAverage(int size) {
        _size = size;
        _sum = 0;
    }

    double next(int val) {
        if (q.size() == _size)
        {
            _sum -= q.front();
            q.pop();
        }
        _sum += val;
        q.push(val);
        //cout << _sum << " " << q.size() << endl;
        if (q.size() == _size) return double(_sum / _size);
        else return double(_sum / q.size());
    }
};

class RecentCounter {
private:
    queue<int> q;

public:
    RecentCounter() {}

    int ping(int t) {
        while (!q.empty() && t - q.front() > 3000)
        {
            q.pop();
        }
        q.push(t);
        return q.size();
    }
};

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class CBTInserter {
private:
    TreeNode* _root;
    queue<TreeNode*> q;
public:
    CBTInserter(TreeNode* root) {
        _root = root;
        queue<TreeNode*> Qtemp;
        Qtemp.push(root);
        int flag = 1;
        while (!Qtemp.empty())
        {
            if (!Qtemp.front()->left || !Qtemp.front()->right) flag = 0;
            TreeNode* node = Qtemp.front();
            if (flag == 0) q.push(Qtemp.front());
            Qtemp.pop();
            if (node->left) Qtemp.push(node->left);
            if (node->right) Qtemp.push(node->right);
        }
    }

    int insert(int v) {
        TreeNode* temp = new TreeNode(v);
        int flag = 0;
        if (!q.front()->left)
        {
            q.front()->left = temp;
            flag = q.front()->val;
        }
        else
        {
            q.front()->right = temp;
            flag = q.front()->val;
            q.pop();
        }
        q.push(temp);
        return flag;
    }

    TreeNode* get_root() {
        return _root;
    }
};

//方法1：剑指 Offer II 044. 二叉树每层的最大值
vector<int> largestValues1(TreeNode* root) {
    if (!root) return vector<int>();
    int _max = INT_MIN;
    vector<int> v;
    queue<TreeNode*> q;
    q.push(root);
    while (!q.empty())
    {
        queue<TreeNode*> qtemp;
        while (!q.empty())
        {
            TreeNode* temp = q.front();
            if (temp->val > _max) _max = temp->val;
            if (temp->left) qtemp.push(temp->left);
            if (temp->right) qtemp.push(temp->right);
            q.pop();
        }
        v.push_back(_max);
        _max = INT_MIN;
        while (!qtemp.empty())
        {
            TreeNode* temp = qtemp.front();
            q.push(temp);
            qtemp.pop();
        }
    }
    return v;
}

//方法2：剑指 Offer II 044. 二叉树每层的最大值
vector<int> largestValues2(TreeNode* root) {
    if (!root) return vector<int>();
    int _max = INT_MIN;
    vector<int> v;
    queue<TreeNode*> q;
    q.push(root);
    while (!q.empty())
    {
        int size = q.size();
        while (size--)
        {
            TreeNode* temp = q.front();
            if (temp->val > _max) _max = temp->val;
            if (temp->left) q.push(temp->left);
            if (temp->right) q.push(temp->right);
            q.pop();
        }
        v.push_back(_max);
        _max = INT_MIN;
    }
    return v;
}


//剑指 Offer II 045. 二叉树最底层最左边的值
int findBottomLeftValue(TreeNode* root) {
    int tar = root->val;
    bool flag = true;
    queue<TreeNode*> q;
    q.push(root);
    while (!q.empty())
    {
        int size = q.size();
        while (size--)
        {
            TreeNode* temp = q.front();
            if (flag && (temp->left || temp->right))
            {
                tar = temp->left ? temp->left->val : temp->right->val;
                flag = false;
            }
            if (temp->left) q.push(temp->left);
            if (temp->right) q.push(temp->right);
            q.pop();
        }
        flag = true;
    }
    return tar;
}

vector<int> rightSideView(TreeNode* root) {
    if (!root) return {};
    vector<int> v;
    queue<TreeNode*> q;
    q.push(root);
    while (!q.empty())
    {
        int size = q.size();
        while (size--)
        {
            TreeNode* temp = q.front();
            if (!size)
            {
                v.push_back(temp->val);
            }
            if (temp->left) q.push(temp->left);
            if (temp->right) q.push(temp->right);
            q.pop();
        }
    }
    return v;
}

int main()
{
    MovingAverage ma(3);
    cout << ma.next(1) << endl;
    cout << ma.next(10) << endl;
    cout << ma.next(3) << endl;
    cout << ma.next(5) << endl;


	return 0;
}