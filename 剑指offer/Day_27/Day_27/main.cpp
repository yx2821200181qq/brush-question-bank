#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<stack>
using namespace std;

bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
    stack<int> s;
    int i = 0, j = 0;
    for (; i < pushed.size(); i++)
    {
        s.push(pushed[i]);
        while (s.size() != 0 && s.top() == popped[j])
        {
            s.pop();
            j++;
        }
    }
    return s.size() == 0;
}

vector<int> spiralOrder(vector<vector<int>>& matrix) {
    if (matrix.size() == 0) return vector<int>();
    int Xmin = 0, Xmax = matrix.size() - 1;
    int Ymin = 0, Ymax = matrix[0].size() - 1;
    vector<int> v;
    while (Xmin <= Xmax && Ymax >= Ymin)
    {
        for (int i = Ymin; i <= Ymax; i++)
        {
            v.push_back(matrix[Xmin][i]);
        }
        Xmin++;
        if (Xmin > Xmax || Ymax < Ymin) break;
        for (int i = Xmin; i <= Xmax; i++)
        {
            v.push_back(matrix[i][Ymax]);
        }
        Ymax--;
        if (Xmin > Xmax || Ymax < Ymin) break;
        for (int i = Ymax; i >= Ymin; i--)
        {
            v.push_back(matrix[Xmax][i]);
        }
        Xmax--;
        if (Xmin > Xmax || Ymax < Ymin) break;
        for (int i = Xmax; i >= Xmin; i--)
        {
            v.push_back(matrix[i][Ymin]);
        }
        Ymin++;
    }

    return v;
}

int main()
{
    vector<vector<int>> vv(3, vector<int>());
    int a[3][4] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
    for (int i = 0; i < 3; i++)
    {
        vector<int> v;
        for (int j = 0; j < 4; j++)
        {
            v.push_back(a[i][j]);
        }
        vv[i] = v;
    }
    spiralOrder(vv);
    return 0;
}