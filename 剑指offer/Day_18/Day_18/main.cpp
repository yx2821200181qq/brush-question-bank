#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<queue>
#include<functional>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

void Work(vector<vector<int>>& vv, TreeNode* root, int target, vector<int>& v, int Sum)
{
    if (Sum + root->val == target && root->left == nullptr && root->right == nullptr)
    {
        v.push_back(root->val);
        vv.push_back(v);
        v.pop_back();
        return;
    }
    if (!root->left && !root->right)
        return;
    v.push_back(root->val);
    if (root->left)
        Work(vv, root->left, target, v, Sum + root->val);
    if (root->right)
        Work(vv, root->right, target, v, Sum + root->val);
    v.pop_back();
}

vector<vector<int>> pathSum(TreeNode* root, int target) {
    vector<vector<int>> vv;
    if (!root)
        return vv;
    vector<int> v;
    Work(vv, root, target, v, 0);
    return vv;
}

void Work(TreeNode* root, priority_queue<int, vector<int>, greater<int>>& pq, int k)
{
    if (!root)
        return;
    if (pq.size() == k)
    {
        int temp = pq.top();
        if (temp < root->val)
        {
            pq.pop();
            pq.push(root->val);
        }
    }
    else
        pq.push(root->val);
    Work(root->left, pq, k);
    Work(root->right, pq, k);
}
int kthLargest(TreeNode* root, int k) {
    priority_queue<int, vector<int>, greater<int>> pq;
    Work(root, pq, k);
    return pq.top();
}

class Node {
public:
    int val;
    Node* left;
    Node* right;

    Node() {}

    Node(int _val) {
        val = _val;
        left = NULL;
        right = NULL;
    }

    Node(int _val, Node* _left, Node* _right) {
        val = _val;
        left = _left;
        right = _right;
    }
};

//Node* Work(Node* root, Node* front)
//{
//    if (!root)
//        return nullptr;
//    return nullptr;
//}
//Node* treeToDoublyList(Node* root) {
//    return Work(root, &(Node()));
//}

//void Work(Node* root, Node* front, Node** tail)
//{
//    if (!root)
//        return;
//    Work(root->left, front, tail);
//    front->right = root;
//    root->left = front;
//    front = root->left;
//    *tail = root;
//    Work(root->right, front, tail);
//}
//Node* treeToDoublyList(Node* root) {
//    Node* head = new Node(0);
//    Node* tail = nullptr;
//    Work(root, head, &tail);
//    tail->right = head->right;
//    head->right->left = tail;
//    return head->right;
//}

//void Work(Node* root, Node** front, Node** tail)
//{
//    if (!root)
//        return;
//    Work(root->left, front, tail);
//    (*front)->right = root;
//    root->left = *front;
//    *front = root;
//    *tail = root;
//    Work(root->right, front, tail);
//}

void Work(Node* root, Node*& front, Node*& tail)
{
    if (!root)
        return;
    Work(root->left, front, tail);
    front->right = root;
    root->left = front;
    front = root;
    if(!tail)
        tail = root;
    Work(root->right, front, tail);
}
Node* treeToDoublyList(Node* root) {
    if (!root)
        return nullptr;
    Node* head = new Node(0);
    Node* tail = nullptr;
    Work(root, head, tail);
    head->right->left = tail;
    tail->right = head->right;
    return head->right;
}

int main()
{
    //TreeNode* head = new TreeNode(-2);
    //TreeNode* node = new TreeNode(-3);
    //head->right = node;
    //node = new TreeNode(-4);
    //head->left = node;
    //pathSum(head, -5);
    //cout << kthLargest(head, 2) << endl;
    Node* head = new Node(-2);
    Node* node = new Node(-3);
    head->right = node;
    node = new Node(-4);
    head->left = node;
    treeToDoublyList(head);

    return 0;
}