#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;

//��ָ Offer 46. �����ַ�����ַ��� ���� ����ö��
void Exchange(int num, string& s)
{
    if (num == 0)
    {
        s.push_back('0');
        return;
    }
    while (num)
    {
        int temp = num % 10;
        s.push_back(temp + '0');
        num = num / 10;
    }
    reverse(s.begin(), s.end());
    return;
}
bool Judge(string s, int cur)
{
    if (cur + 1 == s.size() || s[cur] == '0' || (s[cur] - '0') * 10 + (s[cur + 1] - '0') > 25)
        return false;
    return true;
}
void Work(string s, int& tar, int cur)
{
    if (cur == s.size())
    {
        tar++;
        return;
    }
    else if (cur > s.size())
        return;
    for (int i = 1; i <= 2; i++)
    {
        if (i == 1)
            Work(s, tar, cur + 1);
        else if (Judge(s, cur))
        {
            Work(s, tar, cur + 2);
        }
    }
}
int translateNum(int num) {
    string s;
    Exchange(num, s);
    int tar = 0;
    Work(s, tar, 0);
    return tar;
}

//��ָ Offer 46. �����ַ�����ַ��� ���� �ݹ�
int translateNum1(int num) {
    if (num < 10)
        return 1;
    int flag = num % 100;
    if (flag > 25 || flag < 10)
        return translateNum(num / 10);
    return translateNum(num / 10) + translateNum(num / 100);
}

//��ָ Offer 46. �����ַ�����ַ��� ���� ��̬�滮
int translateNum2(int num) {
    string s = to_string(num);
    int a1 = 0, a2 = 0, cur = 0;
    for (int i = 0; i < s.size(); i++)
    {
        if (i == 0)
        {
            a2 = 1;
            cur = a2;
        }
        else if (i == 1)
        {
            if ((s[0] - '0') * 10 + (s[1] - '0') > 25) a1 = a2;
            else a1 = a2 + 1;
            cur = a1;
        }
        else
        {
            if (s[i - 1] != '0' && (s[i - 1] - '0') * 10 + (s[i] - '0') <= 25) cur = a1 + a2;
            else cur = a1;
            a2 = a1;
            a1 = cur;
        }
    }
    return cur;
}

//int lengthOfLongestSubstring(string s) {
//    vector<int> v(27, 0);
//    int left = 0;
//    int Max = 0, num = 0;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (v[s[i] - 'a'] == 0)
//        {
//            v[s[i] - 'a']++;
//        }
//        else if (v[s[i] - 'a'] != 0)
//        {
//            Max = max(i - left, Max);
//            for (int j = left; j <= i; j++)
//            {
//                if (s[j] == s[i])
//                {
//                    v[s[j] - 'a']--;
//                    left = j + 1;
//                    break;
//                }
//                v[s[j] - 'a']--;
//            }
//        }
//    }
//    return Max;
//}

//48. ������ظ��ַ������ַ��� ���� �����㷨
int flag = 0;
bool Judge(int left, int right, string s)
{
    for (flag = left; flag < right; flag++)
    {
        if (s[flag] == s[right])
            return false;
    }
    return true;
}
int lengthOfLongestSubstring1(string s) {
    int left = 0, Max = 0, num = 0;
    for (int i = 0; i < s.size(); i++)
    {
        if (Judge(left, i, s))
        {
            num = num + 1;
            Max = max(Max, num);
        }
        else
        {
            num = i - flag;
            left = flag + 1;
        }
    }
    if (Max == 0)
        Max = s.size();
    return Max;
}

int main()
{
    //cout << translateNum(12208) << endl;
    cout << lengthOfLongestSubstring1("abcabcbb") << endl;

    return 0;
}