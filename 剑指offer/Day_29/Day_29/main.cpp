#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

bool FindPath(TreeNode* root, stack<TreeNode*>& s, TreeNode* tar)
{
    if (!root) return false;
    s.push(root);
    if (root != tar && !FindPath(root->left, s, tar) && !FindPath(root->right, s, tar))
    {
        s.pop();
        return false;
    }
    return true;
}
TreeNode* FindNode(stack<TreeNode*>& maxS, stack<TreeNode*>& minS)
{
    while (maxS.size() != minS.size())
        maxS.pop();
    TreeNode* maxNode = maxS.top(), * minNode = minS.top();
    maxS.pop();
    minS.pop();
    while (maxNode != minNode)
    {
        maxNode = maxS.top();
        minNode = minS.top();
        maxS.pop();
        minS.pop();
    }
    return maxNode;
}
TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    stack<TreeNode*> pS, qS;
    FindPath(root, pS, p);
    FindPath(root, qS, q);
    if (pS.size() > qS.size())
        return FindNode(pS, qS);
    else
        return FindNode(qS, pS);
}

int main()
{


    return 0;
}