#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
using namespace std;

//剑指 Offer 39. 数组中出现次数超过一半的数字——超过一半的数，必然在中间，排序后取中间值即可
int majorityElement(vector<int>& nums) {
    sort(nums.begin(), nums.end());
    return nums[nums.size() / 2];
}

//剑指 Offer 66. 构建乘积数组
vector<int> constructArr1(vector<int>& a) {
    vector<int> left, right, tar;
    if (a.size() == 0) return tar;
    for (int i = 0; i <= a.size() - 1; i++)
    {
        int temp = 0;
        if (i == 0)
        {
            left.push_back(a[i]);
            right.push_back(a[a.size() - 1]);
        }
        else
        {
            temp = left[i - 1] * a[i];
            left.push_back(temp);
            temp = right[i - 1] * a[a.size() - 1 - i];
            right.push_back(temp);
        }
    }
    for (int i = 0; i < a.size(); i++)
    {
        int numL = 1, numR = 1;
        if (i - 1 >= 0) numL = left[i - 1];
        if (i + 1 < a.size()) numR = right[a.size() - 2 - i];
        tar.push_back(numL * numR);
    }
    return tar;
}

//剑指 Offer 66. 构建乘积数组——简化版1
vector<int> constructArr2(vector<int>& a) {
    if (a.size() == 0) return vector<int>();
    vector<int> Num(a.size() + 1), tar(a.size());
    Num[0] = 1;
    for (int i = 1; i <= a.size(); i++)
    {
        Num[i] = a[i - 1] * Num[i - 1];
    }
    tar[a.size() - 1] = Num[a.size() - 1];
    int temp = a[a.size() - 1];
    for (int i = a.size() - 2; i >= 0; i--)
    {
        tar[i] = temp * Num[i];
        temp *= a[i];
    }
    return tar;
}

//剑指 Offer 66. 构建乘积数组——简化版2
vector<int> constructArr(vector<int>& a) {
    if (a.size() == 0) return {};
    int n = a.size();
    int temp = a[n - 1];
    vector<int> tar(n, 1);
    for (int i = 1; i < n; i++)
        tar[i] = tar[i - 1] * a[i - 1];
    for (int i = n - 2; i >= 0; i--)
    {
        tar[i] = tar[i] * temp;
        temp *= a[i];
    }
    return tar;
}

//int NumDigit(int n)
//{
//    int count = 0;
//    while (n)
//    {
//        count++;
//        n = n / 10;
//    }
//    return count;
//}
//bool compare(int a, int b)
//{
//    int num1 = NumDigit(a);
//    int num2 = NumDigit(b);
//    return a * pow(10, num2) + b < b * pow(10, num1) + a;
//}
//string minNumber(vector<int>& nums) {
//    sort(nums.begin(), nums.end(), compare);
//    for (auto it : nums)
//        cout << it << " ";
//    return string();
//}

static int NumDigit(int n)
{
    if (n == 0) return 1;
    int count = 0;
    while (n)
    {
        count++;
        n = n / 10;
    }
    return count;
}
static bool compare(const int& a, const int& b)
{
    int num1 = NumDigit(a);
    int num2 = NumDigit(b);
    return a * pow(10, num2) + b < b* pow(10, num1) + a;
}
string minNumber(vector<int>& nums) {
    sort(nums.begin(), nums.end(), compare);
    string s;
    for (auto it : nums)
        s += to_string(it);

    return s;
}

int main()
{
    int arr[] = { 1,2,3,4,5,6,9,0,18 };
    vector<int> v;
    for (int i = 0; i < sizeof(arr)/sizeof(arr[0]); i++)
        v.push_back(arr[i]);
    cout << minNumber(v) << endl;

	return 0;
}