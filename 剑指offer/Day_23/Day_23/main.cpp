#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

int Find(vector<int>& in, int InLeft, int InRight, int val)
{
    while (InLeft <= InRight)
    {
        if (in[InLeft] == val) return InLeft;
        InLeft++;
    }
    return 0;
}
TreeNode* Work(vector<int>& pre, vector<int>& in, int PreLeft, int PreRight, int InLeft, int InRight)
{
    TreeNode* head = new TreeNode(pre[PreLeft]);
    if (PreLeft >= PreRight) return head;

    int Inmid = Find(in, InLeft, InRight, pre[PreLeft]);
    int NumL = Inmid - InLeft;
    int NumR = InRight - Inmid;

    if (NumL > 0) head->left = Work(pre, in, PreLeft + 1, PreLeft + NumL, InLeft, Inmid - 1);
    if (NumR > 0) head->right = Work(pre, in, PreLeft + 1 + NumL, PreLeft + NumL + NumR, Inmid + 1, InRight);
    return head;
}
TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
    if (preorder.size() == 0) return nullptr;
    return Work(preorder, inorder, 0, preorder.size() - 1, 0, inorder.size() - 1);
}

//TreeNode* CreateTree()
//{
//    int temp = 0;
//    cin >> temp;
//    if (temp < 0) return nullptr;
//    TreeNode* node = new TreeNode(temp);
//    node->left = CreateTree();
//    node->right = CreateTree();
//}
//
//int Find(vector<int>& in, int InLeft, int InRight, int val)
//{
//    while (InLeft <= InRight)
//    {
//        if (in[InLeft] == val) return InLeft;
//        InLeft++;
//    }
//    return 0;
//}
//TreeNode* Work(vector<int>& pre, vector<int>& in, int PreLeft, int PreRight, int InLeft, int InRight)
//{
//    TreeNode* head = new TreeNode(pre[PreLeft]);
//    if (PreLeft >= PreRight) return head;
//
//    int Inmid = Find(in, InLeft, InRight, pre[PreLeft]);
//    int NumL = Inmid - InLeft;
//    int NumR = InRight - Inmid;
//
//    head->left = Work(pre, in, PreLeft + 1, PreLeft + NumL, InLeft, Inmid - 1);
//    head->right = Work(pre, in, PreLeft + 1 + NumL, PreLeft + NumL + NumR, Inmid + 1, InRight);
//    return head;
//}
//TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
//    return Work(preorder, inorder, 0, preorder.size() - 1, 0, inorder.size() - 1);
//}

int main()
{
    //TreeNode* node = CreateTree();
    vector<int> preorder, inorder;
    int arr1[5] = { 3,9,20,15,7 };
    int arr2[5] = { 9,3,15,20,7 };
    for (int i = 0; i < 5; i++)
    {
        preorder.push_back(arr1[i]);
        inorder.push_back(arr2[i]);
    }
    
    buildTree(preorder, inorder);

    return 0;
}

int hammingWeight(uint32_t n) {
    int count = 0;
    for (int i = 0; i < 32; i++)
    {
        int temp = n >> i;
        temp ^= 1;
        count += (temp & 1 == 1) ? 0 : 1;
    }
    return count;
}

int hammingWeight1(uint32_t n) {
    int tar = 0;
    while (n)
    {
        tar += n & 1;
        n = n >> 1;
    }
    return tar;
}

int add(int a, int b) {
    while (a && b)
    {
        long long tempA = a, tempB = b;
        a = tempA & tempB;//找出两数中都为1的位数
        b = tempA ^ tempB;//找出两数中，一个为1，一个为0的位数
        a = (unsigned)a << 1;//两数中都为1的位置，向左移动一位
        //需要将变量a强制类型转换为无符号整形，否则当a的第32为为1时，在向左移动一位，编译器会报错
    }
    return (a == 0) ? b : a;
}

//int main()
//{
//    cout << add(-2, -8) << endl;
//    //int temp = (-16) ^ (6);
//    //1111 1111 1111 1111 1111 1111 1111 0000 —— -16
//    //0000 0000 0000 0000 0000 0000 0000 0110
//    //cout << temp << endl;
//
//	return 0;
//}