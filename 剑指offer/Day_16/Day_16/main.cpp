#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

//57.和为s的两个数字 —— 二分法
int Dichotomy(vector<int>& nums, int left, int right, int tar)
{
    while (left <= right)
    {
        int mid = left + ((right - left) >> 1);
        if (nums[mid] == tar)
            return mid;
        else if (nums[mid] > tar)
            right = mid - 1;
        else
            left = mid + 1;
    }
    return -1;
}
vector<int> twoSum(vector<int>& nums, int target) {
    vector<int> tarV;
    int right = 0;
    for (int i = nums.size() - 1; i >= 0; i--)
    {
        if (nums[i] < target)
        {
            right = i;
            break;
        }
    }
    for (int i = 0; i < right; i++)
    {
        int temp = Dichotomy(nums, i + 1, right, target - nums[i]);
        if (temp != -1)
        {
            tarV.push_back(nums[temp]);
            tarV.push_back(nums[i]);
            break;
        }
    }
    return tarV;
}

//52.两个链表的第一个公共节点——二分法
ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
    if (!headA || !headB) return nullptr;
    ListNode* curA = headA;
    ListNode* curB = headB;
    while (curA != curB)
    {
        if (curA) curA = curA->next;
        else curA = headB;
        if (curB) curB = curB->next;
        else curB = headA;
    }
    return curA;
}

int lastRemaining(int n, int m) {
    int pos = 0;
    for (int i = 2; i <= n; i++)
        pos = (pos + m) % i;
    return pos;
}
