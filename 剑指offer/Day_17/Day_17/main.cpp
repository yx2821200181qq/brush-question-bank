#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<queue>
#include<vector>
using namespace std;

//int dx[4] = { -1,1,0,0 };
//int dy[4] = { 0,0,-1,1 };
//bool f[7][7] = { false };

//bool Judge(vector<vector<char>>& board, string word, int x, int y, int index)
//{
//    if (board[x][y] != word[index] || f[x][y])
//        return false;
//    else if (board[x][y] == word[index] && index == word.size() - 1)
//        return true;
//    for (int i = 0; i < 4; i++)
//    {
//        if (x + dx[i] < 0 || x + dx[i] >= board.size() || y + dy[i] < 0 || y + dy[i] >= board[0].size())
//            continue;
//        f[x][y] = true;
//        if (Judge(board, word, x + dx[i], y + dy[i], index + 1))
//            return true;
//        f[x][y] = false;
//    }
//    return false;
//}
//bool exist(vector<vector<char>>& board, string word) {
//
//    for (int i = 0; i < board.size(); i++)
//    {
//        for (int j = 0; j < board[i].size(); j++)
//        {
//            if (board[i][j] == word[0] && Judge(board, word, i, j, 0))
//            {
//                return true;
//            }
//        }
//    }
//    return false;
//}

int Sum(int a)
{
    int num = 0;
    while (a)
    {
        num += a % 10;
        a = a / 10;
    }
    return num;
}
bool Judge(int i, int j, int k)
{
    int num = 0;
    num += Sum(i);
    num += Sum(j);
    if (num > k)
        return false;
    return true;
}
int movingCount(int m, int n, int k) {
    int tar = 0;
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (Judge(i, j, k))
            {
                cout << i << " " << j << endl;
                tar++;
            }
        }
    }
    return tar;
}
typedef pair<int, int> PII;
int dx[4] = { -1,1,0,0 };
int dy[4] = { 0,0,-1,1 };

int movingCount1(int m, int n, int k) {
    queue<PII> q;
    int tar = 0;
    PII First(0, 0);
    q.push(First);
    bool f[101][101] = { false };
    f[0][0] = true;
    while (!q.empty())
    {
        tar++;
        PII flag = q.front();
        q.pop();
        for (int i = 0; i < 4; i++)
        {
            int x = flag.first + dx[i];
            int y = flag.second + dy[i];
            if (x >= 0 && x < m && y >= 0 && y < n && Judge(x, y, k) && !f[x][y])
            {
                PII temp(x, y);
                q.push(temp);
                f[x][y] = true;
            }
        }
    }
    return tar;
}

int main()
{
    cout << movingCount1(16, 8, 4) << endl;

	return 0;
}