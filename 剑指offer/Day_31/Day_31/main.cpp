#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

//剑指 Offer II 010. 和为 k 的子数组——时间复杂度过大
int subarraySum(vector<int>& nums, int k) {
    vector<int> v;
    int num = 0;
    for (auto it : nums)
    {
        if (v.empty()) v.push_back(it);
        else v.push_back(v[v.size() - 1] + it);
        // cout << v[v.size()-1] << " ";
    }
    cout << endl;
    for (int i = 0; i < v.size(); i++)
    {
        if (v[i] == k)
        {
            num++;
        }
        for (int j = 0; j < i; j++)
        {
            if (v[j] == k - v[i] || v[j] == v[i] - k)
            {
                if ((k - v[i] >= 0 && v[j] <= 0) || (k - v[i] <= 0 && v[j] >= 0)) num++;
            }
        }
        cout << num << " ";
    }
    return num;
}

//剑指 Offer II 012. 左右两边子数组的和相等
int pivotIndex(vector<int>& nums) {
    int size = nums.size();
    vector<int> v(size + 2, 0);
    for (int i = 1; i <= size; i++)
    {
        v[i] = nums[i - 1] + v[i - 1];
    }
    v[size + 1] = v[size];
    for (int i = 1; i <= size; i++)
    {
        if (v[i - 1] == v[size + 1] - v[i]) return i - 1;
    }
    return -1;
}

//剑指 Offer II 013. 二维子矩阵的和
class NumMatrix {
public:
    NumMatrix(vector<vector<int>>& matrix) {
        vector<int> v(matrix[0].size() + 1, 0);
        tar.push_back(v);
        for (int i = 0; i < matrix.size(); i++)
            tar.push_back(v);
        for (int i = 0; i < matrix.size(); i++)
        {
            for (int j = 0; j < matrix[i].size(); j++)
            {
                tar[i + 1][j + 1] = tar[i][j + 1] + tar[i + 1][j] + matrix[i][j] - tar[i][j];
            }
        }
        for (int i = 0; i <= matrix.size(); i++)
        {
            for (int j = 0; j <= matrix[0].size(); j++)
            {
                cout << tar[i][j] << " ";
            }
            cout << endl;
        }
    }

    int sumRegion(int row1, int col1, int row2, int col2) {
        //cout << tar[row2+1][col2+1] << " " << tar[row2+1][col1] << " " << tar[row1][col2+1] << " " << tar[row1][col1] << endl;
        return tar[row2 + 1][col2 + 1] - tar[row2 + 1][col1] - tar[row1][col2 + 1] + tar[row1][col1];
    }
private:
    vector<vector<int>> tar;
};

int main()
{

	return 0;
}