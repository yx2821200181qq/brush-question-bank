#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<queue>
#include<list>
using namespace std;

class MaxQueue {
private:
    queue<int> _queue;
    list<int> _max;
public:
    MaxQueue() {}

    int max_value() {
        if (_queue.empty())
            return -1;
        return _max.front();
    }

    void push_back(int value) {
        _queue.push(value);
        while (!_max.empty() && _max.back() < value)//��������
            _max.pop_back();
        _max.push_back(value);
    }

    int pop_front() {
        if (_queue.empty())
            return -1;
        int temp = _queue.front();
        if (temp == _max.front())
            _max.pop_front();
        _queue.pop();
        return temp;
    }
};

class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        if (k == 1)
            return nums;
        MaxQueue mq;
        vector<int> v;
        for (int i = 0; i < k; i++)
        {
            mq.push_back(nums[i]);
        }
        v.push_back(mq.max_value());
        for (int i = k; i < nums.size(); i++)
        {
            mq.pop_front();
            mq.push_back(nums[i]);
            v.push_back(mq.max_value());
        }

        return v;
    }
};

int main()
{

	return 0;
}