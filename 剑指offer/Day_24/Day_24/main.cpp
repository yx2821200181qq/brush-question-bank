#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

//剑指 Offer 56 - I. 数组中数字出现的次数
vector<int> singleNumbers(vector<int>& nums) {
    int tar = 0;
    for (auto it : nums)
    {
        tar ^= it;
    }
    int flag = 0;
    for (int i = 0; i <= 31; i++)
    {
        if (((tar >> i) & 1) == 1)
        {
            flag = i;
            break;
        }
    }
    int a1 = 0, a2 = 0;
    for (auto it : nums)
    {
        if (((it >> flag) & 1) == 0) a1 ^= it;
        else a2 ^= it;
    }
    vector<int> ret;
    ret.push_back(a1);
    ret.push_back(a2);
    return ret;
}

//剑指 Offer 56 - II. 数组中数字出现的次数 II——快速排序
int singleNumber1(vector<int>& nums) {
    sort(nums.begin(), nums.end());
    for (int i = 0; i < nums.size();)
    {
        if (i == nums.size() - 1 || nums[i] != nums[i + 2]) return nums[i];
        i = i + 3;
    }
    return 0;
}

//剑指 Offer 56 - II. 数组中数字出现的次数 II——位运算，取每位1的个数余3
int singleNumber2(vector<int>& nums) {
    vector<int> v(32, 0);
    for (auto it : nums)
    {
        for (int i = 0; i < 32; i++)
        {
            if (((it >> i) & 1) == 1) v[i]++;
        }
    }
    int tar = 0;
    for (int i = 0; i < 32; i++)
    {
        if (v[i] % 3 == 1) tar ^= 1 << i;
    }
    return tar;
}

//剑指 Offer 56 - II. 数组中数字出现的次数 II——位运算，简化版
int singleNumber(vector<int>& nums) {
    int tar = 0, b = 0;
    for (int i = 0; i < 32; i++)
    {
        b = 0;
        for (auto it : nums)
            b += ((it >> i) & 1);
        tar ^= (b % 3) << i;
    }
    return tar;
}

//剑指 Offer 16. 数值的整数次方
double myPow(double x, int n) {
    if (x == 0) return 0;
    if (n == 1) return x;
    if (n > 0)
    {
        if (n % 2 == 1)
            return myPow(x * x, n / 2) * x;
        else return myPow(x * x, n / 2);
    }
    if (n == 0 || x == 1.0) return 1;
    else return 1.0 / (x * myPow(x, -(n + 1)));
}

//剑指 Offer 16. 数值的整数次方——简化版
double myPow(double x, int n) {
    if (x == 0) return 0;
    if (n > 0)
    {
        if (n == 1) return x;
        if (n % 2 == 1) return myPow(x * x, n / 2) * x;
        else return myPow(x * x, n / 2);
    }
    if (n == 0 || x == 1.0) return 1;
    else return 1.0 / (x * myPow(x, -(n + 1)));
}

int main()
{
    //vector<int> v;
    //int arr[4] = { 4,1,4,6 };
    //for (int i = 0; i < 4; i++)
    //{
    //    v.push_back(arr[i]);
    //}

    //singleNumbers(v);
    
    //cout << pow(2.00000, -2147483648) << endl;

    cout << myPow(2.0, -2) << endl;
	return 0;
}