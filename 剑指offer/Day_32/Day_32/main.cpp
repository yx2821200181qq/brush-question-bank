#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

ListNode* reverseList(ListNode* head)
{
    ListNode Thead;
    while (head)
    {
        ListNode* temp = head;
        if (!Thead.next)
        {
            Thead.next = temp;
            head = head->next;
            temp->next = nullptr;
        }
        else
        {
            head = head->next;
            temp->next = Thead.next;
            Thead.next = temp;
        }
    }
    return Thead.next;
}

ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    l1 = reverseList(l1);
    l2 = reverseList(l2);
    int num = 0;
    ListNode* cur1 = l1, * cur2 = l2;
    while (cur1 && cur2)
    {
        cur1->val = cur1->val + cur2->val + num;
        num = cur1->val / 10;
        cur1->val %= 10;
        if (cur1->next == nullptr && cur2->next != nullptr)
        {
            cur1->next = cur2->next;
            cur1 = cur1->next;
            break;
        }
        else if (cur1->next == cur2->next && num != 0)
        {
            cur1->next = new ListNode(num);
            num = 0;
        }
        cur1 = cur1->next;
        cur2 = cur2->next;
    }
    while (cur1)
    {
        cur1->val += num;
        num = cur1->val / 10;
        cur1->val %= 10;
        if (cur1->next == nullptr && num != 0)
        {
            cur1->next = new ListNode(num);
            break;
        }
        cur1 = cur1->next;
    }
    return reverseList(l1);
}

void reorderList(ListNode* head) {
    ListNode* left = head, * right = head;
    while (right && right->next)
    {
        left = left->next;
        if (!left->next) return;
        right = right->next->next;
    }
    if (!right) right = left;
    else
    {
        right = left->next;
        left->next = nullptr;
    }
    left = head;
    right = reverseList(right);
    while (right)
    {
        ListNode* temp = right;
        right = right->next;
        if (left->next == temp) left->next = nullptr;
        temp->next = left->next;
        left->next = temp;
        left = temp->next;
    }
}

int main()
{
    /*int a = 10;
    int b = 5;
    int const* p = &a;
    p = &b;
    cout << *p << endl;*/
    int i = 3;
    printf("%d %d", ++i, ++i);

    return 0;
}