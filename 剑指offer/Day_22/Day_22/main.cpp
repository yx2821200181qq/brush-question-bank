#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//求1+2+…+n——静态成员所有对象共用一份
class Sum {
public:
    Sum()
    {
        count += i;
        i++;
    }
    static int Print()
    {
        return count;
    }
    static int count;
    static int i;
};

int Sum::count = 0;
int Sum::i = 1;

class Solution {
public:
    int sumNums(int n) {
        Sum::count = 0;
        Sum::i = 1;
        Sum* arr = new Sum[n];
        return arr[0].Print();
    }
};

class Solution1 {
public:
    int sumNums(int n) {
        ret = 0;
        Sum(n);
        return ret;
    }
private:
    int ret;
    void Sum(int n)
    {
        if (n == 0) return;
        ret += n;
        Sum(n - 1);
    }
};

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

//查找二叉树两结点的公共父节点
void FindElement(TreeNode* root, TreeNode* p, TreeNode* q, int& num)
{
    if (!root) return;
    if (root == p || root == q) num++;
    FindElement(root->left, p, q, num);
    FindElement(root->right, p, q, num);
}
TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    while (true)
    {
        int left = 0, right = 0;
        FindElement(root->left, p, q, left);
        FindElement(root->right, p, q, right);
        if (left == right || (left == 0 && right == 1) || (right == 0 && left == 1)) return root;
        if (left == 2) root = root->left;
        else root = root->right;
    }
    return nullptr;
}

TreeNode* lowestCommonAncestor1(TreeNode* root, TreeNode* p, TreeNode* q) {
    if (!root || root->val == p->val || root->val == q->val) return root;
    while (true)
    {
        if (p->val > root->val && q->val > root->val) root = root->right;
        else if (p->val < root->val && q->val < root->val) root = root->left;
        else break;
    }
    return root;
}

TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    if (!root || root == p || root == q) return root;

    TreeNode* left = lowestCommonAncestor(root->left, p, q);
    TreeNode* right = lowestCommonAncestor(root->right, p, q);

    if (!right) return left;
    if (!left) return right;

    return root;
}