#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<queue>
#include<vector>
#include<list>
using namespace std;

class MaxQueue {
private:
    queue<int> _queue;
    list<int> _max;
public:
    MaxQueue() {}

    int max_value() {
        if (_queue.empty())
            return -1;
        return _max.front();
    }

    void push_back(int value) {
        _queue.push(value);
        while (!_max.empty() && _max.back() < value)
            _max.pop_back();
        _max.push_back(value);
    }

    int pop_front() {
        if (_queue.empty())
            return -1;
        int temp = _queue.front();
        if (temp == _max.front())
            _max.pop_front();
        _queue.pop();
        return temp;
    }
};

vector<int> maxSlidingWindow(vector<int>& nums, int k) {
    list<int> q;
    vector<int> v;
    for (int i = 0; i < k; i++)
    {
        if (q.empty())
        {
            q.push_back(nums[i]);
            continue;
        }
        while (!q.empty() && q.back() < nums[i])
        {
            q.pop_back();
        }
        q.push_back(nums[i]);
    }
    for (int i = 0; i <= nums.size() - k; i++)
    {
        v.push_back(q.front());
        if (nums[i] == q.front()) q.pop_front();
        if (i != nums.size() - k && (q.empty() || q.back() > nums[i+k]))
        {
            q.push_back(nums[i+k]);
            continue;
        }
        while (i != nums.size() - k && q.back() < nums[i + k])
        {
            q.pop_back();
            if (q.empty() || q.back() > nums[i+k]) q.push_back(nums[i+k]);
        }
    }
    return v;
}

int main()
{
    int arr[] = { 1,3,-1,-3,5,3,6,7 };
    vector<int> v;
    for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
    {
        v.push_back(arr[i]);
    }
    maxSlidingWindow(v,3);

    return 0;
}