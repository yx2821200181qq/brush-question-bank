#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<stack>
using namespace std;

class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node() {}
    Node(int _val) {
        val = _val;
        left = NULL;
        right = NULL;
    }
    Node(int _val, Node* _left, Node* _right) {
        val = _val;
        left = _left;
        right = _right;
    }
};
void Work(Node* root, Node*& head, Node*& tail)
{
    if (!root) return;
    Work(root->left, head, tail);
    if (!head)
    {
        head = root;
        tail = root;
    }
    else
    {
        tail->right = root;
        root->left = tail;
        tail = root;
    }
    Work(root->right, head, tail);
}
Node* treeToDoublyList(Node* root) {
    if (!root) return nullptr;
    Node* head = nullptr, * tail = nullptr;
    Work(root, head, tail);
    head->left = tail;
    tail->right = head;
    return head;
}
//void Work(Node*& head, Node* cur)
//{
//    if (!cur) return;
//    if (!cur->left)
//    {
//        if (head)
//        {
//            head->right = cur;
//            cur->left = head;
//        }
//        head = cur;
//        if (!cur->right) return;
//    }
//    Work(head, cur->left);
//    head->right = cur;
//    cur->left = head;
//    head = cur;
//    Work(head, cur->right);
//}
//Node* treeToDoublyList(Node* root) {
//    if (!root) return nullptr;
//    Node* head = nullptr;
//    Node* cur = root;
//    while (cur->left)
//    {
//        cur = cur->left;
//    }
//    head = cur;
//    cur = nullptr;
//    Work(cur, root);
//    cur = root;
//    while (cur->right)
//    {
//        cur = cur->right;
//    }
//    head->left = cur;
//    cur->right = head;
//    return head;
//}

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

TreeNode* Work(vector<int>& inorder, int inhead, int intail, vector<int>& postorder, int posthead, int posttail)
{
    if (inhead > intail) return nullptr;
    TreeNode* cur = new TreeNode(postorder[posttail]);
    if (inhead == intail) return cur;
    for (int i = inhead; i <= intail; i++)
    {
        if (inorder[i] == postorder[posttail])
        {
            cur->left = Work(inorder, inhead, i - 1, postorder, posthead, i - inhead - 1 + posthead);
            cur->right = Work(inorder, i + 1, intail, postorder, i - inhead + posthead, posttail - 1);
            break;
        }
    }
    return cur;
}
TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
    int postlen = postorder.size() - 1;
    int inlen = inorder.size() - 1;
    TreeNode* root = new TreeNode(postorder[postlen]);
    for (int i = 0; i < inorder.size(); i++)
    {
        if (inorder[i] == postorder[postlen])
        {
            root->left = Work(inorder, 0, i - 1, postorder, 0, i - 1);
            root->right = Work(inorder, i + 1, inlen, postorder, i, postlen - 1);
            break;
        }
    }
    return root;
}

//TreeNode* Work(vector<int>& preorder, int prehead, int pretail, vector<int>& inorder, int inhead, int intail) {
//    if (prehead > pretail || inhead > intail) return nullptr;
//    TreeNode* cur = new TreeNode(preorder[prehead]);
//    if (prehead == pretail) return cur;
//    for (int i = inhead; i <= intail; i++)
//    {
//        if (preorder[prehead] == inorder[i])
//        {
//            cur->left = Work(preorder, prehead + 1, prehead + i - inhead, inorder, inhead, i - 1);
//            cur->right = Work(preorder, prehead + 1 + i - inhead, pretail, inorder, i + 1, intail);
//            break;
//        }
//    }
//    return cur;
//}
//TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
//    TreeNode* root = new TreeNode(preorder[0]);
//    for (int i = 0; i < inorder.size(); i++)
//    {
//        if (preorder[0] == inorder[i])
//        {
//            root->left = Work(preorder, 1, i, inorder, 0, i - 1);
//            root->right = Work(preorder, 1 + i, preorder.size() - 1, inorder, i + 1, inorder.size() - 1);
//            break;
//        }
//    }
//    return root;
//}

vector<int> preorderTraversal(TreeNode* root) {
    stack<TreeNode*> s;
    vector<int> v;
    s.push(root);
    v.push_back(root->val);
    while (!s.empty())
    {
        TreeNode* cur = s.top();
        if (cur->left != nullptr)
        {
            s.push(cur->left);
            v.push_back(cur->left->val);
        }
        else if (cur->right != nullptr)
        {
            s.pop();
            s.push(cur->right);
            v.push_back(cur->right->val);
        }
        else
            s.pop();
    }
    return v;
}

//非递归前序遍历二叉树：方法1
vector<int> preorderTraversal(TreeNode* root) {
    if (!root) return vector<int>();
    stack<TreeNode*> s;
    TreeNode* flag;
    vector<int> v;
    s.push(root);
    v.push_back(root->val);
    while (!s.empty())
    {
        TreeNode* cur = s.top();
        if (cur->left != nullptr && cur->left != flag && cur->right != flag)
        {
            s.push(cur->left);
            v.push_back(cur->left->val);
        }
        else if (cur->right != nullptr && cur->right != flag)
        {
            s.push(cur->right);
            v.push_back(cur->right->val);
        }
        else
        {
            flag = s.top();
            s.pop();
        }
    }
    return v;
}

//非递归前序遍历二叉树：方法2
vector<int> preorderTraversal(TreeNode* root) {
    if (!root) return vector<int>();
    stack<TreeNode*> st;
    vector<int> v;
    TreeNode* cur = root;
    while (true)
    {
        while (cur)
        {
            st.push(cur);
            v.push_back(cur->val);
            cur = cur->left;
        }
        cur = st.top();
        st.pop();
        cur = cur->right;
        if (cur) continue;
        if (st.empty()) break;
    }
    return v;
}

//非递归中序遍历二叉树：方法1
vector<int> inorderTraversal(TreeNode* root) {
    if (!root) return vector<int>();
    vector<int> v;
    stack<TreeNode*> s;
    s.push(root);
    TreeNode* flag = root;
    while (!s.empty())
    {
        TreeNode* cur = s.top();
        if (cur->left != nullptr && cur->left != flag && cur->right != flag)
        {
            s.push(cur->left);
        }
        else if (cur->right != nullptr && cur->right != flag)
        {
            v.push_back(cur->val);
            s.push(cur->right);
        }
        else
        {
            s.pop();
            if (cur->right != flag)
            {
                cout << cur->val << endl;
                v.push_back(cur->val);
            }
            flag = cur;
        }
    }
    return v;
}

//非递归中序遍历二叉树：方法2
vector<int> inorderTraversal(TreeNode* root) {
    if (!root) return vector<int>();
    stack<TreeNode*> st;
    vector<int> v;
    TreeNode* cur = root;
    while (true)
    {
        while (cur)
        {
            st.push(cur);
            cur = cur->left;
        }
        cur = st.top();
        v.push_back(cur->val);
        st.pop();
        cur = cur->right;
        if (cur) continue;
        if (st.empty()) break;
    }
    return v;
}

//后序遍历非递归
vector<int> postorderTraversal(TreeNode* root) {
    if (!root) return vector<int>();
    stack<TreeNode*> st;
    vector<int> v;
    TreeNode* flag = root;
    TreeNode* cur = root;
    while (cur || st.empty())
    {
        while (cur)
        {
            st.push(cur);
            cur = cur->left;
        }
        TreeNode* top = st.top();
        while ((!top->left && !top->right) || (!top->right && flag == top->left) || (top->right == flag))
        {
            flag = st.top();
            v.push_back(flag->val);
            st.pop();
            if (st.empty()) break;
            top = st.top();
        }
        if (st.empty()) break;
        cur = top->right;
    }
    return v;
}

int main()
{
    //Node a(1);
    //treeToDoublyList(&a);
    vector<int> postorder, inorder;
    int arr1[5] = { 9,15,7,20,3 };
    int arr2[5] = { 9,3,15,20,7 };
    for (int i = 0; i < 5; i++)
    {
        postorder.push_back(arr1[i]);
        inorder.push_back(arr2[i]);
    }
    buildTree(inorder, postorder);

	return 0;
}