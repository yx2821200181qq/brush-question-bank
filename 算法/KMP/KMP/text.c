#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>

/*
* str 代表主串
* sub 代表子串
* pos 代表从主串的pos位置开始找
*/

void GetNext(char* sub, int* next)
{
	next[0] = -1;
	next[1] = 0;
	int lenSub = strlen(sub);
	int i = 2;//当前i下标
	int k = 0;//前一项的k

	while (i < lenSub)
	{
		if (k == -1 || sub[i - 1] == sub[k])
		{
			next[i] = k + 1;
			i++;
			k++;
		}
		else
		{
			k = next[k];
		}
	}

	//for (; i < lenSub; i++)
	//{
	//	if (sub[i - 1] == sub[k])
	//	{
	//		next[i] = k + 1;
	//		k++;
	//	}
	//	else
	//	{
	//		if (k == 0)
	//		{
	//			next[i] = k;
	//		}
	//		else
	//		{
	//			k = next[k];
	//			next[i] = k;
	//		}
	//	}
	//}
}

int KMP(char* str1, char* sub,int pos)
{
	assert(str1 && sub);
	int lenStr = strlen(str1);
	int lenSub = strlen(sub);
	if (lenSub == 0 || lenStr == 0) return -1;
	if (pos < 0 || pos>lenStr) return -1;

	int* next = (int*)malloc(sizeof(int) * lenSub);
	assert(next);

	GetNext(sub, next);

	int i = pos;//遍历主串
	int j = 0;//遍历子串
	
	while (i < lenStr && j < lenSub)
	{
		if (j==-1 || str1[i] == sub[j])
		{
			i++;
			j++;
		}
		else
		{
			j = next[j];
		}
	}
	if (j >= lenSub) return i - j;
	return -1;
}

int main()
{
	printf("%d\n", KMP("ababcabcdabcde", "abcd", 0));
	printf("%d\n", KMP("ababcabcdabcde", "abcdf", 0));
	
	return 0;
}