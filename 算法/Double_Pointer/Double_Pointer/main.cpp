#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int left = -1;
        for (int i = 0; i < nums.size(); i++)
        {
            if (nums[i])
            {
                swap(nums[i], nums[++left]);
            }
        }
    }
    void duplicateZeros(vector<int>& arr) {
        int cur = 0, dest = -1;
        for (int i = 0; i < arr.size(); i++)
        {
            if (arr[i] == 0)
            {
                dest += 2;
            }
            else dest++;
            if (dest >= arr.size() - 1)
            {
                cur = i;
                break;
            }
        }
        if (dest == arr.size())
        {
            arr[dest - 1] = 0;
            cur--;
            dest -= 2;
        }
        while (cur != dest)
        {
            if (arr[cur])
            {
                arr[dest--] = arr[cur--];
            }
            else
            {
                arr[dest--] = 0;
                arr[dest--] = 0;
                cur--;
            }
        }
    }
};

int main()
{
    Solution s;
    vector<int> v = { 1,0,2,3,0,4,5,0 };
    s.duplicateZeros(v);
    for (auto it : v)
    {
        cout << it << " ";
    }

	return 0;
}