#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

int arr[1001][1001] = { 0 };
int min = 2001;
int sum = 0;

void DFS(int u,int v,int n)
{
	if (u == v && sum < min)
	{
		min = sum;
		return;
	}
	for (int i = 1; i <= n; i++)
	{
		if (arr[u][i] == 1)
		{
			sum++;
			DFS(i, v, n);
			sum--;
		}
	}
}

int main()
{
	int n = 0, m = 0;
	scanf("%d %d", &n, &m);
	int a = 0, b = 0;
	for (int i = 0; i < m; i++)
	{
		scanf("%d %d", &a, &b);
		arr[a][b] = 1;
		arr[b][a] = 1;
	}
	int u = 0, v = 0;
	scanf("%d %d", &u, &v);
	DFS(u,v,n);
	if (min == 2001)
	{
		printf("-1");
		return 0;
	}
	printf("%d", min);

	return 0;
}