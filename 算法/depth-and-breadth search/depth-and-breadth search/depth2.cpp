#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stdio.h>
#include<vector>
using namespace std;

vector<int> son[10005];
bool f[10005];
int ans[1005];

int DFS(int u)
{
	int ret = 0;
	for (int i = 0; i < son[u].size(); i++)
	{
		ret += DFS(son[u][i]);
	}
	ans[u] = ret;
	return ret + 1;
}

int main()
{
	int n, x, y, u;
	scanf("%d", &n);
	for (int i = 0; i < n-1; i++)
	{
		scanf("%d %d", &x, &y);
		son[x].push_back(y);
		f[y] = true;
	}

	for (int i = 1; i <= n; i++)
	{
		if (!f[i])
		{
			u = i;
		}
	}
	DFS(u);

	for (int i = 1; i <= n; i++)
	{
		printf("%d\n", ans[i]);
	}

	return 0;
}