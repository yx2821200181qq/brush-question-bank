#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

//class Date
//{
//public:
//	void Init(int year)
//	{
//		Date::year = year;
//	}
//private:
//	int year;
//};

//#define N 9
//int arr[N] = { 0 };//0表示还没放数
//int flagArr[N + 1] = { 0 };//1表示用过。0表示没有用过
//
//void dfs(int n, int i)
//{
//    if (n == i)//边界
//    {
//        for (int i = 0; i < n; i++)
//            printf("%d ", arr[i]);
//        printf("\n");
//        return;
//    }
//    //枚举每个分支，即当前位置可以填充那些数
//    for (int j = 1; j <= n; j++)
//    {
//        if (flagArr[j] == 0)
//        {
//            //处理
//            arr[i++] = j;
//            flagArr[j] = 1;
//            dfs(n, i);
//
//            //恢复现场
//            flagArr[j] = 0;
//            arr[--i] = 0;
//        }
//    }
//}
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int i = 0;
//    dfs(n, i);
//
//    return 0;
//}

//int n = 0;
//int m = 0;
//const int N = 25;
//int arr[N+1] = { 0 };
//int flagArr[N + 1] = { 0 };
//
//void df(int cur,int flag)
//{
//    if (cur == m+1)
//    {
//        for (int i = 1; i <= m; i++)
//            printf("%d ", arr[i]);
//        printf("\n");
//        return;
//    }
//    for (int i = flag; i <= n; i++)
//    {
//        if (flagArr[i] == 0)
//        {
//            arr[cur] = i;
//            flagArr[i] = 1;
//            df(cur + 1,i+1);
//            flagArr[i] = 0;
//        }
//    }
//}
//
//int main()
//{
//    scanf("%d%d", &n, &m);
//    int cur = 1;
//    df(cur,1);
//
//    return 0;
//}

//const int N = 10;
//int n = 0;
//int arr[N] = { 0,8,1,5,6,4,3,2,9,7 };
//int flagArr[N] = { 0 };
//int _count = 0;
//
//void df2(int cur)
//{
//    int num = 0;
//    for (int i = 1; i <= cur; i++)
//    {
//        num = num * 10 + arr[i];
//        if (num > n)
//            return;
//    }
//    num = n - num;
//    for (int i = cur + 1; i < N-1; i++)
//    {
//        int num1 = 0, num2 = 0;
//        for (int j = cur + 1; j <= i; j++)
//        {
//            num1 = num1 * 10 + arr[j];
//        }
//        for (int j = i + 1; j < N; j++)
//        {
//            num2 = num2 * 10 + arr[j];
//        }
//        if (num1 % num2 == 0 && num1 / num2 == num)
//        {
//            printf("%d %d %d\n",n-num, num1, num2);
//            _count++;
//            return;
//        }
//        if (num1 / num2 > num)
//            df2(cur + 1);
//    }
//}
//
//void df1(int cur)
//{
//    if (cur == N)
//    {
//        df2(1);
//        return;
//    }
//    for (int i = 1; i < N; i++)
//    {
//        if (flagArr[i] == 0)
//        {
//            arr[cur] = i;
//            flagArr[i] = 1;
//            df1(cur + 1);
//            flagArr[i] = 0;
//        }
//    }
//}
//
//int main()
//{
//    _count = 0;
//    scanf("%d", &n);
//    int cur = 1;
//    df1(cur);
//    printf("%d", _count);
//    return 0;
//}

const int N = 10;
int n = 0;
int arr[N] = { 0 };
int flagArr[N] = { 0 };
int _count = 0;

int Add(int L, int R)
{
    int num = 0;
    for (int i = L; i <= R; i++)
    {
        num = num * 10 + arr[i];
    }
    return num;
}

void df1(int cur)
{
    if (cur == N)
    {
        for (int i = 1; i < 8; i++)
        {
            for (int j = i + 1; j <= 8; j++)
            {
                int a = Add(1, i);
                int b = Add(i + 1, j);
                int c = Add(j + 1, 9);
                if (a * c + b == c * n)
                    _count++;
            }
        }
    }
    for (int i = 1; i < N; i++)
    {
        if (flagArr[i] == 0)
        {
            arr[cur] = i;
            flagArr[i] = 1;
            df1(cur + 1);
            flagArr[i] = 0;
        }
    }
}

int main()
{
    _count = 0;
    scanf("%d", &n);
    int cur = 1;
    df1(cur);
    printf("%d", _count);
    return 0;
}