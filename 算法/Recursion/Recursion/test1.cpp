#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

char LightArr[10][10];
int dx[5] = { 0,-1,0,1,0 }, dy[5] = { 0,0,1,0,-1 };

int min(int a, int b)
{
    return (a > b) ? b : a;
}

void turn(int x, int y)
{
    for (int i = 0; i < 5; i++)
    {
        int a = x + dx[i], b = y + dy[i];
        if (a >= 0 && a < 5 && b >= 0 && b < 5)
        {
            LightArr[a][b] ^= 1;
        }
    }
}

int work()
{
    int res = 1000;
    for (int cur = 0; cur < 1 << 5; cur++)//共32种可能
    {
        char tempArr[10][10];
        int count = 0;
        memcpy(tempArr, LightArr, sizeof(LightArr));//将原来的数据保存
        for (int i = 0; i < 5; i++)//第一行的变换
        {
            if (cur >> i & 1)
            {
                count++;
                turn(0, i);
            }
        }
        for (int i = 1; i < 5; i++)//剩下的四行
        {
            for (int j = 0; j < 5; j++)
            {
                if (LightArr[i - 1][j] == '0')
                {
                    count++;
                    turn(i, j);
                }
            }
        }
        bool flag = false;
        for (int i = 0; i < 5; i++)
        {
            if (LightArr[4][i] == '0')
            {
                flag = true;
                break;
            }
        }
        if (!flag)
            res = min(count, res);
        memcpy(LightArr, tempArr, sizeof(LightArr));
    }
    if (res > 6)
        res = -1;
    return res;
}

int main()
{
    int N;
    scanf("%d", &N);
    for (int i = 0; i < N; i++)
    {
        //for (int x = 0; x < 5; x++)
        //{
        //    cin >> LightArr[x];
        //}
        for (int x = 0; x < 5; x++)
        {
                scanf("%s", LightArr[x]);
        }
        printf("%d\n", work());
    }

    return 0;
}

//int main()
//{
//	int a = 16;
//	int b = a >> 1;
//	printf("%d %d", a, b);
//	return 0;
//}

//using namespcae std;
//int N = 0;
//int Arr[5][5] = { 0 };
//int count = 0;
//
//int L[] = { -1,0,1,0 };
//int F[] = { 0,1,0,-1 };
//
//void Alter(int l, int f)
//{
//    Arr[l][f] ^= 1;
//    for (int i = 0; i < 4; i++)
//    {
//        Arr[l + L[i]][f + F[i]] ^= 1;
//    }
//
//}
//
//void Judge()
//{
//    for (int i = 1; i < 5; i++)
//    {
//        for (int j = 0; j < 5; j++)
//        {
//            if (Arr[i - 1][j] == 0)
//            {
//                Alter(i, j);
//                count++;
//                if (count > 6)
//                {
//
//                }
//            }
//        }
//    }
//}
//
//void fbs(int cur)
//{
//    if (cur == 5)
//    {
//        Judge();
//    }
//
//    for (int i = 0; i < 2; i++)
//    {
//        if (i == 0)
//            fbs(cur + 1);
//        else
//        {
//            Arr[0][cur] ^= 1;
//            count++;
//            fbs(cur + 1);
//        }
//    }
//}
//
//int main()
//{
//    scanf("%d", &N);
//    for (int i = 0; i < N; i++)//将N个数组分别求解
//    {
//        for (int i = 0; i < 5; i++)
//        {
//            for (int j = 0; j < 5; j++)
//            {
//                scanf("%d", &Arr[i][j]);
//            }
//        }
//        fbs(0);
//    }
//
//    return 0;
//}

//int N = 0;

//void dfs(int a1, int a2, int cur)
//{
//    if (cur < N)
//        printf("%d ", a1 + a2);
//    else
//        return;
//    dfs(a2, a1 + a2, cur + 1);
//}

//int dfs(int a1, int a2, int cur)
//{
//    if (cur == N)
//        return 0;
//    printf("%d ", a1 + a2);
//    dfs(a2, a1 + a2, cur + 1);
//}
//
//int main()
//{
//    int cur = 2;
//    scanf("%d", &N);
//    if (N == 1)
//        printf("0 ");
//    if (N >= 2)
//        printf("0 1 ");
//    if (N > 2)
//        dfs(0, 1, cur);
//
//    return 0;
//}

//int N = 0;

//void dfs(int a1, int a2, int cur)
//{
//    if (cur < N)
//        printf("%d ", a1 + a2);
//    else
//        return;
//    dfs(a2, a1 + a2, cur + 1);
//}
//
//int main()
//{
//    int cur = 2;
//    scanf("%d", &N);
//    if (N == 1)
//        printf("0 ");
//    if (N >= 2)
//        printf("0 1 ");
//    if(N>2)
//        dfs(0, 1, cur);
//
//    return 0;
//}

//const int N = 10;
//int n = 0;
//int flagArr[N] = { 0 };
//int _count = 0;
//
//int Add(int* arr,int L, int R)
//{
//    int num = 0;
//    for (int i = L; i <= R; i++)
//    {
//        num = num * 10 + arr[i];
//    }
//    return num;
//}
//
//void test(int* arr)
//{
//    for (int i = 1; i < 8; i++)
//    {
//        for (int j = i + 1; j <= 8; j++)
//        {
//            int a = Add(arr,1, i);
//            if (a > n)
//                return;
//            int b = Add(arr,i + 1, j);
//            int c = Add(arr,j + 1, 9);
//            //printf("%d %d %d\n", a, b, c);
//            if (b > (c * n))
//            {
//                printf("%d %d %d\n", a, b, c);
//                break;
//            }
//            if (a * c + b == c * n)
//            {
//                printf("%d %d %d\n", a, b, c);
//                _count++;
//            }
//        }
//    }
//}
//
//int main()
//{
//    n = 468;
//    //int arr[10] = { 0,9,5,7,8,3,4,1,2,6 };
//    printf("%d", 7834126 * n);
//    //test(arr);
//    return 0;
//}

//3 5 9 7 4 1 2 6 8
//3 7 2 4 8 9 6 5 1
//3 9 6 5 1 8 4 7 2
//4 6 5 3 8 7 1 2 9
//4 6 5 8 1 9 2 7 3
//4 6 5 8 7 3 2 9 1
//4 6 5 9 8 1 3 2 7
//9 5 2 6 4 8 3 7 1
//9 5 7 8 3 4 1 2 6
//9 7 4 3 5 8 1 6 2