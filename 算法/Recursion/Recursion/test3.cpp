#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

int n;
int arr[20];

void dfs(int cur)
{
	if (cur > n)
	{
		for (int i = 1; i <= n; i++)
		{
			if (arr[i] == 1)
				printf("%d ", i);
		}
		printf("\n");
		return;
	}

	arr[cur] = 1;
	dfs(cur + 1);

	arr[cur] = 0;
	dfs(cur + 1);
}

int main()
{
	scanf("%d", &n);

	int cur = 1;
	dfs(cur);

	return 0;
}