#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>
using namespace std;

typedef pair<int, int> PII;
const int N = 1010;
char arr[N][N];
bool Judge1[N][N];
bool Judge2[N][N];
int n, res;

int dx[4] = { -1,1,0,0 };
int dy[4] = { 0,0,-1,1 };

void Swap(int x, int y)
{
    for (int i = 0; i < 4; i++)
    {
        int sx = x + dx[i];
        int sy = y + dy[i];
        if (sx >= 0 && sy >= 0 && sx < n && sy < n && arr[sx][sy] == '.')
        {
            arr[x][y] = '0';
            return;
        }
    }
}

void BFS_alter(PII start)
{
    queue<PII> q;
    q.push(start);
    Swap(start.first, start.second);
    Judge1[start.first][start.second] = true;
    while (!q.empty())
    {
        PII flag = q.front();
        q.pop();
        for (int i = 0; i < 4; i++)
        {
            int x = flag.first + dx[i];
            int y = flag.second + dy[i];
            if (x >= 0 && y >= 0 && x < n && y < n && arr[x][y] != '.' && Judge1[x][y] != true)
            {
                PII temp = { x,y };
                q.push(temp);
                Judge1[x][y] = true;
                Swap(x, y);
            }
        }
    }
}

void BFS_find(PII start)
{
    queue<PII> q;
    q.push(start);
    Judge2[start.first][start.second] = true;
    arr[start.first][start.second] = '#';
    bool a = true;
    while (!q.empty())
    {
        PII flag = q.front();
        q.pop();
        for (int i = 0; i < 4; i++)
        {
            int x = flag.first + dx[i];
            int y = flag.second + dy[i];
            if (x >= 0 && y >= 0 && x < n && y < n && arr[x][y] != '.' && Judge2[x][y] != true)
            {
                PII temp = { x,y };
                q.push(temp);
                Judge2[x][y] = true;
                if (arr[x][y] == '#' && a)
                {
                    res++;
                    a = false;
                }
                arr[x][y] = '#';
            }
        }
    }
}

int main()
{
    // 请在此输入您的代码
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        scanf("%s", arr[i]);
    }
    PII start;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (arr[i][j] == '#')
            {
                start = { i,j };
                BFS_alter(start);
            }
        }
    }
    cout << endl;
    for (int i = 0; i < n; i++)
    {
        printf("%s\n", arr[i]);
        //for (int j = 0; j < n; j++)
        //{
        //    /*if (arr[i][j] == '0')
        //    {
        //        start = { i,j };
        //        BFS_find(start);
        //    }*/
        //    
        //}
    }

    cout << res << endl;

    return 0;
}

//typedef pair<int, int> PII;
//
//int w, h;
//const int N = 25;
//char arr[N][N];
//
//int dx[4] = { -1,1,0,0 };
//int dy[4] = { 0,0,-1,1 };
//
//void BFS(PII start)
//{
//	queue<PII> q;
//	q.push(start);
//	arr[start.first][start.second] = '1';
//	while (!q.empty())
//	{
//		PII flag = q.front();
//		q.pop();
//		for (int i = 0; i < 4; i++)
//		{
//			int x = flag.first + dx[i];
//			int y = flag.second + dy[i];
//			if (x >= 0 && y >= 0 && x < h && y < w && arr[x][y] != '#' && arr[x][y] != '1')
//			{
//				PII temp = { x,y };
//				arr[x][y] = '1';
//				q.push(temp);
//			}
//		}
//	}
//}
//
//int Num()
//{
//	int res = 0;
//	for (int i = 0; i < h; i++)
//	{
//		for (int j = 0; j < w; j++)
//		{
//			if (arr[i][j] == '1')
//				res++;
//		}
//	}
//	return res;
//}
//
//int main()
//{
//	cin >> w >> h;
//	while (w != 0 && h != 0)
//	{
//		for (int i = 0; i < h; i++)
//		{
//			scanf("%s", arr[i]);
//		}
//		PII start;
//		for (int i = 0; i < h; i++)
//		{
//			for (int j = 0; j < w; j++)
//			{
//				if (arr[i][j] == '@')
//				{
//					start = { i,j };
//					BFS(start);
//					int distance = Num();
//					cout << distance << endl;
//				}
//			}
//		}
//		cin >> w >> h;
//	}
//
//	return 0;
//}

//typedef struct node {
//    int z;
//    int x;
//    int y;
//};
//
//int L, R, C;
//const int N = 110;
//char arr[N][N][N];
//int dis[N][N][N];
//
//int dz[3] = { 1,-1 };
//int dx[4] = { -1,1,0,0 };
//int dy[4] = { 0,0,-1,1 };
//
//int DFS(node start, node end)
//{
//    queue<node> q;
//    q.push(start);
//    memset(dis, -1, sizeof(dis));
//    dis[start.z][start.x][start.y] = 0;
//
//    while (!q.empty())
//    {
//        node temp = q.front();
//        q.pop();
//        for (int i = 0; i < 4; i++)
//        {
//            int z = temp.z;
//            int x = temp.x + dx[i];
//            int y = temp.y + dy[i];
//            if (x >= 0 && y >= 0 && x < R && y < C && dis[z][x][y] == -1 && arr[z][x][y] != '#')
//            {
//                node flag = { z,x,y };
//                q.push(flag);
//                dis[z][x][y] = dis[temp.z][temp.x][temp.y] + 1;
//                if (z == end.z && x == end.x && y == end.y)
//                    return dis[z][x][y];
//            }
//        }
//        for (int i = 0; i < 2; i++)
//        {
//            int z = temp.z + dz[i];
//            int x = temp.x;
//            int y = temp.y;
//            if (z < L && z >= 0 && dis[z][x][y] == -1 && arr[z][x][y] != '#')
//            {
//                node flag = { z,x,y };
//                q.push(flag);
//                dis[z][x][y] = dis[temp.z][temp.x][temp.y] + 1;
//                if (z == end.z && x == end.x && y == end.y)
//                    return dis[z][x][y];
//            }
//        }
//    }
//    return -1;
//}
//
//int main()
//{
//    cin >> L >> R >> C;
//    while (L != 0 && R != 0 && C != 0)
//    {
//        for (int i = 0; i < L; i++)
//        {
//            for (int j = 0; j < R; j++)
//            {
//                scanf("%s", arr[i][j]);
//            }
//        }
//        node start, end;
//        for (int i = 0; i < L; i++)
//        {
//            for (int j = 0; j < R; j++)
//            {
//                for (int k = 0; k < C; k++)
//                {
//                    if (arr[i][j][k] == 'S')
//                        start = { i,j,k };
//                    if (arr[i][j][k] == 'E')
//                        end = { i,j,k };
//                }
//            }
//        }
//        int distance = DFS(start,end);
//        if (distance == -1) cout << "Trapped!" << endl;
//        else cout << "Escaped in " << distance << " minute(s)." << endl;
//        cin >> L >> R >> C;
//    }
//
//    return 0;
//}

//int N = 1e5;
//int main()
//{
//	int a = 1;
//	int res = 0;
//	int i = 1;
//	for (; res <= N; i++)
//	{
//		res += a;
//		a *= 2;
//		cout << a << " " << res << endl;
//	}
//	cout << i << endl;
//	return 0;
//}

//const int N = 1e4 + 10;
//int n;
//int arr[N];
//int res;
//
//bool Judge()
//{
//    for (int i = 1; i <= n; i++)
//    {
//        if (arr[i] != i)
//            return true;
//    }
//    return false;
//}

//void Work()
//{
//    int flag = n;
//    for (int i = 1; i <= n; i++)
//    {
//        if (arr[i] != i)
//        {
//            swap(arr[i], arr[arr[i]]);
//            res++;
//            i--;
//        }
//    }
//}

//void Work()
//{
//    int flag = n;
//    while (Judge())
//    {
//        for (int i = flag; i > 0; i--)
//        {
//            if (arr[i] == flag)
//            {
//                for (int j = i; j < flag; j++)
//                {
//                    arr[j] = arr[j + 1];
//                    res++;
//                }
//                arr[flag] = flag;
//                break;
//            }
//        }
//        flag--;
//    }
//}

//void Work()
//{
//    int flag = n;
//    while (Judge())
//    {
//        for (int i = 1; i <= n; i++)
//        {
//            if (arr[i] != i)
//            {
//                swap(arr[i], arr[arr[i]]);
//                res++;
//            }
//        }
//    }
//}
//
//int main()
//{
//    // 请在此输入您的代码
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    Work();
//    cout << res << endl;
//
//    return 0;
//}

//typedef pair<int, int> PII;
//
//const int N = 210;
//int t, r, c;
//char arr[N][N];
//int dis[N][N];
//
//int dx[4] = { -1,1,0,0 };
//int dy[4] = { 0,0,-1,1 };
//
//int pfs(PII start, PII end)
//{
//	queue<PII> q;
//	q.push(start);
//	memset(dis, -1, sizeof(dis));
//	dis[start.first][start.second] = 0;
//	
//	while (!q.empty())
//	{
//		PII front = q.front();
//		q.pop();
//		for (int i = 0; i < 4; i++)
//		{
//			int x = front.first + dx[i];
//			int y = front.second + dy[i];
//			if (x >= 0 && x < r && y>=0 && y < c && arr[x][y] != '#' && dis[x][y] == -1 )
//			{
//				PII temp = { x,y };
//				q.push(temp);
//				dis[x][y] = dis[front.first][front.second] + 1;
//				if (x == end.first && y == end.second)
//					return dis[x][y];
//			}
//		}
//	}
//	return -1;
//}
//
//int main()
//{
//	cin >> t;
//	while (t--)
//	{
//		cin >> r >> c;
//		for (int i = 0; i < r; i++)
//		{
//			scanf("%s", arr[i]);
//		}
//		PII start, end;
//		for (int i = 0; i < r; i++)
//		{
//			for (int j = 0; j < c; j++)
//			{
//				if (arr[i][j] == 'S')
//					start = { i,j };
//				if (arr[i][j] == 'E')
//					end = { i,j };
//			}
//		}
//		int distance = pfs(start, end);
//		if (distance == -1) cout << "oop!" << endl;
//		else cout << distance << endl;
//	}
//
//	return 0;
//}