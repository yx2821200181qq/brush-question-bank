#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;

const int N = 1e5 + 10;
int n, m, t;
int last[N];//上一次出现的时间
bool range[N];//是否到达优先级
int Num[N];//现在的等级

typedef struct date {
    int time;
    int num;
};
date arr[N];

int compare(const void* a1, const void* a2)
{
    return (*(date*)a1).time - (*(date*)a2).time;
}

int main()
{
    cin >> n >> m >> t;
    for (int i = 1; i <= m; i++)
    {
        scanf("%d %d", &(arr[i].time), &(arr[i].num));
    }
    qsort(arr, m+1, sizeof(arr[0]), compare);
    printf("-----------------------------------\n");
    for (int i = 1; i <= m; i++)
    {
        printf("%d %d\n", arr[i].time, arr[i].num);
    }

    return 0;
}

//int main()
//{
//	int a = 20;
//	long long res = 1;
//	for (int i = 1; i <= a; i++)
//	{
//		res *= i;
//	}
//	printf("%d", res);
//	return 0;
//}

//const int N = 100010;
//int n, m;
//int arr[N];
//
//int compare_down(const void* a1, const void* a2)
//{
//    return *(int*)a1 < *(int*)a2;
//}
//int compare_up(const void* a1, const void* a2)
//{
//    return *(int*)a1 > *(int*)a2;
//}
//
//int main()
//{
//    // 请在此输入您的代码
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//    {
//        arr[i] = i;
//    }
//    for (int i = 0; i < m; i++)
//    {
//        int a, b;
//        cin >> a >> b;
//        if (a == 0)
//        {
//            qsort(arr + 1, b, 4, compare_down);
//        }
//        else
//        {
//            qsort(arr + b, n - b + 1, 4, compare_up);
//        }
//    }
//    for (int i = 1; i <= n; i++)
//    {
//        cout << arr[i] << " ";
//    }
//    cout << endl;
//    return 0;
//}

//int gcd(int x, int y)
//{
//    return !y ? x : gcd(y, x % y);
//}
//int main()
//{
//    int f[2022];
//    memset(f, 0, sizeof f);
//    for (int i = 1; i <= 2021; i++) {
//        for (int j = i + 1; j <= i + 21; j++) {
//            if (j > 2021)
//                break;
//            if (f[j] == 0)
//                f[j] = f[i] + j * i / gcd(i, j);
//            else
//                f[j] = min(f[j], f[i] + j * i / gcd(i, j));
//        }
//    }
//    cout << f[2021] << endl;
//}

//10266837

//const int N = 2030;
//int f[N];
//
//int work(int x, int y)
//{
//	int a = 1;
//	while (1)
//	{
//		if (x * a % y == 0)
//			return a * x;
//		a++;
//	}
//	return -1;
//}
//
//int main()
//{
//	f[1] = 1;
//	for (int i = 2; i <= 22; i++)
//	{
//		int count = 1;
//		while (count != i)
//		{
//			if (1 == count)
//				f[i] = i;
//			else
//				f[i] = min(f[i], f[count] + work(i, count));
//			count++;
//		}
//	}
//	for (int i = 23; i <= 2021; i++)
//	{
//		for (int j = i - 1; j >= i - 21; j--)
//		{
//			if (f[i] == 0)
//				f[i] = f[j] + work(i, j);
//			else
//				f[i] = min(f[i], f[j] + work(i, j));
//		}
//	}
//	cout << f[2021] << endl;
//
//	return 0;
//}