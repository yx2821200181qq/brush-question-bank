#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<string>
#include<set>
using namespace std;

const int N = 1e5 + 10;
int n, m, t;
int last[N];//上一次出现的时间
bool range[N];//是否到达优先级
int Num[N];//现在的等级

struct date {
    int time;
    int num;
};
date arr[N];

int compare(const void* a1, const void* a2)
{
    return (*(date*)a1).time - (*(date*)a2).time;
}

int main()
{
    cin >> n >> m >> t;
    for (int i = 1; i <= m; i++)
    {
        scanf("%d %d", &(arr[i].time), &(arr[i].num));
    }
    qsort(arr + 1, m, sizeof(arr[0]), compare);
    cout << "------------" << endl;
    for (int i = 1; i <= m; i++)
    {
        printf("%d %d\n", (arr[i].time), (arr[i].num));
    }

    return 0;
}