#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;



//typedef long long LL;
//LL x, y;
//LL res;
//
//LL Work(LL n)
//{
//    LL num = 0;
//    for (LL i = 1; i <= n; i++)
//    {
//        num += (i * 2 - 1) * 2 + (i * 2) * 2;
//    }
//    return num;
//}
//
//int main()
//{
//    // 请在此输入您的代码
//    cin >> x >> y;
//    LL temp_X = abs(x);
//    LL temp_y = abs(y);
//    LL Max = max(temp_y, temp_X);
//    if (Max > 1 && y < 0 && (-1 * (Max - 1) > y))
//    {
//        res += Work(Max);
//        Max++;
//    }
//    else if (Max > 1)
//        res += Work(Max - 1);
//    if (x >= -1 * Max && y == -1 * (Max - 1))
//    {
//        res += Max * 2 - 1 - (x - (-1 * Max));
//    }
//    if (-1 * Max == x && Max >= y)
//    {
//        res += (Max * 2 - 1) * 2 - (Max - y);
//    }
//    if (Max >= x && Max == y)
//    {
//        res += (Max * 2 - 1) * 2 + Max * 2 - (Max - x);
//    }
//    if (Max == x && -1 * Max <= y)
//    {
//        res += (Max * 2 - 1 + Max * 2) * 2 - (y - (-1 * Max));
//    }
//
//    cout << res << endl;
//
//    return 0;
//}

//typedef long long LL;
//LL x, y;
//LL res;
//
//LL Work(LL n)
//{
//    LL num = 0;
//    for (LL i = 1; i <= n; i++)
//    {
//        num += (i * 2 - 1) * 2 + (i * 2) * 2;
//    }
//    return num;
//}

//int main()
//{
//    // 请在此输入您的代码
//    cin >> x >> y;
//    LL temp_X = abs(x);
//    LL temp_y = abs(y);
//    LL Max = max(temp_y, temp_X);
//    if (Max > 1 && y < 0 && (-1 * (Max - 1) > y))
//    {
//        res += Work(Max);
//        Max++;
//    }
//    else if (Max > 1)
//        res += Work(Max - 1);
//    if (x >= -1 * Max && y == -1 * (Max - 1))
//    {
//        res += Max * 2 - 1 - (x - (-1 * Max));
//    }
//    if (-1 * Max == x && Max >= y)
//    {
//        res += (Max * 2 - 1) * 2 - Max - y;
//    }
//    if (Max >= x && Max == y)
//    {
//        res += (Max * 2 - 1) * 2 + Max * 2 - Max - x;
//    }
//    if (Max == x && -1 * Max <= y)
//    {
//        res += (Max * 2 - 1 + Max * 2) * 2 - x - (-1 * Max);
//    }
//
//    cout << res << endl;
//
//    return 0;
//}

//const int N = 110;
//const int M = 100 * 100000 + 10;
//int n;
//int w[N];
//bool f[N][M];
//
//int main()
//{
//    // 请在此输入您的代码
//    cin >> n;
//    int sum = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> w[i];
//        sum += w[i];
//        f[i][w[i]] = true;
//    }
//    
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= sum; j++)
//        {
//            if (f[i][j] == true)
//                continue;
//            f[i][j] = f[i - 1][j] || f[i - 1][abs(j - w[i])] || f[i - 1][j + w[i]];
//        }
//    }
//    int res = 0;
//    for (int i = 1; i <= sum; i++)
//    {
//        res += f[n][i];
//    }
//    cout << res << endl;
//
//    return 0;
//}

//typedef long long LL;
//LL k, res;
//
//LL Judge(LL x)
//{
//    LL count = 0;
//    while (x > 0)
//    {
//        count += x / 5;
//        x /= 5;
//    }
//    return count;
//}
//
//int main()
//{
//    // 请在此输入您的代码
//    scanf("%lld", &k);
//    LL right = 1e8 + 10;
//    LL left = 1;
//    while (left < right)
//    {
//        int mid = right - ((right - left) >> 1)-1;
//        LL jud = Judge(mid);
//        if (jud == k)
//        {
//            right = mid;
//        }
//        else if (jud > k)
//            right = mid - 1;
//        else if (jud < k)
//            left = mid + 1;
//    }
//    cout << Judge(left) << endl;
//    if (Judge(left) == k)
//        cout << left << endl;
//    else
//        cout << -1 << endl;
//
//    return 0;
//}

//const int N = 100010;
//int arr[N];//排序
//int index[N];//比对
//int resArr[N];//
//int n;
//
//int Work(int ind)
//{
//    int count = 0;
//    for (int i = ind; i <= n; i++)
//    {
//        if (arr[i] == arr[ind])
//            count++;
//        else
//            break;
//    }
//    return count;
//}
//
//int main()
//{
//    // 请在此输入您的代码
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> arr[i];
//        resArr[i] = arr[i];
//    }
//    sort(arr + 1, arr + 1 + n);
//    int mid = n / 2 + n % 2;
//
//    int mid_count = 0;//中间值总个数
//    int left_count = 0;//左值总个数
//    int right_count = 0;//右值总个数
//
//    for (int i = 1; i <= n; i++)//计算各值总个数
//    {
//        if (arr[i] == arr[mid])
//        {
//            mid_count = Work(i);//中间值总个数
//            left_count = i - 1;//左值总个数
//            right_count = n - left_count - mid_count;//右值总个数
//            break;
//        }
//    }
//
//    for (int i = 1; i <= n; i++)
//    {
//        if (arr[i] < arr[mid])
//        {
//            if (left_count < right_count)
//            {
//                if (left_count + mid_count == right_count)
//                {
//                    index[arr[i]] = arr[left_count + mid_count + 1] - arr[i];
//                }
//                if (left_count + mid_count > right_count)
//                {
//                    index[arr[i]] = arr[mid_count] - arr[i] + 1;
//                }
//            }
//            if (left_count == right_count)
//            {
//                index[arr[i]] = arr[mid] - arr[i] + 1;
//            }
//            if (left_count > right_count)
//            {
//                index[arr[i]] = arr[mid] - arr[i];
//            }
//        }
//        else if (arr[i] == arr[mid])
//        {
//            if (left_count < right_count)
//            {
//                if (mid_count > 1)
//                    index[arr[i]] = 1;
//                if (mid_count == 1)
//                    index[arr[i]] = arr[i + 1] - arr[i] + 1;
//            }
//            break;
//        }
//    }
//
//    for (int i = 1; i <= n; i++)//输出
//    {
//        cout << index[resArr[i]] << " ";
//    }
//    cout << endl;
//
//    return 0;
//}