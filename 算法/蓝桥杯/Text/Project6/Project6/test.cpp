#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include <Windows.h>
using namespace std;

//typedef long long LL;
//const int N = 100010;
//LL mod = 1000000007;
//int f[N], g[N];
//int n, ma, mb;
//LL res;
//
//int main()
//{
//    cin >> n;
//
//    cin >> ma;
//    for (int i = 0; i < ma; i++) cin >> f[i];
//    cin >> mb;
//    for (int i = 0; i < mb; i++) cin >> g[i];
//
//    reverse(f, f + ma);
//    reverse(g, g + mb);
//
//    int flag = max(max(f[0], g[0]) + 1, 2);
//    res = (res + f[0] - g[0]) % mod;
//    for (int i = 1; i < ma; i++)
//    {
//        int k = max(max(f[i], g[i]) + 1, 2);
//        res = (res + (f[i] - g[i]) * flag) % mod;
//        flag = (flag*k)%mod;
//    }
//    cout << res << endl;
//
//    return 0;
//}

//typedef long long LL;
//LL n, res;
//
//LL g[2][10000000];
//
//void Print(int index)
//{
//    for (int i = 1; g[index][i] != 0; i++)
//        printf("%lld ", g[index][i]);
//    Sleep(300);
//    printf("\n");
//}
//
//int main()
//{
//    // 请在此输入您的代码
//    cin >> n;
//    LL index = 1;
//    bool flag = false;
//    while (true)
//    {
//        if (index % 2 == 1)
//        {
//            for (LL i = 1; i <= index; i++)
//            {
//                if (i == 1 || i == index)
//                    g[0][i] = 1;
//                else
//                    g[0][i] = g[1][i] + g[1][i - 1];
//                res++;
//                if (g[0][i] == n)
//                {
//                    flag = true;
//                    break;
//                }
//            }
//        }
//        else
//        {
//            for (LL i = 1; i <= index; i++)
//            {
//                if (i == 1 || i == index)
//                    g[1][i] = 1;
//                else
//                    g[1][i] = g[0][i] + g[0][i - 1];
//                res++;
//                if (g[1][i] == n)
//                {
//                    flag = true;
//                    break;
//                }
//            }
//        }
//        //Print(index % 2);
//        index++;
//        if (flag)
//            break;
//    }
//    cout << res << endl;
//    return 0;
//}

//bool g[110][100000];
//int n;
//int w[110];
//int num;
//
//int main()
//{
//    // 请在此输入您的代码
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> w[i];
//        num += w[i];
//    }
//    
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= num; j++)
//        {
//            g[i][j] = g[i - 1][j] || g[i - 1][j + w[i]] || g[i - 1][abs(j - w[i])];
//        }
//        g[i][w[i]] = true;
//    }
//    int res = 0;
//    for (int i = 1; i <= num; i++)
//    {
//        if (g[n][i])
//            res++;
//    }
//    cout << res << endl;
//
//    return 0;
//}

//long long n;
//
//int main()
//{
//	cin >> n;
//	int d = 24 * 60 * 60 * 1000;
//	d = n % d;
//
//	int h = d / (60 * 60 * 1000);
//	int m = d % (60 * 60 * 1000) / (60 * 1000);
//	int s = d % (60 * 60 * 1000) % (60 * 1000) / 1000;
//
//	printf("%-02d:%-02d:%-02d\n", h, m, s);
//
//	return 0;
//}

//typedef long long LL;
//const long long N = 2021041820210418;
//
//int main()
//{
//	int res = 0;
//	for (LL i = 1; i <= N; i++)
//	{
//		if (i > N / 2 && i != N)
//			i = N;
//		for (LL j = 1; j <= N; j++)
//		{
//			if (j * i > N)
//				break;
//			if (j > N / 2 && j != N)
//				j = N;
//			for (LL k = 1; k <= N; k++)
//			{
//				if (i * k * j == N)
//					res++;
//				if (i * j * k > N)
//					break;
//			}
//		}
//	}
//	cout << res << endl;
//	return 0;
//}


//const int N = 110;
//int n, m;
//int arr[N][N];
//int dis[N][N];
//int Res[N][N];
//int res, flag;
//
//int dx[4] = { -1,1,0,0 };
//int dy[4] = { 0,0,-1,1 };
//
//void DFS(int x, int y, int count)
//{
//    flag++;
//    if (res < count)
//        res = count;
//    dis[x][y] = 1;
//    for (int i = 0; i < 4; i++)
//    {
//        int tx = x + dx[i];
//        int ty = y + dy[i];
//        if (flag == 1 && Res[tx][ty] != 0)
//        {
//            if (res < count + Res[tx][ty])
//            {
//                res = count + Res[tx][ty];
//            }
//            continue;
//        }
//        if (tx > 0 && tx <= n && ty > 0 && ty <= m && dis[tx][ty] == 0 && arr[x][y] > arr[tx][ty])
//        {
//            dis[tx][ty] = 1;
//            DFS(tx, ty, count + 1);
//            Res[tx][ty] = max(flag - count, Res[tx][ty]);
//            dis[tx][ty] = 0;
//        }
//    }
//    dis[x][y] = 0;
//}
//
//int main()
//{
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            cin >> arr[i][j];
//        }
//    }
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            if (Res[i][j] == 0)
//            {
//                flag = 0;
//                DFS(i, j, 1);
//            }
//        }
//    }
//    cout << res << endl;
//    return 0;
//}

//const int N = 105;
//int g[N][N][N];
//int n, m;
//
//int main()
//{
//    cin >> n >> m;
//
//    g[0][0][2] = 1;
//
//    for (int i = 0; i <= n; i++)//店
//    {
//        for (int j = 0; j <= m; j++)//花
//        {
//            for (int k = 0; k <= m; k++)//酒
//            {
//                if (i > 0 && k % 2 == 0)
//                {
//                    g[i][j][k] += g[i - 1][j][k / 2];
//                }
//                if (j > 0 && k >= 0)
//                {
//                    g[i][j][k] += g[i][j - 1][k + 1];
//                }
//                //if(g[i][j][k] != 0)
//                //    printf("g[%d][%d][%d]:%d\n", i, j, k,g[i][j][k]);
//            }
//        }
//    }
//
//    cout << g[n][m-1][1] << endl;
//
//    return 0;
//}

//const int N = 100010;
//int n;
//int arr[N];
//
//int Work1()
//{
//    int res = 0;
//    int left = arr[0];
//    int right = arr[n - 1];
//    int mid = left + ((right - left) >> 1);
//    for (int i = 0; i < n; i++)
//    {
//        res += abs(arr[i] - mid);
//    }
//    return res;
//}
//
//int Work2()
//{
//    int mid = 0;
//    int res = 0;
//    int left = 0;
//    int right = 0;
//    if (n % 2 == 0)
//    {
//        left = (n - 1) / 2;
//        right = (n - 1) / 2 + 1;
//        mid = left + ((right - left) >> 1);
//    }
//    else
//        mid = n / 2;
//    for (int i = 0; i < n; i++)
//    {
//        res += abs(arr[i] - arr[mid]);
//    }
//    return res;
//}
//
//int main()
//{
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> arr[i];
//    }
//    sort(arr, arr + n);
//    int left = arr[0];
//    int right = arr[n - 1];
//    int mid = left + ((right - left) >> 1);
//    int res = 0;
//    for (int i = 0; i < n; i++)
//    {
//        res += abs(arr[i] - mid);
//    }
//    cout << res << endl;
//
//    res = min(Work1(), Work2());
//
//    return 0;
//}

//const int N = 100010;
//int n;
//int arr[N];
//
//int Work1()
//{
//    int res = 0;
//    int left = arr[0];
//    int right = arr[n - 1];
//    int mid = left + ((right - left) >> 1);
//    for (int i = 0; i < n; i++)
//    {
//        res += abs(arr[i] - mid);
//    }
//    return res;
//}
//
//int Work2()
//{
//    int mid = 0;
//    int res = 0;
//    int left = 0;
//    int right = 0;
//    if (n % 2 == 0)
//    {
//        left = (n - 1) / 2;
//        right = (n - 1) / 2 + 1;
//        mid = left + ((right - left) >> 1);
//    }
//    else
//        mid = n / 2;
//    for (int i = 0; i < n; i++)
//    {
//        res += abs(arr[i] - arr[mid]);
//    }
//    return res;
//}
//
//int main()
//{
//    cin >> n;
//    for(int i=0;i<n;i++)
//    {
//        cin >> arr[i];
//    }
//    sort(arr,arr+n);
//    int res = 0;
//    res = min(Work1(), Work2());
//    cout << res << endl;
//    return 0;
//}

//const int N = 1e5 + 10;
//int g[N][3];
//int n;
//int w[N];
//
//int main()
//{
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> w[i];
//    }
//    g[1][0] = 0;
//    g[1][1] = -1 * w[1];
//
//    for (int i = 2; i <= n; i++)
//    {
//        g[i][1] = max(g[i - 1][0] - w[i], g[i - 1][1]);
//        g[i][0] = max(g[i - 1][0], g[i - 1][1] + w[i]);
//    }
//    cout << max(g[n][1], g[n][0]) << endl;
//
//    return 0;
//}