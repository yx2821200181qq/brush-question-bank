#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

int main()
{
    // 请在此输入您的代码
    int n = 0, a1 = 0;
    cin >> n;
    int count = 0;
    for (int i = 0; i < n; i++)
    {
        if (i == 0)
        {
            cin >> a1;
            continue;
        }
        int temp = 0;
        cin >> temp;
        if (a1 > 0)
        {
            if (temp<0 && temp * (-1) > a1)
                count++;
            if (temp > 0 && temp < a1)
                count++;
        }
        else
        {
            if (temp > 0 && temp < a1 * (-1))
                count++;
            if (temp<0 && a1>temp)
                count++;
        }
    }
    cout << count + 1;
    return 0;
}