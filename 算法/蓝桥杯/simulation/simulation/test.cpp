#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<string>
#include<set>
using namespace std;

//const int N = 1e5 + 10;
//int n, m, t;
//int last[N];//上一次出现的时间
//bool range[N];//是否到达优先级
//int Num[N];//现在的等级
//
//typedef struct date {
//    int time;
//    int num;
//};
//date arr[N];
//
//int compare(const void* a1, const void* a2)
//{
//    return (*(date*)a1).time > (*(date*)a2).time;
//}
//
//int main()
//{
//    cin >> n >> m >> t;
//    for (int i = 1; i <= m; i++)
//    {
//        scanf("%d %d", &(arr[i].time), &(arr[i].num));
//    }
//    qsort(arr + 1, m, sizeof(arr[0]), compare);
//
//    return 0;
//}

const int N = 1e5 + 10;
int n, m, t;
int last[N];//上一次出现的时间
bool range[N];//是否到达优先级
int Num[N];//现在的等级

typedef struct date {
    int time;
    int num;
};
date arr[N];

int compare(date a1, date a2)
{
    return a1.time < a2.time;
}

int main()
{
    cin >> n >> m >> t;
    for (int i = 1; i <= m; i++)
    {
        scanf("%d %d", &(arr[i].time), &(arr[i].num));
    }
    sort(arr + 1, arr+m+1, compare);
    for (int i = 1; i <= m; i++)
    {
        Num[arr[i].num] += 2;
        if (last[arr[i].num] != 0)
        {
            Num[arr[i].num] -= arr[i].time - last[arr[i].num] - 1;
            if (Num[arr[i].num] < 0)
                Num[arr[i].num] = 0;
        }
        last[arr[i].num] = arr[i].time;
        if (Num[arr[i].num] > 5)
            range[arr[i].num] = true;
        else if (Num[arr[i].num] <= 3)
            range[arr[i].num] = false;
    }
    for (int i = 1; i <= n; i++)
    {
        if (last[i] != 0)
            Num[i] -= t - last[i];
        if (Num[i] < 0)
            Num[i] = 0;
        if (Num[i] > 5)
            range[i] = true;
        else if (Num[i] <= 3)
            range[i] = false;
    }
    int res = 0;
    for (int i = 1; i <= n; i++)
    {
        if (range[i] == true)
            res++;
    }
    cout << res << endl;

    return 0;
}

//const int N = 1e4;
//int n, m, t;
//int arr[N][N];
//int resArr[N];
//int res[N];
//
//int main()
//{
//    cin >> n >> m >> t;
//    //某一时刻内有订单的店铺的序号
//    //某一时刻一个店铺订单的数量
//    //n：店数
//    //m：组数
//    //t：时刻数
//    for (int i = 1; i <= m; i++)
//    {
//        int a, b;
//        cin >> a >> b;
//        arr[a][b]++;
//    }
//    for (int i = 1; i <= t; i++)
//    {
//        for (int j = 1; j <= n; j++)
//        {
//            if (arr[i][j] > 0)
//            {
//                resArr[j] += arr[i][j] * 2;
//
//            }
//            else if (resArr[j] != 0)
//            {
//                resArr[j]--;
//            }
//            if (resArr[j] > 5)
//                res[j] = 1;
//            if (resArr[j] <= 3)
//                res[j] = 0;
//        }
//    }
//    int Res = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        cout << res[i] << " ";
//        if (i % 50 == 0)
//            cout << endl;
//        Res += res[i];
//    }
//
//    cout << Res << endl;
//
//    return 0;
//}
const int N = 1e5 + 7;
int Num[N];
int n, m, t;

//n：几家店
//m：订单数
//t：时刻

struct Switch {
    int time;//时刻
    int num;//店序
}arr[N];

//int compare(const void* a1, const void* a2)
//{
//    return (((Switch*)a1)->time) > (((Switch*)a2)->time);
//}

int compare(const void* a1, const void* a2)
{
    Switch* s1 = (Switch*)a1;
    Switch* s2 = (Switch*)a2;
//    cout << "Comparing " << s1->time << " and " << s2->time << endl;
    return s1->time > s2->time;
}

//void swap(void* p1, void* p2, int size)
//{
//    for (int i = 0; i < size; i++)
//    {
//        char temp = *((char*)p1 + i);
//        *((char*)p1 + i) = *((char*)p2 + i);
//        *((char*)p2 + i) = temp;
//    }
//}
//
//void my_psort(void* arr4, int num, int size, int (*cmp)(const void*,const void*))
//{
//    for (int i = 0; i < num; i++)
//    {
//        for (int j = 0; j < num - 1 - i; j++)
//        {
//            if (cmp((char*)arr4 + j * size, (char*)arr4 + (j + 1) * size) > 0)
//            {
//                swap((char*)arr4 + j * size, (char*)arr4 + (j + 1) * size, size);
//            }
//        }
//    }
//}

int main()
{
    cin >> n >> m >> t;
    for (int i = 0; i < m; i++)
    {
        int a, b;
        cin >> arr[i].time >> arr[i].num;
    }
    qsort(arr, m, sizeof(arr[0]), compare);

    //int flag = 0;
    //for (int i = 0; i < m; i++)
    //{
    //    Num[arr[i].num] += 2;
    //    if (i == 0) flag = arr[i].time;
    //    if (i != 0 && flag == arr[i].time)
    //        Num[arr[i].num]++;
    //    if (flag != arr[i].time)
    //    {
    //        for (int j = 1; j <= n; j++)
    //        {
    //            if (Num[j] > 0 && j != arr[i].num)
    //            {
    //                Num[j]--;
    //            }
    //        }
    //        flag = arr[i].time;
    //    }
    //}
    //int res = 0;
    //for (int i = 0; i < n+1; i++)
    //{
    //    if (Num[i] > 5)
    //        res++;
    //}
    //cout << res << endl;
    //return 0;
}

//const int N = 1e5 + 7;
//int arr[N];
//int n, m, t;
//
//int main()
//{
//    cin >> n >> m >> t;
//    set<int, int> s;
//    s.insert(1, 2);
//    
//
//    return 0;
//}

//int main()
//{
//	int a = 1e5;
//	cout << a << endl;
//
//	return 0;
//}

//int T;
//int h1, h2, m1, m2, s1, s2, r1;
//int h3, h4, m3, m4, s3, s4, r2;
//int res1, res2, res3;
//
//void Work()
//{
//    int temp;
//    h2 = h2 + 24 * r1;
//    h4 = h4 + 24 * r2;
//    res1 = ((h2 + h4) - (h1 + h3)) / 2;
//    temp = ((h2 + h4) - (h1 + h3)) % 2;//时-取余
//    res2 = ((m2 + m4) - (m1 + m3) + temp * 60) / 2;
//    temp = ((m2 + m4) - (m1 + m3) + temp * 60) % 2;//分-取余
//    res3 = ((s2 + s4) - (s1 + s3) + temp * 60) / 2;
//
//    res2 += res3 / 60;
//    res3 %= 60;
//    res1 += res2 / 60;
//    res2 %= 60;
//
//    while (res3 < 0)
//    {
//        res3 = res3 + 60;
//        res2--;
//    }
//    while (res2 < 0)
//    {
//        res2 = res2 + 60;
//        res1--;
//    }
//}
//
//int main()
//{
//    cin >> T;
//    for (int i = 0; i < T; i++)
//    {
//        scanf("%d:%d:%d %d:%d:%d (+%d)", &h1, &m1, &s1, &h2, &m2, &s2, &r1);
//        scanf("%d:%d:%d %d:%d:%d (+%d)", &h3, &m3, &s3, &h4, &m4, &s4, &r2);
//        Work();
//        printf("%02d:%02d:%02d\n", res1, res2, res3);
//        r1 = r2 = 0;
//    }
//
//    return 0;
//}

//int month[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//int arr[3];
//int arr1[3][3];
//int flag;
//
////AA/BB/CC
//
//int compare(void* a1, void* a2)
//{
//    return
//}
//
//bool Judge(int i, int j, int k)
//{
//    if (arr[i] >= 60)
//        flag = 19 * 100 + arr[i];
//    else if (arr[i] >= 10 && arr[i] < 60)
//        flag = 20 * 100 + arr[i];
//    else
//        flag = 20 * 100 + arr[i];
//
//    if ((flag % 4 == 0 && flag % 100 != 0) || flag % 100 == 0) month[2] = 29;
//    else month[2] = 28;
//
//    if (arr[j] > 12 || arr[j] <= 0)
//        return false;
//    if (arr[k] > month[arr[j]] || arr[k] <= 0)
//        return false;
//    return true;
//}
//
//int main()
//{
//    string s;
//    cin >> s;
//    arr[0] = (s[0] - '0') * 10 + s[1] - '0';
//    arr[1] = (s[3] - '0') * 10 + s[4] - '0';
//    arr[2] = (s[6] - '0') * 10 + s[7] - '0';
//    int temp = 0;
//    if (Judge(0, 1, 2))//年-月-日
//    {
//        arr1[0][0] = flag;
//        arr1[0][1] = arr[1];
//        arr1[0][2] = arr[2];
//    }
//
//    if (Judge(2, 0, 1))//月-日-年
//    {
//        arr1[1][0] = flag;
//        arr1[1][1] = arr[0];
//        arr1[1][2] = arr[1];
//    }
//
//    if (Judge(2, 1, 0))//日-月-年
//    {
//        arr1[2][0] = flag;
//        arr[2][1] = arr[1];
//        arr[2][2] = arr[0];
//    }
//    qsort(arr1, 3, 4, compare);
//
//    return 0;
//}

//int month[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//int arr[3];
//int flag;
//
////AA/BB/CC
//
//bool Judge(int i, int j, int k)
//{
//    if (arr[i] >= 60)
//        flag = 19 * 100 + arr[i];
//    else if (arr[i] >= 10 && arr[i] < 60)
//        flag = 20 * 100 + arr[i];
//    else
//        flag = 20 * 100 + arr[i];
//
//    if ((flag % 4 == 0 && flag % 100 != 0) || flag % 100 == 0) month[2] = 29;
//    else month[2] = 28;
//
//    if (arr[j] > 12)
//        return false;
//    if (arr[k] > month[arr[j]])
//        return false;
//    return true;
//}
//
//int main()
//{
//    string s;
//    cin >> s;
//    arr[0] = (s[0] - '0') * 10 + s[1] - '0';
//    arr[1] = (s[3] - '0') * 10 + s[4] - '0';
//    arr[2] = (s[6] - '0') * 10 + s[7] - '0';
//    int temp = 0;
//    if (Judge(0, 1, 2))//年-月-日
//    {
//        cout << flag << "-";
//        if (arr[1] >= 10) cout << arr[1] << "-";
//        else cout << temp << arr[1] << "-";
//
//        if (arr[2] >= 10) cout << arr[2] << endl;
//        else cout << temp << arr[2] << endl;
//    }
//
//    if (Judge(2, 0, 1))//月-日-年
//    {
//        cout << flag << "-";
//        if (arr[0] >= 10) cout << arr[0] << "-";
//        else cout << temp << arr[0] << "-";
//
//        if (arr[1] >= 10) cout << arr[1] << endl;
//        else cout << temp << arr[1] << endl;
//    }
//
//    if (Judge(2, 1, 0))//日-月-年
//    {
//        cout << flag << "-";
//        if (arr[1] >= 10) cout << arr[1] << "-";
//        else cout << temp << arr[1] << "-";
//
//        if (arr[0] >= 10) cout << arr[0] << endl;
//        else cout << temp << arr[0] << endl;
//    }
//
//    return 0;
//}

//int w, m, n;
//int x1, x2;
//
//int main()
//{
//    cin >> w >> m >> n;
//
//    for (int i = 1; i * w < 10000; i++)
//    {
//        if (i * w >= m && x1 == 0)
//            x1 = i;
//        if (i * w >= n && x2 == 0)
//            x2 = i;
//        if (x1 != 0 && x2 != 0)
//            break;
//    }
//    int a = max(x1, x2) - min(x1, x2);
//    int b, b1, b2;
//    if (x1 % 2 == 0)//右大
//    {
//        b1 = m - (x1 - 1) * w - 1;
//    }
//    else//左大
//    {
//        b1 = x1 * w - m;
//    }
//
//    if (x2 % 2 == 0)//右大
//    {
//        b2 = n - (x2 - 1) * w - 1;
//    }
//    else//左大
//    {
//        b2 = x2 * w - n;
//    }
//    b = max(b1, b2) - min(b1, b2);
//    cout << a + b << endl;
//    return 0;
//}

//string date1, date2;
//int res;
//
//int month[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//
////0000 00 00
//
//void Swap(string& date)
//{
//    int y = (date[0] - '0') * 1000 + (date[1] - '0') * 100 + (date[2] - '0') * 10 + date[3] - '0';
//    int m = (date[4] - '0') * 10 + date[5] - '0';
//    int d = (date[6] - '0') * 10 + date[7] - '0';
//
//    if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0)
//        month[2] = 29;
//    else
//        month[2] = 28;
//
//    if (month[m] > d)//日
//    {
//        d = d + 1;
//        date[7] = d % 10 + '0';
//        date[6] = d / 10 + '0';
//    }
//    else if (month[m] == d)//月
//    {
//        if (m == 12)//年
//        {
//            d = 1;
//            m = 1;
//            y++;
//            date[0] = y / 1000 + '0';
//            date[1] = (y / 100) % 10 + '0';
//            date[2] = (y / 10) % 10 + '0';
//            date[3] = y % 10 + '0';
//            date[5] = m % 10 + '0';
//            date[4] = m / 10 + '0';
//            date[7] = d % 10 + '0';
//            date[6] = d / 10 + '0';
//        }
//        else
//        {
//            d = 1;
//            date[7] = d % 10 + '0';
//            date[6] = d / 10 + '0';
//            m++;
//            date[5] = m % 10 + '0';
//            date[4] = m / 10 + '0';
//        }
//    }
//}
//
//bool Judge(string& date)
//{
//    string temp = date;
//    reverse(temp.begin(), temp.end());
//    if (temp == date)
//        return true;
//    else
//        return false;
//}
//
//int main()
//{
//    getline(cin, date1);
//    getline(cin, date2);
//
//    while (date1 != date2)
//    {
//        if (Judge(date1))
//            res++;
//        Swap(date1);
//    }
//    if (Judge(date2))
//        res++;
//    cout << res << endl;
//
//    return 0;
//}

//int month[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//
//bool Judge(int date)
//{
//    int y = date / 10000;
//    int m = date % 10000 / 100;
//    int d = date % 10000 % 100;
//    if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0)
//        month[2] = 29;
//    else
//        month[2] = 28;
//
//    if (m > 12)
//        return false;
//    if (d > month[m])
//        return false;
//    return true;
//}
//bool ABJudge(int date)
//{
//    int a = date / 10000;
//    int a1 = a / 100;
//    int a2 = a % 100;
//    if (a1 == a2)
//        return true;
//}
//
//int main()
//{
//    // 请在此输入您的代码
//    int date1;
//    cin >> date1;
//    int a = 0, b = 0;
//    for (int i = 1000; i < 10000 && (a == 0 || b == 0); i++)
//    {
//        int date = i, x = i;
//        for (int i = 0; i < 4; i++)
//        {
//            date = x % 10 + date * 10;
//            x = x / 10;
//        }
//        if (date > date1 && Judge(date) && a == 0)
//        {
//            a = date;
//        }
//        if (date > date1 && Judge(date) && ABJudge(date) && b == 0)
//        {
//            b = date;
//        }
//    }
//    cout << a << endl;
//    cout << b << endl;
//    return 0;
//}