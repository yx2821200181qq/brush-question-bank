#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<cstdlib>
using namespace std;

const int N = 1010, MOD = 1e8 + 7;
int n, s, a, b;
int f[N][N];

int main() {
    cin >> n >> s >> a >> b;

    f[0][0] = 1;
    for (int i = 1; i < n; i++)
        for (int j = 0; j < n; j++) {
            f[i][j] = (f[i - 1][((j - i * a) % n + n) % n] + f[i - 1][((j + i * b) % n + n) % n]) % MOD;
        }

    cout << f[n - 1][(s % n + n) % n] % MOD << endl;

    return 0;
}

//#include <iostream>
//using namespace std;
//
//const int N = 55;
//int f[N][N][15][15];
//int n, m, k;
//int w[N][N];
//const int mod = 1000000007;
//int count;
//
//int main()
//{
//    // 请在此输入您的代码
//    cin >> n >> m >> k;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            cin >> w[i][j];
//            w[i][j]++;
//        }
//    }
//
//    f[1][1][1][w[1][1]] = 1;
//    f[1][1][0][0] = 1;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            for (int b = 0; b <= k; b++)//宝贝数
//            {
//                for (int c = 0; c <= 13; c++)//最大价值
//                {
//                    int& val = f[i][j][b][c];
//                    val = (val + f[i - 1][j][b][c]) % mod;
//                    val = (val + f[i][j - 1][b][c]) % mod;
//                    if (b > 0 && w[i][j] == c)
//                    {
//                        for (int d = 0; d < c; d++)
//                        {
//                            val = (val + f[i - 1][j][b - 1][d]) % mod;
//                            val = (val + f[i][j - 1][b - 1][d]) % mod;
//                        }
//                    }
//                }
//            }
//        }
//    }
//    for (int i = 0; i <= 13; i++)
//    {
//        count += f[n][m][k][i];
//    }
//    cout << count % mod << endl;
//    return 0;
//}

//const int N = 53;
//int f[N][N][15][15];
//int n, m, k;
//int w[N][N];
//const int mod = 1000000007;
//int _count;
//
//int main()
//{
//    // 请在此输入您的代码
//    cin >> n >> m >> k;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            cin >> w[i][j];
//        }
//    }
//
//    f[1][1][1][w[1][1]] = 1;
//    f[1][1][0][0] = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            for (int b = 0; b <= k; b++)//宝贝数
//            {
//                for (int c = 0; c <= 12; c++)//最大价值
//                {
//                    f[i][j][b][c] = (f[i - 1][j][b][c] + f[i][j - 1][b][c]) % mod;
//                    if (b > 0 && w[i][j] == c)
//                    {
//                        for (int d = 0; d < c; d++)
//                        {
//                            f[i][j][b][c] += (f[i - 1][j][b - 1][d] + f[i][j - 1][b = 1][d]) % mod;
//                        }
//                    }
//                }
//            }
//        }
//    }
//    for (int i = 1; i <= 12; i++)
//    {
//        _count += f[n][m][k][i];
//    }
//    cout << _count << endl;
//    return 0;
//}

//int n = 0;
//const int N = 1010;
//int W[N],V[N];
//
//int main()
//{
//	int Max = -1;
//	cin >> n;
//	for (int i = 1; i <= n; i++)
//	{
//		cin >> W[i];
//		V[i] = 1;
//	}
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 1; j < i; j++)
//		{
//			if (W[i] > W[j])
//			{
//				V[i] = max(V[j] + 1, V[i]);
//				Max = (Max > V[i]) ? Max : V[i];
//			}
//		}
//	}
//	cout << Max;
//
//	return 0;
//}

//int T, R, C;
//int Max;
//const int N = 110;
//int arr[N][N];
//int f[N][N];
//
//int main()
//{
//    cin >> T;
//    while (T--)
//    {
//        cin >> R >> C;
//        for (int i = 1; i <= R; i++)
//        {
//            for (int j = 1; j <= C; j++)
//            {
//                cin >> arr[i][j];
//            }
//        }
//
//        for (int i = 1; i <= R; i++)
//        {
//            for (int j = 1; j <= C; j++)
//            {
//                f[i][j] = max(f[i - 1][j], f[i][j - 1]) + arr[i][j];
//            }
//        }
//        cout << f[R][C] << endl;
//    }
//
//    return 0;
//}

//int T, R, C;
//int Max;
//const int N = 110;
//int arr[N][N];
//int f[N];
//
//int main()
//{
//    cin >> T;
//    while (T--)
//    {
//        memset(f, 0, sizeof(f));
//        cin >> R >> C;
//        for (int i = 1; i <= R; i++)
//        {
//            for (int j = 1; j <= C; j++)
//            {
//                cin >> arr[i][j];
//            }
//        }
//
//        for (int i = 1; i <= R; i++)
//        {
//            for (int j = 1; j <= C; j++)
//            {
//                f[j] = max(f[j], f[j - 1]) + arr[i][j];
//            }
//        }
//        cout << f[C] << endl;
//    }
//
//    return 0;
//}

//int T, R, C;
//int M, Max, JudgeMax;
//const int N = 110;
//int arr[N][N];
//
//int l[2] = { 0,1 };
//int r[2] = { 1,0 };
//
//void DFS(int x, int y)
//{
//    if (x == R && y == C && Max < JudgeMax)
//    {
//        Max = JudgeMax;
//        return;
//    }
//    for (int i = 0; i < 2; i++)
//    {
//        if (x + l[i] <= R && y + r[i] <= C)
//        {
//            JudgeMax += arr[x + l[i]][y + r[i]];
//            DFS(x + l[i], y + r[i]);
//            JudgeMax -= arr[x + l[i]][y + r[i]];
//        }
//    }
//}
//
//int main()
//{
//    cin >> T;
//    while (T--)
//    {
//        M = 0;
//        Max = 0;
//        cin >> R >> C;
//        for (int i = 1; i <= R; i++)
//        {
//            for (int j = 1; j <= C; j++)
//            {
//                cin >> arr[i][j];
//            }
//        }
//        JudgeMax += arr[1][1];
//        DFS(1, 1);
//        cout << Max << endl;
//    }
//
//
//    return 0;
//}

//#include<iostream>
//#include<cstring>
//#include<cstdlib>
//#include<cmath>
//#include<algorithm>
//using namespace std;
//
//int N, V;
//int w[1010], v[1010];
//int f[1010];
//
//int main()
//{
//    cin >> N >> V;
//    for (int i = 1; i <= N; i++)
//        cin >> w[i] >> v[i];
//    for (int i = 1; i <= N; i++)
//    {
//        for (int j = V; j >= w[i]; j--)
//        {
//            f[j] = max(f[j], f[j - w[i]] + v[i]);
//        }
//    }
//
//    cout << f[V] << endl;
//
//    return 0;
//}