#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
	int n, m;
	scanf("%d %d", &n, &m);

	int arr[5][5];

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			scanf("%d", &arr[i][j]);
		}
	}

	int x, y;
	scanf("%d %d", &x, &y);

	printf("%d", arr[x-1][y-1]);

	return 0;
}