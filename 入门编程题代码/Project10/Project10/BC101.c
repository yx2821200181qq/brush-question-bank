#define _CRT_SECURE_NO_WARNINGS 1

int main()
{
	float score[5][5];

	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			scanf("%f", &score[i][j]);
		}
	}

	int sum = 0;
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			printf("%.1f ", score[i][j]);
			sum += score[i][j];
		}
		printf("%.1f ", sum);
		sum = 0;
	}

	return 0;
}