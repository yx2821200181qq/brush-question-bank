#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
	int n = 0;
	int arr[5000] = { 0 };
	int brr[5000] = { 0 };

	scanf("%d", &n);

	for (int i = 0; i < n; i++)
	{
		scanf("%d", &arr[i]);
		brr[i] = arr[i];
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < i; j++)
		{
			if (arr[i] == brr[j])
			{
				arr[i] = 0;
			}
		}
	}

	for (int i = 0; i < n; i++)
	{
		if (arr[i] == 0)
		{
			continue;
		}
		printf("%d ", arr[i]);
	}

	return 0;
}