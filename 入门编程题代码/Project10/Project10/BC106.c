#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
	int n = 0;

	scanf("%d", &n);

	int a = 0;
	int count = 0;

	for (int i = 0; i < n ; i++)
	{
		for (int j = 0; j < n; j++)
		{
			scanf("%d", &a);
			if (i > j && a==0)
			{
				count++;
			}
		}
	}

	int sum = 0;
	for (int i = 0; i < n; i++)
	{
		sum += i;
	}

	if (sum == count)
	{
		printf("YES");
	}
	else
	{
		printf("NO");
	}

	return 0;
}