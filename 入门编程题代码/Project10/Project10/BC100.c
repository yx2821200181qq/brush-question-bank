#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
	int n, m;

	//输入n，m
	scanf("%d %d", &n, &m);

	int arr[1000] = { 0 };
	int brr[1000] = { 0 };
	
	//输入两个数组的内容
	for (int i = 0; i < n; i++)
	{
		scanf("%d", &arr[i]);
	}

	for (int j = 0; j < m; j++)
	{
		scanf("%d", &brr[j]);
	}

	int i = 0;
	int j = 0;
	while (i < n && j < m)
	{
		if (arr[i] > brr[j])
		{
			printf("%d ", brr[j]);
			j++;
		}
		else
		{
			printf("%d ", arr[i]);
			i++;
		}
	}

	if (i == n)
	{
		for (int l = j; l < m; l++)
		{
			printf("%d ", brr[l]);
		}
	}
	else
	{
		for (int l = i; l < n; l++)
		{
			printf("%d ", arr[l]);
		}
	}

	return 0;
}