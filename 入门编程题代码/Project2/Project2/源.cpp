//#include<stdio.h>
//
//int main()
//{
//	printf("%d\n", 15);
//	printf("%1d\n", 15);
//	printf("%4d\n", 15);
//
//	return 0;
//}

//#include<stdio.h>
//
//int main()
//{
//	int ret = printf("Hello World!");
//	printf("\n");
//	printf("%d\n", ret);
//
//	return 0;
//}

//#include<stdio.h>
//
//int main()
//{
//    int num = 0;
//    float c_score = 0.0;
//    float math_score = 0.0;
//    float english_score = 0.0;
//    scanf_s("%d;%f,%f,%f", &num, &c_score, &math_score, &english_score);
//    printf("The each subject score of No. %d is %.2f, %.2f, %.2f.\n"
//        , num, c_score, math_score, english_score);
//
//    return 0;
//}

//#include<stdio.h>
//
//int main()
//{
//	int ch = 0;
//	while ((ch = getchar()) != EOF)
//	{
//		//判断字母
//		if ((ch>='A' && ch<='Z') || (ch>='a' && ch<='z'))
//		{
//			printf("YES\n");
//		}
//		else
//		{
//			printf("NO\n");
//		}
//		//清理掉\n
//		getchar();
//	}
//
//	return 0;
//}

//#include<stdio.h>
//
//int main()
//{
//	int n = 0;
//	scanf_s("%d", &n);
//
//	//printf("    %d    \n", n);
//	//printf("   %d %d   \n", n,n);
//	//printf("  %d %d %d  \n", n,n,n);
//	//printf(" %d %d %d %d \n", n,n,n,n);
//	//printf("%d %d %d %d %d\n", n,n,n,n,n);
//
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		//打印一行
//		//些打印空格
//		for (int j = 0; j < 4-i; j++)
//		{
//			printf(" ");
//		}
//		//在打印1
//		for (int j = 0; j <= i; j++)
//		{
//			printf("%d", n);
//			printf(" ");
//		}
//		printf("\n");
//	}
//
//	return 0;
//}

//#include<stdio.h>
//
//int main()
//{
//	int arr[] = { 73,32,99,97,110,32,100,111,32,105,116,33 };
//
//	int sz = sizeof(arr) / sizeof(0);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%c", arr[i]);
//	}
//
//	return 0;
//}

//#include<stdio.h>
//
//int main()
//{
//    int year = 0, month = 0, date = 0;
//    scanf_s("%4d%2d%2d", &year, &month, &date);
//
//    printf("year=%d\n", year);
//    printf("month=%02d\n", month);
//    printf("date=%d\n", date);
//
//    return 0;
//}

#include<iostream>
using namespace std;

template<class T1,class T2>
class Person;

template<class T1, class T2>
void printPerson2(Person<T1, T2> p)
{
	cout << "类外-姓名：" << p.age << endl;
	cout << "类外-年龄：" << p.name << endl;
}

template<class T1,class T2>
class Person
{
	//全局函数，打印Person的信息,类内实现
	friend void printfPerson(Person<T1,T2> p)
	{
		cout << "姓名：" << p.age << endl;
		cout << "年龄：" << p.name << endl;
	}

	//全局函数，类外实现
	//加空模板参数列表
	//如果全局函数是类外实现，需要让编译器提前知道这个函数的存在
	friend void printPerson2<>(Person<T1, T2> p);

public:
	Person(T1 name, T2 age)
	{
		this->name = name;
		this->age = age;
	}

private:
	T1 name;
	T2 age;
};


void text01()
{
	Person<string, int>p("张三", 20);
	printfPerson(p);
	printPerson2(p);
}

int main()
{
	text01();

	return 0;
}