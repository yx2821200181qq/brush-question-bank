#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stddef.h>
//typedef struct {
//    int a;
//    char b;
//    short c;
//    short d;
//}AA_t;
//
//int main()
//{
//    AA_t a;
//    printf("%d", sizeof(a));
//    /*int num = 0;
//    while (scanf("%d", num) != EOF)
//    {
//        int a = 0;
//        while (a < num)
//        {
//            for (int i = 0; i < num - a - 1; i++)
//                printf("  ");
//            for (int i = 0; i < a + 1; i++)
//                printf("* ");
//            printf("\n");
//            a++;
//        }
//    }*/
//    return 0;
//}

#pragma pack(4)/*编译选项，表示4字节对齐 平台：VS2013。语言：C语言*/
int main()
{
    struct A
    {
        int a;
        short b;
        int c;
        char d;
    };
    struct B
    {
        int a;
        short b;
        char c;
        int d;
    };
    struct A stT1 = { 1,2,4,'1' };
    struct B stT2;

    printf("%d %d", sizeof(stT1), sizeof(stT2));
    //printf("%d", sizeof(short));
    return 0;
}

//#include <stdio.h>
//int compare(const void* a1, const void* a2)
//{
//    return *(int*)a1 - *(int*)a2;
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int* arr = (int*)malloc(sizeof(int) * n);
//    for (int i = 0; i < n; i++)
//    {
//        int a = 0;
//        scanf("%d", &a);
//        arr[i] = a;
//    }
//    qsort(arr, n, sizeof(arr[0]), compare);
//    int temp = 0;
//    for (int i = 0; i < n; i++)
//    {
//        if (temp == arr[i])
//            continue;
//        printf("%d ", arr[i]);
//        temp = arr[i];
//    }
//    return 0;
//}