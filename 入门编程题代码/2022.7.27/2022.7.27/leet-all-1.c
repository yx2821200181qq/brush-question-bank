#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int* twoSum(int* nums, int numsSize, int target, int* returnSize) 
{
	for (int i = 0; i < numsSize-1; i++)
	{
		for (int j = i + 1; j < numsSize; j++)
		{
			if (nums[i] + nums[j] == target)
			{
				returnSize[0] = i;
				returnSize[1] = j;
				//return returnSize;
			}
		}
	}
}

int main()
{
	int nums[] = { 2, 11, 7, 15 };
	int target = 9;
	int returnSize[2] = { 0 };
	int numsSize = sizeof(nums) / sizeof(nums[0]);

	twoSum(nums,numsSize , target, returnSize);

	printf("%d ", returnSize[0]);
	printf("%d ", returnSize[1]);

	return 0;
}

//int main()
//{
//	printf("%d\n", sizeof(char*));
//	printf("%d\n", sizeof(long*));
//	printf("%d\n", sizeof(double*));
//	printf("%d\n", sizeof(int*));
//
//	return 0;
//}

//int* test(int* arr)
//{
//	*arr = 1;
//	return arr;
//}
//
//int main()
//{
//	int arr[2] = { 0 };
//	*arr=test(arr);
//
//	printf("%d", arr[0]);
//
//	return 0;
//}

//#include<stdio.h>
//int* arry(int a[]);
//int main()
//{
//    int a[] = { 0,1,0,1,1,0,1,0 };
//    int* b = arry(a);
//    int i;
//    for (i = 0; i < 8; i++) {
//        printf("%d ", a[i]);
//    }
//    return 0;
//}
//int* arry(int a[])
//{
//    int i;
//    for (i = 0; i < 8; i++) {
//        if (a[i] == 0) {
//            a[i] = 1;
//        }
//        else {
//            a[i] = 0;
//        }
//    }
//    return a;
//}