//#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
////矩阵交换
//
//int main()
//{
//	int n = 0;//行
//	int m = 0;//列
//	scanf("%d %d", &n, &m);
//
//	char op = 0;//字符
//	int a = 0;//行
//	int b = 0;//列
//	int arr[10][10] = { 0 };
//
//	//输入矩阵值
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < m; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//
//	int k = 0;
//	scanf("%d", &k);
//
//	while (k>0)
//	{
//		scanf(" %c %d %d", &op, &a, &b);
//
//		//行变换
//		if (op == 'r')
//		{
//			int temp = 0;
//			for (int i = 0; i < m; i++)
//			{
//				temp = arr[b - 1][i];
//				arr[b - 1][i] = arr[a - 1][i];
//				arr[a - 1][i] = temp;
//			}
//		}
//		else if (op == 'c')//列变换
//		{
//			int temp = 0;
//			for (int i = 0; i < n; i++)
//			{
//				temp = arr[i][a - 1];
//				arr[i][a - 1] = arr[i][b - 1];
//				arr[i][b - 1] = temp;
//			}
//		}
//		k--;
//	}
//	
//	
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < m; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}


#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//矩阵交换

int main()
{
	int n = 0;
	int m = 0;
	scanf("%d %d", &n, &m);
	int arr[10][10] = { 0 };

	//输入矩阵值
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			scanf("%d", &arr[i][j]);
		}
	}

	int k = 0;
	scanf("%d", &k);

	for (int j = 0; j < k; j++)
	{
		char t = 0;
		int a = 0;
		int b = 0;
		//getchar();//消处上次输入留下的\n
		scanf(" %c %d %d", &t, &a, &b);

		//行变换
		if (t == 'r')
		{
			int temp = 0;
			for (int i = 0; i < m; i++)
			{
				temp = arr[b - 1][i];
				arr[b - 1][i] = arr[a - 1][i];
				arr[a - 1][i] = temp;
			}
		}
		else if (t == 'c')//列变换
		{
			int temp = 0;
			for (int i = 0; i < n; i++)
			{
				temp = arr[i][a - 1];
				arr[i][a - 1] = arr[i][b - 1];
				arr[i][b - 1] = temp;
			}
		}
	}


	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}

	return 0;
}