#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int max3(int a, int b, int c)
{
	int max = a;
	if (max < b)
	{
		max = b;
	}

	if (max < c)
	{
		max = c;
	}
	return max;
}

int main()
{
	int a = 0;
	int b = 0;
	int c = 0;
	float m = 0;

	scanf("%d %d %d", &a, &b, &c);

	m = (max3(a + b, b, c)*1.0) / (max3(a, b + c, c) + max3(a, b, c + b));

	printf("%.2f", m);

	return 0;
}