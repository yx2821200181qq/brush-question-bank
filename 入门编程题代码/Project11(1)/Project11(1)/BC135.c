#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
	int count = 0;
	int flag = 0;

	for (int i = 100; i < 1000; i++)
	{
		for (int j = 2; j < i; j++)
		{
			if (i % j == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
		{
			printf("%d\n", i);
			count++;
		}
		flag = 0;
	}

	printf("%d", count);

	return 0;
}