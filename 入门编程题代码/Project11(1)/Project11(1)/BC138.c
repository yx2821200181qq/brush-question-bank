#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

struct Linklist
{
	int date;
	struct Linklist* next;
};

int main()
{
	int n = 0;
	scanf("%d", &n);

	//创建一个空链表
	struct Linklist* list = NULL;//指向列表起始位置
	struct Linklist* tail = NULL;//指向列表最后一个节点


	//插入元素-尾部插入
	for (int i = 0; i < n; i++)
	{
		//动态申请一个节点，在堆区申请
		struct Linklist* pn = (struct Linklist*)malloc(sizeof(struct Linklist));
		
		scanf("%d", &pn->date);
		pn->next = NULL;

		//尾部插入
		if (list == NULL)
		{
			list = pn;
			tail = pn;
		}
		else
		{
			tail->next = pn;
			tail = tail->next;
		}
	}
	//删除数据m
	int m = 0;
	scanf("%d",&m);

	//删除元素
	struct Linklist* cur = list;
	struct Linklist* temp = NULL;
	while (cur != NULL)
	{
		if (cur->date == m)
		{
			//删除
			if (cur == list)//第一个节点
			{
				list = cur->next;
				cur = list;
			}
			else
			{
				temp->next = cur->next;
				cur = cur->next;
			}
		}
		else
		{
			temp = cur;
			cur = cur->next;
		}
	}

	//链表的长度
	int count = 0;
	cur = list;
	while (cur != NULL)
	{
		count++;
		cur = cur->next;
	}
	printf("%d\n", count);

	//打印链表
	cur = list;
	while (cur!=NULL)
	{
		printf("%d ", cur->date);
		cur = cur->next;
	}

	//释放链表
	cur = list;
	while (cur != NULL)
	{
		struct Node* pd = cur;
		cur = cur->next;
		free(pd);
	}
	list = NULL;

	return 0;
}