#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>


 typedef struct TreeNode {
     int val;
     struct TreeNode *left;
     struct TreeNode *right;
 }BTNode;
 

//int SumPlace(int* arr, int* count)
//{
//    int sum = 0;
//    for (int i = 0; i <= *count; i++)
//    {
//        sum = sum * 2 + arr[i];
//    }
//    return sum;
//}
//
//void ProOrder(struct TreeNode* root, int* num, int* arr, int* count)
//{
//    if (root->left == NULL && root->right == NULL)
//    {
//        arr[*count] = root->val;
//        (*num) += SumPlace(arr, count);
//        return;
//    }
//    arr[(*count)++] = root->val;
//    ProOrder(root->left, num, arr, count);
//    ProOrder(root->right, num, arr, count);
//    *count--;
//}
//
//int sumRootToLeaf(struct TreeNode* root) {
//    int num = 0, count = 0;
//    int* arr = (int*)malloc(sizeof(int) * 1001);
//    ProOrder(root, &num, arr, &count);
//    free(arr);
//    return num;
//}

 int Tilt(struct TreeNode* root)
 {
     if (!root)
         return 0;
     if (root->left == NULL && root->right == NULL)
     {
         return root->val;
     }
     return root->val + Tilt(root->left) + Tilt(root->right);
 }

 void ProOrder(struct TreeNode* root)
 {
     if (!root)
         return 0;
     root->val = abs(Tilt(root->left) - Tilt(root->right));
     ProOrder(root->left);
     ProOrder(root->right);
 }

 int Sum(struct TreeNode* root)
 {
     if (!root)
         return;
     if (root->left == NULL && root->right == NULL)
     {
         return root->val;
     }
     return root->val + Sum(root->left) + Sum(root->right);
 }

 int findTilt(struct TreeNode* root) {
     ProOrder(root);
     int sum = Sum(root);
     return sum;
 }

struct TreeNode* BuyNode(int data)
{
    struct TreeNode* node = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    node->val = data;
    node->left = NULL;
    node->right = NULL;
    return node;
}

int main()
{
    BTNode* node1 = BuyNode(1);
    BTNode* node2 = BuyNode(2);
    //BTNode* node3 = BuyNode(1);
    //BTNode* node4 = BuyNode(0);
    //BTNode* node5 = BuyNode(1);
    //BTNode* node6 = BuyNode(0);
    //BTNode* node7 = BuyNode(1);

    node1->left = node2;
    //node1->right = node3;
    //node2->left = node4;
    //node2->right = node5;
    //node3->left = node6;
    //node3->right = node7;
    //sumRootToLeaf(node1);
    printf("%d\n", findTilt(node1));
}