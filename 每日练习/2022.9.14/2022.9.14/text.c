#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int compare(const void* a1, const void* a2)
//{
//    return *(int*)a1 - *(int*)a2;
//}
//
//double trimMean(int* arr, int arrSize) {
//    qsort(arr, arrSize, sizeof(arr[0]), compare);
//    int a = arrSize * 0.05;
//    double num = 0;
//    for (int i = a; i < arrSize - a; i++)
//    {
//        num += arr[i];
//    }
//    num = num * 1.0 / (arrSize - 2 * a);
//    return num;
//}

//#include<stdio.h>
//#include<math.h>
//
//int main()
//{
//    int num = 0;
//    scanf("%d", &num);
//    int count = 0;
//    int i = 0;
//    while (num)
//    {
//        int a = 0;
//        a = num % 10;
//        num = num / 10;
//        if (a % 2 == 0)
//        {
//            count = count * 10;
//        }
//        else
//        {
//            count = count * 10 + 1;
//        }
//        i++;
//    }
//    printf("%d", count);
//
//    return 0;
//}

#include<stdio.h>
#include<math.h>

int main()
{
    int num = 0;
    scanf("%d", &num);
    int arr[10] = { -1 };
    int i = 0;
    while (num)
    {
        if (num % 2 == 0)
            arr[i++] = 0;
        else
            arr[i++] = 1;
        num = num / 10;
    }
    int count = 0;
    while (i--)
    {
        count += arr[i] * pow(10, i);
    }
    printf("%d", count);
    return 0;
}
