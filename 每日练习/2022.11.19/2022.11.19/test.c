﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct {
    int* arr;
    int begine;
    int end;
} MyCircularQueue;

bool myCircularQueueIsEmpty(MyCircularQueue* obj);

MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    obj->arr = (int*)malloc(sizeof(int) * (k + 1));
    obj->begine = 0;
    obj->end = 0;

    return obj;
}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    assert(obj);

    if (obj->begine == 0)
    {
        obj->arr[++obj->begine] = value;
        ++obj->end;
        return true;
    }
    else if (obj->end < (sizeof(obj->arr) / sizeof(int) - 1) && (obj->end >= obj->begine || obj->end + 1 < obj->begine))
    {
        obj->arr[++obj->end] = value;
        return true;
    }
    else if (obj->end == (sizeof(obj->arr) / sizeof(int) - 1) && obj->begine > 1)
    {
        obj->arr[1] = value;
        obj->end = 1;
        return true;
    }

    return false;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    assert(obj);
    if (obj->begine > 0)
    {
        obj->begine++;
        if (obj->end + 1 == obj->begine)
        {
            obj->end = obj->begine = 0;
        }
        if (obj->begine > (sizeof(obj->arr) / sizeof(int) - 1))
        {
            obj->begine = 1;
        }
        return true;
    }
    return false;
}

int myCircularQueueFront(MyCircularQueue* obj) {
    assert(obj);
    if (myCircularQueueIsEmpty(obj))
        return -1;

    return obj->arr[obj->begine];
}

int myCircularQueueRear(MyCircularQueue* obj) {
    assert(obj);
    if (myCircularQueueIsEmpty(obj))
        return -1;

    return obj->arr[obj->end];
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    assert(obj);
    if (obj->begine == 0)
        return true;
    return false;
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    assert(obj);
    if (obj->begine == 1 && obj->end == (sizeof(obj->arr) / sizeof(int) - 1))
        return true;
    else if (obj->begine == obj->end + 1)
        return true;

    return false;
}

void myCircularQueueFree(MyCircularQueue* obj) {
    assert(obj);

    free(obj->arr);
    obj->arr = NULL;
    obj->begine = 0;
    obj->end = 0;
}

int main()
{
    int* arr = (int*)malloc(sizeof(int) * 4);
    int arr1[4] = { 0 };
    printf("%d", sizeof(arr1) / sizeof(int));

    //MyCircularQueue* obj = myCircularQueueCreate(3);// 设置长度为 3
    //myCircularQueueEnQueue(obj,1);// 返回 true
    //myCircularQueueEnQueue(obj, 2);// 返回 true
    //myCircularQueueEnQueue(obj,3);// 返回 true
    //myCircularQueueEnQueue(obj,4);// 返回 false，队列已满
    //circularQueue.Rear();  // 返回 3
    //circularQueue.isFull();  // 返回 true
    //circularQueue.deQueue();  // 返回 true
    //circularQueue.enQueue(4);  // 返回 true
    //circularQueue.Rear();  // 返回 4

    return 0;
}