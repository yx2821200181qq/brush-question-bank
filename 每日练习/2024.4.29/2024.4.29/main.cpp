#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
private:
    void GetDiagonal(vector<vector<int>>& mat, vector<int>& v, int abs, int ord, int row, int line) // 获取对角线所有元素
    {
        for (int i = 0; i + abs < row && i + ord < line; i++)
        {
            v.push_back(mat[i + abs][i + ord]);
        }
    }
    void SetDiagonal(vector<vector<int>>& mat, vector<int>& v, int abs, int ord, int row, int line) // 修改对角线所有元素
    {
        for (int i = 0; i + abs < row && i + ord < line; i++)
        {
            mat[i + abs][i + ord] = v[i];
        }
    }
public:
    vector<vector<int>> diagonalSort(vector<vector<int>>& mat) {
        vector<int> v; // 存放对角线所有元素，并且在此基础上排序
        int row = mat.size();    // 行
        int line = mat[0].size();// 列
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < line; j++)
            {
                if (i != 0 && j != 0) break; // 该对角线已经被排序，无需再次排序
                GetDiagonal(mat, v, i, j, row, line);
                sort(v.begin(), v.end());
                SetDiagonal(mat, v, i, j, row, line);
                v.resize(0);
            }
        }
        return mat;
    }
};

class Solution1 {
public:
    vector<int> findColumnWidth(vector<vector<int>>& grid) {
        vector<int> vtar;
        size_t row = grid.size(), line = grid[0].size();
        for (int i = 0; i < line; i++) // 列
        {
            for (int j = 0; j < row; j++) // 行
            {
                if (grid[j][i] == 0)
                {
                    if (j == 0) vtar.push_back(1);
                    vtar[i] = max(1, vtar[i]);
                    continue;
                }
                int temp = 0;
                if (grid[j][i] < 0)
                {
                    temp = 1;
                }
                while (grid[j][i])
                {
                    temp++;
                    grid[j][i] /= 10;
                }
                if (j == 0) vtar.push_back(temp);
                else vtar[i] = max(temp, vtar[i]);
            }
        }
        return vtar;
    }
};

int main()
{

    return 0;
}