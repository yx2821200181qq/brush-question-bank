#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>

int int_cmp(const void* a1, const void* a2)
{
    return (*(int*)a1) > (*(int*)a2);
}

int* relativeSortArray(int* arr1, int arr1Size, int* arr2, int arr2Size, int* returnSize) {
    *returnSize = arr1Size;
    int* arr = (int*)calloc(1001, sizeof(int));
    int* Array = (int*)calloc(arr1Size, sizeof(int));
    if (!arr || !Array)
    {
        perror("calloc fail!");
        exit(-1);
    }
    for (int i = 0; i < arr1Size; i++)
    {
        arr[arr1[i]]++;
    }
    int cur = 0;
    for (int i = 0; i < arr2Size; i++)
    {
        for (int j = 0; j < arr[arr2[i]]; j++)
        {
            Array[cur++] = arr2[i];
        }
        arr[arr2[i]] = 0;
    }
    int start = cur;
    for (int i = 0; i < arr1Size; i++)
    {
        if (arr[arr1[i]] != 0)
        {
            for (int j = 0; j < arr[arr1[i]]; j++)
            {
                Array[cur++] = arr1[i];
            }
        }
        arr[arr1[i]] = 0;
    }
    qsort(&Array[start], cur - start, sizeof(int), int_cmp);
    return Array;
}

//typedef struct Quere {
//    struct TreeNode* node;
//    int layer;
//}Quere;

//struct TreeNode {
//    int val;
//    struct TreeNode *left;
//    struct TreeNode *right;
//};
//
//bool isEvenOddTree(struct TreeNode* root) {
//    Quere arr[100001] = { 0 };
//    int curLeft = 0, curRight = 0;
//    arr[curRight++].node = root;
//    arr[curRight++].layer = 0;
//    while (curLeft != curRight)
//    {
//        if (arr[curLeft].node == NULL)
//        {
//            break;
//        }
//
//        if (arr[curLeft].node->left && arr[curLeft].node->right)
//        {
//            arr[curRight].node = arr[curLeft].node->left;
//            arr[curRight++].layer = arr[curLeft].layer + 1;
//            arr[curRight].node = arr[curLeft].node->right;
//            arr[curRight++].layer = arr[curLeft].layer + 1;
//        }
//        else if (arr[curLeft].node->left)
//        {
//            arr[curRight].node = arr[curLeft].node->left;
//            arr[curRight++].layer = arr[curLeft].layer + 1;
//        }
//        else if (arr[curLeft].node->right)
//        {
//            arr[curRight].node = arr[curLeft].node->right;
//            arr[curRight++].layer = arr[curLeft].layer + 1;
//        }
//        curLeft++;
//    }
//    curLeft = 0;
//    while (curLeft < curRight)
//    {
//        if (arr[curLeft].node == NULL || curLeft + 1 == curRight)
//        {
//            break;
//        }
//        if (arr[curLeft + 1].layer == arr[curLeft].layer)
//        {
//            if (arr[curLeft].layer % 2 == 1 && arr[curLeft].node->val < arr[curLeft + 1].node->val)
//            {
//                return false;
//            }
//            if (arr[curLeft].layer % 2 == 0 && arr[curLeft].node->val > arr[curLeft + 1].node->val)
//            {
//                return false;
//            }
//        }
//        curLeft++;
//    }
//    return true;
//}


int main()
{
    int arr1[] = { 28,6,22,8,44,17 };
    int arr2[] = { 22,28,8,6 };
    int returnSize = 0;
    relativeSortArray(arr1, sizeof(arr1)/sizeof(arr1[0]), arr2, sizeof(arr2)/sizeof(arr2[0]), &returnSize);

    return 0;
}