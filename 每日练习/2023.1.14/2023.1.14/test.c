#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int H[4] = { -1,0,1,0 };
int L[4] = { 0,1,0,-1 };

//void DFS(int (*image)[3], int x, int y, int sr, int sc, int color, int flag)
//{
//    if (image[sr][sc] == color)
//        return;
//    else
//    {
//        image[sr][sc] = color;
//
//        for (int i = 0; i < 4; i++)
//        {
//            if (sr + H[i] >= 0 && sr + H[i] < x && sc + L[i] >= 0 && sc + L[i] < y)
//            {
//                if (image[sr + H[i]][sc + L[i]] == flag)
//                {
//                    DFS(image, x, y, sr + H[i], sc + L[i], color, flag);
//                }
//            }
//        }
//    }
//}
//
//int** floodFill(int (*image)[3], int imageSize, int imageColSize, int sr, int sc, int color) {
//    int x = imageSize, y = imageColSize;
//    //*returnSize = x;
//    //*returnColumnSizes = (int*)malloc(sizeof(int) * y);
//    //for (int i = 0; i < x; i++)
//    //{
//    //    (*returnColumnSizes)[i] = y;
//    //}
//    int a = image[sr][sc];
//    DFS(image, x, y, sr, sc, color, a);
//    return image;
//}

int max = 0;

void DFS(int** grid, int x, int y, int sr, int sc, int* count)
{
    (*count)++;
    grid[sr][sc] = 2;
    for (int i = 0; i < 4; i++)
    {
        if (sr + H[i] >= 0 && sr + H[i] < x && sc + L[i] >= 0 && sc + L[i] < y)
        {
            if (grid[sr + H[i]][sc + L[i]] == 1)
                DFS(grid, x, y, sr + H[i], sc + L[i], count);
        }
    }
}

int maxAreaOfIsland(int** grid, int gridSize, int* gridColSize) {
    int x = gridSize, y = gridColSize[0];
    int count = 0;
    max = 0;

    for (int i = 0; i < x; i++)
    {
        for (int j = 0; j < y; j++)
        {
            if (grid[i][j] == 1)
            {
                DFS(grid, x, y, i, j, &count);
                printf("%d,%d,%d ", i, j, count);
                if (count > max)
                {
                    max = count;
                }
                count = 0;
            }
        }
        printf("\n");
    }

    return max;
}

int main()
{
    int image[3][3] = { {1,1,1},{1,1,0},{1,0,1} };
    //floodFill(image, 3, 3, 1, 1, 2);
    printf("%d", **image);
    return 0;
}