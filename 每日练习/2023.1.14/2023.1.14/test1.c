#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#define LEN sizeof(struct Student)
struct Student {
	char number[20];
	char name[10];
	int Chinese;
	int Math;
	int English;
	int average;
	struct Student* next;
};
struct Student* head = NULL;
struct Student* end = NULL;
void Menu() {
	void Menu();
	void Add();
	void View();
	system("cls");
	printf("1、学生信息录入\n");
	printf("2、查询全部学生成绩\n");
	printf("5、退出系统\n");
	int choice;
	printf("请选择要使用的功能：\n");
loop:
	scanf("%d", &choice);
	switch (choice) {
	case 1:
		Add();
		Menu();
	case 2:
		View();
		Menu();
	case 5:
		exit(0);
	default:
		printf("输入有误");
		goto loop;
	}
}
void Add() {
	system("cls");
	int count;
	printf("请输入要录入信息的个数：");
	scanf("%d", &count);
	struct Student* s1, * s2, * temp;
	s1 = s2 = (struct Student*)malloc(LEN);
	if (head == NULL) {
		head = s1;
	}
	else {
		end->next = s1;
	}
	int i = 1;
	while (count > 0) {
		printf("请输入第%d位学生信息：\n", i++);
		printf("学号：\n");
		scanf("%s", s1->number);
		printf("姓名：\n");
		scanf("%s", s1->name);
		printf("语文成绩：\n");
		scanf("%d", &s1->Chinese);
		printf("数学成绩:\n");
		scanf("%d", &s1->Math);
		printf("英语成绩:\n");
		scanf("%d", &s1->English);
		count--;
		/*if (count == 0)
			break;
		s2 = (struct Student*)malloc(LEN);
		s1->next = s2;*/
		//s1 = s2;
		s2->next = s1;
		s2 = s1;
		s1 = (struct Student*)malloc(LEN);
		printf("添加成功！\n");
		system("pause");
		system("cls");
	}
	s2->next = NULL;
	end = s2;
	printf("全部添加成功！\n");
	system("pause");
}
void View() {
	system("cls");
	struct Student* p;
	if (head != NULL) {
		printf("所有学生成绩如下：\n");
		for (p = head; p != NULL;) {
			p->average = (p->Chinese + p->Math + p->English) / 3;
			printf("学号：%s\t姓名：%s\n", p->number, p->name);
			printf("语文：%d\t数学:%d\t英语：%d\t平均分：%d\n", p->Chinese, p->Math, p->English, p->average);
			p = p->next;
		}
	}
	system("pause");
}
int main() {
	void Menu();
	void Add();
	void View();
	Menu();
}