#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

char* Trans(char* arr, int n)
{
    int num[5] = { 0 };
    int i = 0;
    while (n)
    {
        num[i] = n % 10;
        n = n / 10;
        i++;
    }
    while (i >= 0)
    {
        i--;
        *arr++ = num[i] + '0';
    }
    return arr;
}

char* compressString(char* S) {
    int cur = 0;
    int len = strlen(S);
    char flag = S[0];
    int num = 0;
    char* arr = (char*)malloc(len * 2);
    char* tmp = arr;
    while (cur < len)
    {
        if (flag == S[cur])
        {
            num++;
            cur++;
        }
        else
        {
            *tmp++ = flag;
            if (num < 10)
                *tmp++ = num + '0';
            else
                tmp = Trans(tmp, num);
            flag = S[cur];
            num = 0;
        }
    }
    *tmp++ = flag;
    if (num < 10)
        *tmp++ = num + '0';
    else
        Trans(tmp, num);
    *tmp = '\0';
    if (strlen(arr) < strlen(S))
        return arr;
    else
        return S;
}

int exchangeBits(int num) {
    for (int i = 1; i < 31; i += 2)
    {
        int flag1 = 1, flag2 = 1;
        flag1 = num & (flag1 << (i - 1));
        flag2 = num & (flag2 << i);
        if (flag1 != (flag2 >> 1))
        {
            if (flag1 == 0)
            {
                num = num ^ (flag2 >> 1);
                num = num ^ flag2;
            }
            else
            {
                num = num ^ (flag1 << 1);
                num = num ^ flag1;
            }
        }
    }
    return num;
}

int main()
{
   /* char* S = "VVVVVVVVVVDDDDDDDDDDIIIIIIIIIIlllllllAAAAqqqqqqqbbbNNNNffffff";
    printf("%s",compressString(S));*/

    int num = 571603718;
    printf("%d",exchangeBits(num));

    return 0;
}