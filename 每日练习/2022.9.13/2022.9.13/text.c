#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>

//int compare(const void* a1, const void* a2)
//{
//    return *(int*)a1 - *(int*)a2;
//}
//
//int maximumSwap(int num) {
//    int count = 0;
//    int a = num;
//    int* arr = (int*)malloc(sizeof(int) * count);
//    int* arr1 = (int*)malloc(sizeof(int) * count);
//
//    while (a)
//    {
//        a = a / 10;
//        count++;
//    }
//
//    int i = 0;
//    int j = 1;
//    while (i < count)
//    {
//        arr[i] = num % 10;
//        num = num / 10;
//        i++;
//    }
//    i = 0;
//
//    while (i < count)
//    {
//        arr1[i] = arr[i];
//        i++;
//    }
//    qsort(arr1, count, sizeof(arr1[0]), compare);
//
//    int b1 = 0, b2 = 0;
//    for (i = count - 1; i >= 0; i--)
//    {
//        if (arr[i] != arr1[i])
//        {
//            b1 = arr[i];
//            b2 = arr1[i];
//            arr[i] = arr1[i];
//        }
//    }
//    for (i = 0; i < count; i++)
//    {
//        if (b2 == arr[i])
//        {
//            arr[i] = b1;
//        }
//    }
//    i = 0;
//    while (i<count)
//    {
//        num += arr[i] * pow(10, i);
//        i++;
//    }
//    return num;
//}

//int compare(const void* a1, const void* a2)
//{
//    return *(int*)a1 - *(int*)a2;
//}
//
//int maximumSwap(int num) {
//    int* arr = (int*)malloc(sizeof(int) * 9);
//    int count = 0;
//    while (num)
//    {
//        arr[count++] = num % 10;
//        num = num / 10;
//    }
//    int b1 = arr[count - 1];
//    int max = -1;
//    int boa = 0;
//    for (int i = 0; i < count - 1; i++)
//    {
//        if (max < arr[i])
//        {
//            max = arr[i];
//            boa = i;
//        }
//    }
//    if (max > b1)
//    {
//        arr[boa] = b1;
//        arr[count - 1] = max;
//    }
//    int i = 0;
//    while (i < count)
//    {
//        num += arr[i] * pow(10, i);
//        i++;
//    }
//    return num;
//}

//int maximumSwap(int num) {
//    if (num < 10)
//    {
//        return num;
//    }
//    int* arr = (int*)malloc(sizeof(int) * 9);
//    int count = 0;
//    while (num)
//    {
//        arr[count++] = num % 10;
//        num = num / 10;
//    }
//    int b1 = arr[count - 1];
//    int max = -1;
//    int min = 10;
//    int boa = 0, boa1 = 0;
//    for (int i = 0; i < count - 1; i++)
//    {
//        if (max < arr[i])
//        {
//            max = arr[i];
//            boa = i;
//        }
//        if (min > arr[i])
//        {
//            min = arr[i];
//            boa1 = i;
//        }
//    }
//    if (max > b1)
//    {
//        arr[boa] = b1;
//        arr[count - 1] = max;
//    }
//    if (max<b1 && boa1>boa)
//    {
//        arr[boa] = min;
//        arr[boa1] = max;
//    }
//
//    int i = 0;
//    while (i < count)
//    {
//        num += arr[i] * pow(10, i);
//        i++;
//    }
//    return num;
//}

int Max(int* arr, int count)
{
    int max = -1;
    int pro = 0;
    for (int i = 0; i < count; i++)
    {
        if (max < arr[i])
        {
            pro = i;
            max = arr[i];
        }
    }
    return pro;
}

int maximumSwap(int num) {
    if (num < 10)
    {
        return num;
    }
    int* arr = (int*)malloc(sizeof(int) * 9);
    int count = 0;
    while (num)
    {
        arr[count++] = num % 10;
        num = num / 10;
    }
    for (int i = count - 1; i >= 0; i--)
    {
        int index = Max(arr, i);
        if (arr[index] > arr[i])
        {
            int temp = arr[index];
            arr[index] = arr[i];
            arr[i] = temp;
            break;
        }
    }
    for (int i = 0; i < count; i++)
        num += arr[i] * pow(10, i);

    return num;
}

int main()
{
    printf("%d", maximumSwap(9974));
	return 0;
}