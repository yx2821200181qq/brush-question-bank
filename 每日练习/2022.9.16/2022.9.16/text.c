#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int strStr(char* haystack, char* needle) {
//    char* start1 = haystack;
//    char* end1 = haystack;
//    char* end2 = needle;
//    while (*end1)
//    {
//        while (*end1 == *end2 && *end2 != '\0')
//        {
//            end1++;
//            end2++;
//        }
//        if (*end2 == '\0')
//            return start1 - haystack;
//        end2 = needle;
//        end1 = start1++;
//    }
//    return -1;
//}

//void GetNext(char* needle, int next[])
//{
//    int a = strlen(needle);
//    next[0] = -1;
//    next[1] = 0;
//    int k = 0;
//    int i = 2;
//    while (i < a)
//    {
//        if (k == -1 || needle[k] == needle[i - 1])
//        {
//            next[i] = k + 1;
//            k++;
//            i++;
//        }
//        else
//        {
//            k = next[k];
//        }
//    }
//}
//
//int strStr(char* haystack, char* needle) {
//    int a = strlen(needle);
//    int* next = (int*)malloc(sizeof(int) * a);
//    if(a>2)
//        GetNext(needle, next);
//    else
//    {
//        if(a==1)
//            next[0] = -1;
//        if (a == 2)
//        {
//            next[0] = -1;
//            next[1] = 0;
//        }
//    }
//    int i = 0, j = 0;
//    while (i < strlen(haystack))
//    {
//        while (j == -1 || haystack[i] == needle[j])
//        {
//            i++;
//            j++;
//        }
//        if (needle[j] == '\0')
//            return i - a + 1;
//        j = next[j];
//    }
//    return -1;
//}
#include<stdbool.h>
//bool isPalindrome(char* s) {
//    int a = strlen(s);
//    int count = 0;
//    for (int i = 0; i < a; i++)
//    {
//        //if (isupper(s[i]))
//        //    *(s+i) = tolower(s[i]);
//        if (isspace(s[i]))
//            count++;
//        if (ispunct(s[i]))
//            count++;
//        if (islower(s[i]))
//            s[i - count] = s[i];
//    }
//    a = a - count;
//    char* end = &s[a - 1];
//    char* start = s;
//    while (start < end)
//    {
//        if (tolower(*start) == tolower(*end))
//        {
//            start++;
//            end--;
//        }
//        else
//            return false;
//    }
//    return true;
//}

bool isPalindrome(char* s) {
    int a = strlen(s);
    char* arr = (char*)malloc(sizeof(char) * a);
    strcpy(arr, s);
    int count = 0;
    for (int i = 0; i < a; i++)
    {
        if (isupper(arr[i]))
            arr[i] = tolower(arr[i]);
        if (isspace(arr[i]))
            count++;
        if (ispunct(arr[i]))
            count++;
        if (islower(arr[i]))
        {
            arr[i - count] = arr[i];
            //arr[i] = ' ';
        }
    }
    a = a - count;
    char* end = &arr[a - 1];
    char* start = arr;
    while (start < end)
    {
        if (*start == *end)
        {
            start++;
            end--;
        }
        else
            return false;
    }
    return true;
}

int main()
{
    /*strStr("hello", "ll");*/
    //printf("%d", printf("%d\n", printf("132\n")));
    printf("%d", isPalindrome(" "));
	return 0;
}