#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>

//bool checkInclusion(char* s1, char* s2) {
//    int len1 = strlen(s1), len2 = strlen(s2);
//    int left = 0, right = len1 - 1;
//    int* arr = (int*)calloc(len1, sizeof(int));
//    int* arr1 = (int*)calloc(len1, sizeof(int));
//    while (right < len2)
//    {
//        for (int i = 0; i < len1; i++)
//        {
//            int flag = 1;
//            for (int j = left; j <= right; j++)
//            {
//                if (s1[i] == s2[j] && arr[i] == 0 && arr1[j-left] == 0)
//                {
//                    flag = 0;
//                    arr[i] = 1;
//                    arr1[j - left] = 1;
//                    break;
//                }
//            }
//            if (flag)
//                break;
//        }
//        int count = 0;
//        for (int i = 0; i < len1; i++)
//        {
//            if (arr[i] == 1)
//            {
//                count++;
//                arr1[i + left] = 0;
//                arr[i] = 0;
//            }
//        }
//        if (count == len1)
//        {
//            return true;
//        }
//        left++;
//        right++;
//    }
//    return false;
//}

//bool checkInclusion(char* s1, char* s2) {
//    int len1 = strlen(s1), len2 = strlen(s2);
//    if (len1 > len2)
//        return false;
//    int num1 = 0, num2 = 0;
//    for (int i = 0; i < len1; i++)
//        num1 ^= s1[i];
//    int left = 0, right = len1 - 1;
//    for (int i = left; i <= right; i++)
//    {
//        num2 ^= s2[i];
//    }
//    while (right < len2)
//    {
//        if (left != 0)
//        {
//            num2 ^= s2[left - 1];
//            num2 ^= s2[right];
//        }
//        if ((num2 ^ num1) == 0)
//            return true;
//        right++;
//        left++;
//    }
//    return false;
//}

bool checkInclusion(char* s1, char* s2) {
    int len1 = strlen(s1), len2 = strlen(s2);
    if (len1 > len2)
        return false;

    int left = 0, right = len1 - 1;
    int arr[26] = { 0 };
    int arr1[26] = { 0 };
    for (int i = 0; i < len1; i++)
    {
        arr[s1[i] - 'a']++;
        arr1[s2[i] - 'a']++;
    }
    while (right < len2)
    {
        if (left != 0)
        {
            arr1[s2[left - 1] - 'a']--;
            arr1[s2[right] - 'a']++;
        }
        int flag = 1;
        for (int i = 0; i < 26; i++)
        {
            if (arr[i] != arr1[i])
            {
                flag = 0;
            }
        }
        if (flag)
            return true;
        right++;
        left++;
    }
    return false;
}

int main()
{
    char* s1 = "ab";
    char* s2 = "eidbaooo";
    printf("%d", checkInclusion(s1, s2));

    return 0;
}