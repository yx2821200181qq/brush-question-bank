#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<vector>
#include<queue>
using namespace std;

//void Print(int* arr, int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//void ShellSort(int* arr, int n)
//{
//	int gap = n / 2;
//	while (gap)
//	{
//		for (int i = 0; i < n - gap; i++)
//		{
//			for (int j = i + gap; j > 0; j -= gap)
//			{
//				if (arr[j] < arr[j - gap])
//				{
//					int temp = arr[j];
//					arr[j] = arr[j - gap];
//					arr[j - gap] = temp;
//				}
//				else
//					break;
//			}
//			Print(arr, n);
//		}
//		gap = gap / 2;
//	}
//}
//
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	ShellSort(arr, 10);
//
//	return 0;
//}

//struct TreeNode {
//    int val;
//    TreeNode* left;
//    TreeNode* right;
//    TreeNode() : val(0), left(nullptr), right(nullptr) {}
//    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
//};
//
//void Work(TreeNode* root, string& s)
//{
//    if (!root) return;
//    s += to_string(root->val);
//
//    if (root->left)
//    {
//        s.push_back('(');
//        Work(root->left, s);
//    }
//    else if (!root->left && root->right) s += "()";
//    if (root->right)
//    {
//        s.push_back('(');
//        Work(root->right, s);
//    }
//    s.push_back(')');
//}
//
//string tree2str(TreeNode* root)
//{
//    if (!root) return string();
//    string s;
//    Work(root, s);
//    s.pop_back();
//    return s;
//}
//
////二叉树层序遍历
//void Work(vector<vector<int>>& vv, TreeNode* root)
//{
//    queue<TreeNode*> q;
//    q.push(root);
//    while (!q.empty())
//    {
//        vector<int> temp;
//        int n = q.size();
//        while (n--)
//        {
//            TreeNode* node = q.front();
//            q.pop();
//            temp.push_back(node->val);
//            if (node->left) q.push(node->left);
//            if (node->right) q.push(node->right);
//        }
//        vv.push_back(temp);
//    }
//    reverse(vv.begin(), vv.end());
//}
//vector<vector<int>> levelOrderBottom(TreeNode* root) {
//    if (!root) return vector<vector<int>>();
//    vector<vector<int>> vv;
//    Work(vv, root);
//    return vv;
//}
//
//
////二叉树最近公共祖先
//void findNode(TreeNode* root, TreeNode* p, TreeNode* q, int& flag)
//{
//    if (!root) return;
//    if (root == p || root == q) flag++;
//    findNode(root->left, p, q, flag);
//    findNode(root->right, p, q, flag);
//}
//TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//    while (true)
//    {
//        int left = 0, right = 0;
//        if (root->left) findNode(root->left, p, q, left);
//        if (root->right) findNode(root->right, p, q, right);
//
//        if (left == right || left == 1 || right == 1) return root;
//        else if (left == 2) root = root->left;
//        else root = root->right;
//    }
//    return nullptr;
//}
//
////236. 二叉树的最近公共祖先
//TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//    if (!root) return nullptr;
//    if (root == p || root == q) return root;
//
//    TreeNode* left = lowestCommonAncestor(root->left, p, q);
//    TreeNode* right = lowestCommonAncestor(root->right, p, q);
//
//    if (!right) return left;
//    if (!left) return right;
//    return root;
//}

//int main()
//{
//    char a = 10;
//    char b = 126;
//    char c = a + b;
//    printf("%d", c);
//
//	return 0;
//}