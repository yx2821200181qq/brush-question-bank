#include <iostream>
#include <vector>
#include <map>
using namespace std;

int countTestedDevices(vector<int>& batteryPercentages) {
    int num = 0;
    for (auto it : batteryPercentages)
    {
        if (it - num > 0) num++;
    }
    return num;
}

int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
    int gasNUm = 0, costNum = 0;
    int flag = -1, i = 0;
    while (!((flag == -1 && i == cost.size()) || flag == i))
    {
        gasNUm += gas[i];
        costNum += cost[i];
        if (gasNUm < costNum)
        {
            gasNUm = 0;
            costNum = 0;
            if (flag != -1)
            {
                i = flag++;
                flag = -1;
                continue;
            }
        }
        else if (flag == -1)
        {
            flag = i;
        }
        i++;
        if (i == cost.size() && flag != -1)
        {
            i = 0;
        }
    }
    return flag;
}

vector<char> vs = { 'I', 'V', 'X', 'L', 'C', 'D', 'M' };
vector<int> vi = { 1, 5, 10, 50, 100, 500, 1000 };
int romanToInt(string s) {
    map<char, int> msi;
    for (int i = 0; i < vs.size(); i++)
    {
        msi[vs[i]] = vi[i];
    }
    int tar = 0;
    for (int i = 0; i < s.size(); i++)
    {
        if (i != s.size() - 1)
        {
            if (msi[s[i]] >= msi[s[i + 1]])
            {
                tar += msi[s[i]];
            }
            else
            {
                tar += 1 - msi[s[i]];
            }
        }
        else tar += msi[s[i]];
    }
    return tar;
}

int main()
{
    /*vector<int> gas = { 2,3,4 };
    vector<int> cost = { 3,4,3 };
    canCompleteCircuit(gas, cost);*/

    romanToInt(string("MCMXCIV"));
	return 0;
}