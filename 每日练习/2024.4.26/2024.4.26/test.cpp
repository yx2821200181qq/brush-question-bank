#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;

class SnapshotArray {
private:
    vector<vector<pair<int, int>>> Sparr;
    size_t id;
public:
    SnapshotArray(int length)
        :id(0)
        , Sparr(length, vector<pair<int, int>>())
    {}

    void set(int index, int val) {
        Sparr[index].emplace_back(id, val);
    }

    int snap() {
        return id++;
    }

    //int get(int index, int snap_id) {
    //    // 二分查找
    //    //int left = 0;
    //    //int right = Sparr[index].size() - 1;
    //    //while (left < right)
    //    //{
    //    //    int mid = left + ((right - left) >> 1);
    //    //    if (Sparr[index][mid].first < snap_id) left = mid + 1;
    //    //    else right = mid;
    //    //}
    //    pair<int, int> p = make_pair(snap_id + 1, -1);
    //    auto x = upper_bound(Sparr[index].begin(), Sparr[index].end(), p);
    //    return x == Sparr[index].begin() ? 0 : prev(x)->second;
    //}

    int get(int index, int snap_id) {
        // 二分查找
        int left = 0;
        int right = Sparr[index].size() - 1;
        while (left < right)
        {
            int mid = left + ((right - left) >> 1) + 1;
            if (Sparr[index][mid].first > snap_id) right = mid - 1;
            else left = mid;
        }
        if (Sparr[index].empty() || Sparr[index][0].first > snap_id) return 0;
        //if (left != 0 && Sparr[index][left].first > snap_id) return Sparr[index][left - 1].second;
        //else if (Sparr[index][left].first <= snap_id) return Sparr[index][left].second;
        else return Sparr[index][left].second;
        // return Sparr[index][left].first != snap_id ? Sparr[0][0].second : Sparr[index][left].second;
        // return Sparr[index][0].second;
        // return 1;
        // auto x = upper_bound(Sparr[index].begin(), Sparr[index].end(), pair{snap_id + 1, -1});
        // return x == Sparr[index].begin() ? 0 : prev(x)->second;
    }
};

int main()
{
    SnapshotArray sa(4);
    sa.snap();
    sa.snap();
    sa.get(3, 1);
    sa.set(2, 4);
    sa.snap();

	return 0;
}