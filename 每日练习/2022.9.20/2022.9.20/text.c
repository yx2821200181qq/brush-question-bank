#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>

int compare(const void* a1, const void* a2)
{
    return *(int*)a2 - *(int*)a1;
}

int minus(int* nums, int sum, int numsSize, int* s)
{
    (*s)++;
    for (int i = 0; i < numsSize; i++)
    {
        if (sum >= nums[i])
        {
            sum = sum - nums[i];
            nums[i] = 0;
        }
        if (sum == 0)
        {
            return 1;
        }
    }
    return 0;
}
//0 3871 3522 2646 2512 1932 922 521 515 0 312 304 269 181 146 123
bool canPartitionKSubsets(int* nums, int numsSize, int k) {
    qsort(nums, numsSize, sizeof(nums[0]), compare);
    int sum = 0;
    int c = 0;
    int s = 0;
    for (int i = 0; i < numsSize; i++)
    {
        printf("%d ", nums[i]);
        sum += nums[i];
    }
    if (sum % k != 0)
        return false;
    sum = sum / k;
    if (sum < nums[0])
        return false;

    for (int i = 0; i < numsSize; i++)
    {
        c = minus(nums, sum, numsSize,&s);
        int harm = 0;
        for (int i = 0; i < numsSize; i++)
        {
            harm += nums[i];
        }
        if (harm == 0)
            break;
        if (c == 0)
        {
            return false;
        }
    }
    if (s != k)
        return false;
    return true;
}

int main()
{
    int nums[] = { 3522,181,521,515,304,123,2512,312,922,407,146,1932,4037,2646,3871,269 };
    int k = 5;
    printf("%d",canPartitionKSubsets(&nums, sizeof(nums) / sizeof(nums[0]), k));
    return 0;
}