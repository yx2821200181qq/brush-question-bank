#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;


// 常规填坑法
//int n = 0;
//int arr[17] = { 0 };
//
//void bfs(int pos, int num)
//{
//    if (pos == n + 1) // 该层递归结束
//    {
//        for (int i = 0; i < num; i++)
//        {
//            cout << arr[num] << " ";
//        }
//        cout << endl;
//    }
//
//    for (int i = 0; i < 2; i++)
//    {
//        if (i == 0) // 选
//        {
//            arr[num] = pos;
//            bfs(pos + 1, num + 1);
//        }
//        else // 不选
//        {
//            bfs(pos + 1, num);
//        }
//    }
//}

// 二进制判断
int n = 0;
int flag = 0;

void bfs(int pos)
{
    if (pos == n + 1) // 到达终点结束递归
    {
        for (int i = 1; i < pos; i++)
        {
            if (flag & (1 << i)) cout << i << " ";
        }
        cout << endl;
        return;
    }

    flag ^= 1 << pos;
    bfs(pos + 1);

    flag ^= 1 << pos;
    bfs(pos + 1);
}

int main()
{
    cin >> n;
    //bfs(1, 0);
    bfs(1);

    return 0;
}