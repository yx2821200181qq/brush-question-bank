#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
using namespace std;

static constexpr double INFY = 1e20;

int minSkips(vector<int>& dist, int speed, int hoursBefore) {
    int n = dist.size();
    vector<vector<double>> f(n + 1, vector<double>(n + 1, INFY));
    f[0][0] = 0;

    for (int i = 1; i <= n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (j == 0) // 一次跳过都没有时
            {
                f[i][j] = ceil(f[i - 1][j]) + double(dist[i - 1]) / speed;
            }
            else if (i - 1 != j)
            {
                f[i][j] = min(f[i - 1][j - 1] + double(dist[i - 1]) / speed, ceil(f[i - 1][j]) + double(dist[i - 1]) / speed);
            }
            else // i条路时全部跳过
            {
                f[i][j] = f[i - 1][j - 1] + double(dist[i - 1]) / speed;
            }
        }
    }
    for (int i = n; i >= 0; i--)
    {
        if (f[n][i] < hoursBefore) return i;
    }
    return -1;
}

    // 可忽略误差
    static constexpr double EPS = 1e-7;
    // 极大值
    static constexpr double INFTY = 1e20;

    int minSkips1(vector<int>& dist, int speed, int hoursBefore) {
        int n = dist.size();
        vector<vector<double>> f(n + 1, vector<double>(n + 1, INFTY));
        f[0][0] = 0.;
        for (int i = 1; i <= n; ++i) {
            for (int j = 0; j <= i; ++j) {
                if (j != i) {
                    f[i][j] = min(f[i][j], ceil(f[i - 1][j] + (double)dist[i - 1] / speed - EPS));
                }
                if (j != 0) {
                    f[i][j] = min(f[i][j], f[i - 1][j - 1] + (double)dist[i - 1] / speed);
                }
            }
        }
        for (int j = 0; j <= n; ++j) {
            if (f[n][j] < hoursBefore + EPS) {
                return j;
            }
        }
        return -1;
    }

int main()
{
    vector<int> v = { 7,3,5,5 };
    int s = 2;
    int h = 10;
    cout << minSkips(v, s, h);
	return 0;
}