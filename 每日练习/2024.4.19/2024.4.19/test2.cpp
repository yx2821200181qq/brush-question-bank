#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
using namespace std;

class Solution {
private:
    void dfs(vector<int>& num, vector<bool>& flag, vector<vector<int>>& tar)
    {
        if (num.size() == v.size())
        {
            tar.push_back(v);
            return;
        }
        for (int i = 0; i < num.size(); i++)
        {
            if (flag[i] == true)
            {
                flag[i] = false;
                v.push_back(num[i]);
                dfs(num, flag, tar);
                flag[i] = true;
                v.resize(v.size() - 1);
            }
        }
    }
public:
    vector<vector<int>> permute(vector<int>& nums) {
        vector<bool> flag(7, true);
        vector<vector<int>> tar;
        dfs(nums, flag, tar);
        return tar;
    }
private:
    vector<int> v;
};

int main()
{
    Solution s;
    vector<int> v = { 0,1 };
    s.permute(v);

    return 0;
}