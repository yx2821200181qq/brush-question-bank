#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

double average(vector<int>& salary) {
    sort(salary.begin(), salary.end());
    double tar = 0.0;
    for (int i = 1; i < salary.size() - 1; i++)
    {
        tar += salary[i];
    }
    return tar / (salary.size() - 2);
}

// 跳跃游戏
class JumpPlay1 // 超出时间限制
{
private:
    bool flag = false;
    void Work(vector<int>& nums, int index)
    {
        if (flag) return;
        for (int i = nums[index]; i > 0; i--)
        {
            if (nums.size() - 1 <= index + i)
            {
                flag = true;
                return;
            }
            Work(nums, index + i);
        }
    }
public:
    bool canJump(vector<int>& nums) {
        if (nums.size() == 1) return true;
        Work(nums, 0);
        return flag;
    }
};

class JumpPlay2 // 解决问题
{
private:
    int Work(vector<int>& nums, int index, int right)
    {
        pair<int, int> p;
        for (int i = index + 1; i <= right && i < nums.size(); i++)
        {
            if (i == index + 1)
            {
                p.first = i;
                p.second = 1 + nums[i];
                continue;
            }
            if (p.second <= i - index + nums[i])
            {
                p.first = i;
                p.second = i - index + nums[i];
            }
        }
        return p.first;
    }
public:
    bool canJump(vector<int>& nums) {
        if (nums.size() == 1) return true;
        int index = 0, n = nums.size() - 1;
        while (index != n)
        {
            index = Work(nums, index, nums[index] + index);
            if (nums[index] == 0 && index != n)
                return false;
            if (index + nums[index] >= n)
                return true;
        }
        return false;
    }
};

class Solution {
private:
    int Work(vector<int>& nums, int index, int right)
    {
        if (right >= nums.size() - 1) return nums.size() - 1;
        pair<int, int> p;
        for (int i = index + 1; i <= right && i < nums.size(); i++)
        {
            if (i == index + 1)
            {
                p.first = i;
                p.second = 1 + nums[i];
                continue;
            }
            if (p.second <= i - index + nums[i])
            {
                p.first = i;
                p.second = i - index + nums[i];
            }
        }
        return p.first;
    }
public:
    int jump(vector<int>& nums) {
        if (nums.size() == 1) return 0;
        int index = 0, n = nums.size() - 1, num = 0;
        while (index != n)
        {
            index = Work(nums, index, nums[index] + index);
            num++;
        }
        return num;
    }
};

int main()
{
    Solution s;
    vector<int> v = { 2,3,1,1,4 };
    s.jump(v);

    return 0;
}