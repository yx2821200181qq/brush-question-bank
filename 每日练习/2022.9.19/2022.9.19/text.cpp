#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

//typedef struct Num
//{
//    int key;
//    int count;
//    struct num* next;
//}Num;
//
//void Insert(int a,Num* end)
//{
//
//}
//
//void judge(int a, Num* num)
//{
//    if (num->count == 0)
//    {
//        num->key = judge;
//        num->count++;
//        return;
//    }
//    Num* temp = num;
//    Num* end;
//    while (temp != NULL)
//    {
//        if (temp->key == a)
//        {
//            temp->count++;
//            return;
//        }
//        if (temp->next == NULL)
//            end = temp;
//        temp = temp->next;
//    }
//    end->count = 1;
//    end->key = a;
//    end->next = NULL;
//}
//
//int* frequencySort(int* nums, int numsSize, int* returnSize) {
//    Num num;
//    num.key = 0;
//    num.count = 0;
//    num.next = NULL;
//    for (int i = 0; i < numsSize; i++)
//    {
//        judge(nums[i], &num);
//    }
//}

//#include <iostream>
//using namespace std;
//
//int main() {
//    int ver = _MSC_VER; // get the version
//    cout << "MSVC Version: " << ver << endl;
//    return 0;
//}

typedef struct Num
{
    int key;
    int count;
}Num;

typedef struct S
{
    Num num[201];
    int ab;
}s;

void Insert(int a, s* arr)
{
    if (arr->ab == 0)
    {
        arr->num[arr->ab].key = a;
        arr->num[arr->ab++].count++;
        return;
    }
    for (int i = 0; i < arr->ab; i++)
    {
        if (a == arr->num[i].key)
        {
            arr->num[i].count++;
            return;
        }
    }
    arr->num[arr->ab].key = a;
    arr->num[arr->ab++].count++;
}

int compare(const void* a1, const void* a2)
{
    if ((*(Num*)a1).count != (*(Num*)a2).count)
    {
        return ((Num*)a1)->count - ((Num*)a2)->count;
    }
    return ((Num*)a2)->key - ((Num*)a1)->key;
}

int* frequencySort(int* nums, int numsSize, int* returnSize) {
    s arr;
    arr.ab = 0;
    memset(arr.num, 0, sizeof(arr.num));
    for (int i = 0; i < numsSize; i++)
    {
        Insert(nums[i], &arr);
    }
    printf("%d\n", sizeof(arr.num[0]));
    //printf("%d\n", sizeof(Num) * arr.ab);

    qsort(arr.num, arr.ab, sizeof(arr.num[0]), compare);
    int* arr1 = (int*)malloc(sizeof(int) * numsSize);
    int c = 0;
    for (int i = 0; i < arr.ab; i++)
    {
        for (int j = 0; j < arr.num[i].count; j++)
        {
            arr1[c++] = arr.num[i].key;
        }
    }
    *returnSize = numsSize;
    return arr1;
}

int main()
{
    int nums[] = { -1,1,-6,4,5,-6,1,4,1 };
    int returnSize = 0;
    frequencySort(nums, sizeof(nums) / sizeof(nums[0]), &returnSize);
    return 0;
}