#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//int main()
//{
//	int n = 0;
//	cout << "���������֣�";
//	cin >> n;
//	int count = 0;
//
//	for (int i = 1; i <= n; i++)
//	{
//		int a = i;
//		int sum1 = 0;
//		while (a)
//		{
//			sum1 += a % 10;
//			a = a / 10;
//		}
//
//		int b = i;
//		int sum2 = 0;
//		while (b)
//		{
//			sum2 += b % 2;
//			b = b / 2;
//		}
//		if (sum1 == sum2)
//		{
//			count++;
//		}
//	}
//	cout << count;
//
//	return 0;
//}

//int* spiralOrder(int** matrix, int matrixSize, int* matrixColSize, int* returnSize) {
//    int n = 0;
//    int flag = 1;
//    int* arr = (int*)malloc(sizeof(int) * matrixSize * matrixColSize[0]);
//    char** arr1 = (char**)malloc(sizeof(char*) * matrixSize);
//    for (int i = 0; i < matrixSize; i++)
//    {
//        arr1[i] = (char*)malloc(sizeof(char) * matrixColSize[0]);
//        for (int j = 0; j < matrixColSize[0]; j++)
//        {
//            arr1[i][j] = '0';
//        }
//    }
//    int i = 0, j = 0;
//
//    while (n < matrixSize * matrixColSize[0])
//    {
//        arr[n++] = matrix[i][j];
//        arr1[i][j] = '1';
//        switch (flag)
//        {
//        case 1:
//            if (j + 1 == matrixColSize[0] - 1 || arr1[i][j + 1] == '1')
//            {
//                flag = 2;
//            }
//            j++;
//            continue;
//        case 2:
//            if (i + 1 == matrixSize - 1 || arr1[i + 1][j] == '1')
//            {
//                flag = 3;
//            }
//            i++;
//            continue;
//        case 3:
//            if (j - 1 == 0 || arr1[i][j - 1] == '1')
//            {
//                flag = 4;
//            }
//            j--;
//            continue;
//        case 4:
//            if (arr1[i - 2][j] == '1')
//            {
//                flag = 1;
//            }
//            i--;
//            continue;
//        }
//    }
//    *returnSize = matrixSize * matrixColSize[0];
//    return arr;
//}

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
int** generateMatrix(int n, int* returnSize) {
    *returnSize = n;
    //*returnColumnSizes[0] = n;
    int** matrix = (int**)malloc(sizeof(int*) * n);
    for (int i = 0; i < n; i++)
    {
        matrix[i] = (int*)malloc(sizeof(int) * n);
    }
    int num = 1;
    int up = 0;
    int below = n - 1;
    int left = 0;
    int right = n - 1;
    while (up <= below || left <= right)
    {
        for (int i = left; i <= right; i++)
        {
            matrix[up][i] = num++;
        }
        up++;
        for (int i = up; i <= below; i++)
        {
            matrix[i][right] = num++;
        }
        right--;
        for (int i = right; i >= left; i--)
        {
            matrix[below][i] = num++;
        }
        below--;
        for (int i = below; i >= up; i--)
        {
            matrix[i][left] = num++;
        }
        left++;
    }
    return matrix;
}

int* findDiagonalOrder(int mat[3][3], int matSize, int* matColSize, int* returnSize) {
    *returnSize = matSize * matColSize[0];
    int* arr = (int*)malloc(sizeof(int) * (*returnSize));
    int n = 0;
    int a = 0;
    while (a < (matSize * matColSize[0]))
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (i + j == n)
                {
                    arr[a++] = mat[j][i];
                }
            }
        }
        n++;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (i + j == n)
                {
                    if (i + j == n)
                    {
                        arr[a++] = mat[i][j];
                    }
                }
            }
        }
        n++;
    }
    return arr;
}

int main()
{
	//int n = 0;
	//int sum = 0;
	//cin >> n;

	//for (int i = 0; i <= n; i++)
	//{
	//	sum += i;
	//}
	//cout << sum;
    //int a = 1;
    //int* b = &a;
    //int** matrix = &b;
    //int matrixSize = 1;
    //int c = 1;
    //int* matrixColSize = &c;
    //int d = 0;
    //int* returnSize = &d;

    int n = 0;
    int matSize = 3;
    int matColSize[3] = { 3,3,3 };
    int mat[3][3] = { 1,2,3,4,5,6,7,8,9 };
    int returnSize = 0;

    findDiagonalOrder(mat, matSize, matColSize, &returnSize);
    //generateMatrix(n, &returnSize);
    //spiralOrder(matrix, matrixSize, matrixColSize, returnSize);
	return 0;
}