#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//排序算法
template<class T1, class T2>
void mySort(T1& arr, T2 len)
{
	for (int i = 0; i < len; i++)
	{
		int max = i;//认定最大值的下标
		for (int j = i + 1; j < len; j++)
		{
			if (arr[max] < arr[j])
			{
				max = j;
			}
		}
		T1 temp;
		*temp = arr[i];
		arr[i] = arr[max];
		arr[max] = *temp;
	}
}

void text01()
{
	//测试char数组
	//char charArr[] = "badcfe";
	int arr[] = {1,5,9,3,4,6,8};
	int len = sizeof(arr) / sizeof(arr[0]);
	//数组之间传参，传过去的是第一个元素的地址，需要额外传递数组长度
	mySort(arr, len);
	for(auto it :arr)
		cout << it << " ";
}

int main()
{
	int y = 2456;
	printf("%#x", y);
	//printf("y=|%3o| y=|%8o| y=|%#8o| y=|%08o|", y, y, y, y);

	return 0;
}

//int main()
//{
//	//text01();
//	//char a = nullptr;
//	char a = '\x008';
//	char b = '\101';
//
//	cout << a << " " << b << endl;
//
//	return 0;
//}