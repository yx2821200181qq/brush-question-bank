#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int sum(int** img, int i, int j, int imgSize)
//{
//    int count = 0;
//    int a = 0;
//    for (int m = -1; m < 2; m++)
//    {
//        if (i - m < 0 && i - m>2)
//            continue;
//        for (int n = -1; n < 2; n++)
//        {
//            if (j - n < 0 && i - m>2)
//                continue;
//            count++;
//            a += img[i - m][j - n];
//        }
//    }
//    return a / count;
//}
//
//int** imageSmoother(int** img, int imgSize, int* imgColSize, int* returnSize, int** returnColumnSizes) {
//    *returnSize = imgSize;
//    *returnColumnSizes = (int*)malloc(sizeof(int) * imgSize);
//    int** arr = (int**)malloc(sizeof(int*) * imgSize);
//
//    for (int i = 0; i < imgSize; i++)
//    {
//        (*returnColumnSizes)[i] = imgColSize[i];
//        arr[i] = (int*)malloc(sizeof(int) * imgColSize[i]);
//        for (int j = 0; i < imgSize; j++)
//        {
//            arr[i][j] = sum(img, i, j, imgSize);
//        }
//    }
//    return arr;
//}
#include<string.h>
int** imageSmoother(int** img, int imgSize, int* imgColSize, int* returnSize, int** returnColumnSizes) {
    int m = imgSize, n = imgColSize[0];
    int** ret = (int**)malloc(sizeof(int*) * m);
    *returnColumnSizes = (int*)malloc(sizeof(int) * m);
    for (int i = 0; i < m; i++) {
        ret[i] = (int*)malloc(sizeof(int) * n);
        memset(ret[i], 0, sizeof(int) * n);
        (*returnColumnSizes)[i] = n;
    }
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            int num = 0, sum = 0;
            for (int x = i - 1; x <= i + 1; x++) {
                for (int y = j - 1; y <= j + 1; y++) {
                    if (x >= 0 && x < m && y >= 0 && y < n) {
                        num++;
                        sum += img[x][y];
                    }
                }
            }
            ret[i][j] = sum / num;
        }
    }
    *returnSize = m;
    return ret;
}

int main()
{
    int img[3][3] = { 1,1,1,1,0,1,1,1,1 };
    int** returnColumnSizes = NULL;
    int imgColsize[3] = { 3,3,3 };
    int returnSize = 0;
    imageSmoother(img, 3, imgColsize, &returnSize, returnColumnSizes);

    return 0;
}