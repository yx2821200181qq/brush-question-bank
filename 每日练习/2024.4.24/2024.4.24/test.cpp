#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int Mymax(int a, int b)
{
    return (a > b) ? a : b;
}

int result(string s)
{
    // 1. 统计三种类型是否存在， 给出缺少类型的个数
    bool latterEle = false, DigitEle = false, specialEle = false;
    for (auto c : s)
    {
        if (isdigit(c)) DigitEle = true;
        else if (islower(c) || isupper(c)) latterEle = true;
        else specialEle = true;
    }
    int missingType = 0;
    if (!latterEle) missingType++;
    if (!DigitEle) missingType++;
    if (!specialEle) missingType++;

    // 2. 统计字符串长度是否满足要求，超出多少，缺少多少，最终两个结果的和就是需要添加或删除的个数
    int missingLength = Mymax(0, 8 - s.size()); // 缺少
    int extraLength = Mymax(0, s.size() - 20);  // 超出

    // 3. 计算需重复出现三次的次数
    int missingRepeat = 0;
    for (int i = 2; i < s.size(); i++)
    {
        if (s[i] == s[i - 1] && s[i - 1] == s[i - 2])
        {
            missingRepeat++;
        }
    }

    // 4. 总结需要修改的步数
    //   1. 减去重复出现
    int target = missingRepeat; // 先减去重复出现的
    if (missingLength) // 此时还缺少了重复出现的
    {
        if (missingRepeat < missingLength)
            missingLength -= missingRepeat;
        else
            missingLength = missingRepeat - missingLength;
    }
    else if (s.size() - missingRepeat < 8) // 若减去重复的字符串长度小于8
        missingLength = 0;
    if (extraLength) // 减去重复的，字符串是否还超过20
    {
        extraLength -= missingRepeat;
        if (extraLength < 0) extraLength = 0;
    }

    //   2. 增加缺少的类型
    target += missingType; // 若缺少类型必须要增加
    if (missingLength) // 增加缺少类型后是否满足字符串最低长度
    {
        missingLength -= missingType;
        if (missingLength < 0) missingLength = 0;
    }
    if (extraLength) // 增加缺少类型后还需减少几个字符
    {
        extraLength += missingType;
    }
    else if (s.size() + missingType > 20) // 若增加类型后字符串长度大于20
    {
        extraLength = s.size() + missingType - 20;
    }
    //   3. 最后加上需要增加或减少的字符
    target += missingLength + extraLength;

    return target;
}

int main()
{
    string s;
    cin >> s;

    cout << result(s) << endl;

    return 0;
}

//int minStepsToStrongPassword(string pwd) {
//    int n = pwd.length();
//    int missingTypes = 3; // 缺少的字符类型数，初始为3，表示缺少字母、数字和特殊字符
//
//    // 统计密码中包含的字母、数字和特殊字符的数量
//    bool hasDigit = false, hasLower = false, hasUpper = false, hasSpecial = false;
//    for (char c : pwd) {
//        if (isdigit(c)) hasDigit = true;
//        else if (islower(c) || isupper(c)) hasLower = true;
//        else hasSpecial = true;
//    }
//
//    // 更新缺少的字符类型数
//    if (hasDigit) missingTypes--;
//    if (hasLower) missingTypes--;
//    if (hasSpecial) missingTypes--;
//
//    // 检查密码长度是否在要求范围内
//    int missingLength = max(0, 8 - n);
//    int extraLength = max(0, n - 20);
//
//    // 计算需要删除或插入的操作次数
//    int missingRepeat = 0;
//    for (int i = 2; i < n; ++i) {
//        if (pwd[i] == pwd[i - 1] && pwd[i - 1] == pwd[i - 2]) {
//            missingRepeat++;
//            //i += 2; // 跳过连续三个重复字符
//        }
//    }
//
//    // 返回需要的最少修改步数
//    return max(missingTypes, missingLength + extraLength + missingRepeat);
//}
//
//int main() {
//    string pwd;
//    cout << "Enter password: ";
//    cin >> pwd;
//
//    cout << "Minimum steps to strong password: " << minStepsToStrongPassword(pwd) << endl;
//
//    return 0;
//}


//bool MyJudge(string s)
//{
//    if (s.size() > 15 || s.empty()) // 字符串为空或字符串长度超出限制返回false
//        return false;
//    for (int i = 0; i < s.size(); i++) // 遍历查看字符串是否符合规则
//    {
//        if (i == 0 && s[i] == '-')
//            continue;
//        if (s[i] >= '0' && s[i] <= '9')
//            continue;
//        else
//            return false;
//    }
//}
//
//string result(string& n1, string& n2)
//{
//    // 判断字符串是否符合规则
//    if (!MyJudge(n1) || !MyJudge(n2))
//        return "-1";
//
//    // 负数情况下
//    bool isRever = false;
//    if (n1[0] == '-')
//    {
//        isRever = true;
//        n1 = n1.substr(1);
//    }
//    if (n2[0] == '-')
//    {
//        isRever = true;
//        n2 = n2.substr(1);
//    }
//
//    // 相乘
//    int n = n1.length();
//    int m = n2.length();
//    string result(n + m, '0');
//    for (int i = n - 1; i >= 0; i--)
//    {
//        int flag = 0;
//        for (int j = m - 1; j >= 0; j--)
//        {
//            int mul = (n1[i] - '0') * (n2[j] - '0') + (result[i + j + 1] - '0') + flag;
//            flag = mul / 10;
//            result[i + j + 1] = mul % 10 + '0';
//        }
//        result[i] += flag;
//    }
//
//    size_t startpos = result.find_first_not_of("0");
//    if (startpos != string::npos)
//    {
//        result = result.substr(startpos);
//    }
//    else
//    {
//        result = "0";
//    }
//
//    if (isRever)
//        result = "-" + result;
//    return result;
//}
//
//int main()
//{
//    string s;
//    cin >> s;
//
//    // 将字符串拆开
//    size_t pos = s.find(',');
//    string num1 = s.substr(0, pos);
//    string num2 = s.substr(pos + 1);
//
//    cout << result(num1, num2) << endl;
//
//    return 0;
//}

//bool MyJudge(string s)
//{
//    if (s.size() > 15 || s.empty())
//        return false;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (i == 0 && s[i] == '-')
//            continue;
//        if (s[i] >= '0' && s[i] <= '9')
//            continue;
//        else
//            return false;
//    }
//    return true;
//}
//
//string multiply(string num1, string num2) {
//    // 检查输入字符串是否包含非数字字符
//    if (!MyJudge(num1) || !MyJudge(num2))
//        return "-1";
//
//    // 负数情况处理
//    bool isRever = false;
//    if (num1[0] == '-')
//    {
//        isRever = true;
//        num1 = num1.substr(1);
//    }
//    if (num2[0] == '-')
//    {
//        isRever = true;
//        num2 = num2.substr(1);
//    }
//
//    // 逐位相乘
//    int n = num1.length(), m = num2.length();
//    string result(n + m, '0');
//    for (int i = n - 1; i >= 0; --i) {
//        int carry = 0;
//        for (int j = m - 1; j >= 0; --j) {
//            int mul = (num1[i] - '0') * (num2[j] - '0') + (result[i + j + 1] - '0') + carry;
//            carry = mul / 10;
//            result[i + j + 1] = (mul % 10) + '0';
//        }
//        result[i] += carry;
//    }
//
//    // 移除结果开头的多余的 '0'
//    size_t startpos = result.find_first_not_of("0");
//    if (startpos != string::npos) {
//        result = result.substr(startpos);
//    }
//    else {
//        result = "0";
//    }
//
//    // 添加负号
//    if (isRever) result = "-" + result;
//
//    return result;
//}
//
//int main() {
//    string input;
//    cout << "Enter two numbers separated by comma: ";
//    cin >> input;
//
//    // 解析输入字符串
//    size_t pos = input.find(',');
//    string num1 = input.substr(0, pos);
//    string num2 = input.substr(pos + 1);
//
//    // 调用乘法函数并输出结果
//    cout << "Product: " << multiply(num1, num2) << endl;
//
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <string>
//
//using namespace std;
//
//int minCut(string s)
//{
//    int n = s.size();
//    vector<vector<bool>> dp(n, vector<bool>(n, false));
//    vector<int> cut(n);
//
//    // 初始化 dp 数组和 cut 数组
//    for (int i = 0; i < n; ++i)
//    {
//        cut[i] = i;
//        for (int j = 0; j <= i; j++)
//        {
//            if (s[i] == s[j] && (i - j <= 1 || dp[j + 1][i - 1]))
//            {
//                dp[j][i] = true;
//                if (j == 0)
//                    cut[i] = 0;
//                else
//                    cut[i] = min(cut[i], cut[j - 1] + 1);
//            }
//        }
//    }
//
//    return cut[n - 1];
//}
//
//int main() {
//    string s;
//    cin >> s;
//    cout << minCut(s) << endl;  // 输出为 0
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int count1 = 0;
//
//int foo(int n)
//{
//	count1++;
//	if (n <= 3)
//		return 1;
//	else
//		return foo(n - 3) + foo(n - 5) + 1;
//}
//
//int foo(int n)
//{
//	if (n <= 1)
//		return 1;
//	else
//		return n * foo(n - 5);
//}
//
//
//
//A = 1, 0, 2
//    0, 2, 0
//	4, 0, 6
//
//int main()
//{
//	foo(foo(9));
//	cout << count1 << endl;
//
//	return 0;
//}