#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int compare_int(const void* a1, const void* a2)
{
    return (*(int*)a1) > (*(int*)a2);
}

int firstMissingPositive(int* nums, int numsSize) {
    qsort(nums, numsSize, 4, compare_int);
    int a = 1;
    int count = 0;
    if (nums[numsSize - 1] <= numsSize)
    {
        count = numsSize;
    }
    else
    {
        count = nums[numsSize - 1];
    }
    for (int i = 0; i < count; i++)
    {
        if (nums[i] == a)
        {
            a++;
        }
    }
    return a;
}

int sameArr(int* nums, int numsSize)
{
    for (int i = 0; i < numsSize - 1; i++)
    {
        if (nums[i] != nums[i + 1])
        {
            return 1;
        }
    }
    return 0;
}
//int compare_int(const void* a1, const void* a2)
//{
//    return (*(int*)a1) > (*(int*)a2);
//}

int minMoves(int* nums, int numsSize) {
    int count = 0;
    while (sameArr(nums, numsSize))
    {
        qsort(nums, numsSize, 4, compare_int);
        int a = nums[numsSize - 1] - nums[0];
        for (int i = 0; i < numsSize - 1; i++)
        {
            nums[i] += a;
        }
        count += a;
    }
    return count;
}

//int checkPossibility(int* nums, int numsSize) {
//    int count = 0;
//    int count1 = 0;
//    for (int i = 1; i < numsSize; i++)
//    {
//        if (nums[i - 1] > nums[i])
//        {
//            if (nums[i - 1] > nums[i + 1] && i != numsSize - 1 && numsSize != 3)
//            {
//                return 0;
//            }
//            count++;
//        }
//        if (count == 2)
//        {
//            return 0;
//        }
//    }
//
//    return 1;
//}
#include<stdbool.h>
bool checkPossibility(int* nums, int numsSize) {
    int count = 0;
    for (int i = 1; i < numsSize; i++)
    {
        if (nums[i - 1] > nums[i])
        {
            if (i != numsSize - 1)
            {
                if (nums[i - 1] > nums[i + 1] && i != 1)
                {
                    return false;
                }
            }
            count++;
        }
        if (count == 2)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    int nums[] = { -1,4,2,3 };
    //printf("%d", nums);
    int numsSize = 4;

    printf("%d", checkPossibility(nums, numsSize));
	return 0;
 }