#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//void check(char key[][255], char name[][255], int valid[])
//{
//	for (int i = 0; i < 5; i++)
//	{
//		int flag = 1;
//		for (int j = 0; j < 10; j++)
//		{
//			if (strcmp(key[j], name[i]) == 0)
//			{
//				valid[i] = 1;
//				flag = 0;
//				break;
//			}
//		}
//		if (flag)
//			valid[i] = 0;
//	}
//}
//
//void print(char name[][255], int valid[])
//{
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%s\t%s\n", name[i], valid[i] == 0 ? "不是" : "是");
//	}
//}
//
//int main()
//{
//	char key[10][255] = { {"break"},{"char"},{"while"},{"int"},{"for"},{"return"},{"long"},{"continue"},{"if"},{"switch"} };
//	char id[5][255] = { {"while"},{"FOR"},{"switch"},{"Int"},{"main"} };
//	int valid[5], i;
//	check(key, id, valid);
//	print(id, valid);
//	for (i = 0; i < 5; i++)
//		printf("%s\t%s\n", id[i], valid[i] == 0 ? "不是" : "是");
//
//	return 0;
//}

int convert(char b[], int d[])
{
	int num = 0;
	int cur = 0;
	int sym = 0;
	int len = strlen(b);
	for (int i = 0; i < len; i++)
	{
		if (b[i] == '.')
		{
			if (num != 0)
			{
				d[cur] = sym*num;
				cur++;
			}
			num = 0;
		}
		else
		{
			if (i == 0 || (i >= 1 && b[i - 1] == '.'))
			{
				if (b[i] == '1')
					sym = -1;
				else
					sym = 1;
				continue;
			}
			num = num * 2 + b[i]-'0';
		}
	}
	if (b[len - 1] != '.')
	{
		d[cur] = num;
		cur++;
	}
	return cur;
}

void sort(int d[], int n)
{
	//冒泡排序
	for (int i = 0; i < n; i++)
	{
		int flag = 1;
		for (int j = 0; j < n - i-1; j++)
		{
			if (d[j] > d[j + 1])
			{
				int temp = d[j];
				d[j] = d[j + 1];
				d[j + 1] = temp;
				flag = 0;
			}
		}
		if (flag)
			break;
	}
}

int main()
{
	char str_b[100] = "...111100.01111.01100111...0111..110000.011..";
	int int_d[10];
	int i, k;
	k = convert(str_b, int_d);
	for (i = 0; i < k; i++)
		printf("%d\t", int_d[i]);
	printf("\n");
	sort(int_d, k);
	for (i = 0; i < k; i++)
		printf("%d\t", int_d[i]);
	printf("\n");
	return 0;
}