#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int* constructArray(int n, int k, int* returnSize) {
    *returnSize = n;
    int* arr = (int*)malloc(sizeof(int) * n);
    arr[0] = 1;
    int a = 0;

    int* arr1 = (int*)malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++)
    {
        arr1[i] = i + 1;
    }

    for (int i = 1; i < n; i++)
    {
        if (k - a > 0)
        {
            arr[i] = arr[i - 1] + k - a++;
            if (arr[i] > n || arr1[arr[i] - 1] == -1)
            {
                arr[i] = arr[i - 1] - k + a-1;
            }
            arr1[arr[i] - 1] = -1;
        }
        else
        {
            for (int j = 1; j < n; j++)
            {
                if (arr1[j] != -1)
                {
                    arr[i++] = arr1[j];
                }
            }
            break;
        }
    }
    return arr;
}

int main()
{
    int returnSize = 0;
    int n = 6;
    int k = 3;
    constructArray(n, k, &returnSize);
	return 0;
}