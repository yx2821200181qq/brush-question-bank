#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void Enter(char* s, int cur, char* arr, int* local)
{
    int len = strlen(s);
    while (cur - 1 >= 0 && s[cur - 1] != ' ')
    {
        cur--;
    }
    while (cur < len && s[cur] != ' ')
    {
        arr[(*local)++] = s[cur++];
    }
    arr[--(*local)] = ' ';
    (*local)++;
}

char* sortSentence(char* s) {
    int num = 1;
    int len = strlen(s);
    char* arr = (char*)malloc(sizeof(char) * (len + 1));
    int cur = len - 1;
    int local = 0;
    while (cur > 0)
    {
        if (s[cur] == num + '0')
        {
            Enter(s, cur, arr, &local);
            num++;
            cur = len;
        }
        cur--;
    }
    arr[--local] = '\0';
    return arr;
}

int Int_cmp(const void* a1, const void* a2)
{
    return *(int*)a1 > *(int*)a2;
}

//int findLHS(int* nums, int numsSize) {
//    int start = numsSize - 1, end = numsSize - 1;
//    int length = 0;
//    qsort(nums, numsSize, sizeof(int), Int_cmp);
//    int flag = nums[start];
//    while (start > 0 && nums[start] == flag)
//        start--;
//    flag = nums[start];
//    while (start > 0 && nums[start - 1] == flag)
//        start--;
//    while (start != end || start != 0)
//    {
//        if (end != start && nums[end] - nums[start] == 1 && length < end - start+1)
//            length = end - start + 1;
//
//        flag = nums[end];
//        while (end > 0 && flag == nums[end])
//            end--;
//        if (start != 0)
//            start--;
//        flag = nums[start];
//        while (start > 0 && nums[start-1] == flag)
//            start--;
//    }
//
//    return length;
//}

int findLHS(int* nums, int numsSize) {
    qsort(nums, numsSize, sizeof(int), Int_cmp);
    int arr[20000][2];
    int flag = nums[0], count = 1;
    arr[0][0] = 0;
    for (int i = 0; i < numsSize; i++)
    {
        if (nums[i] != flag)
        {
            arr[count][0] = i;
            if (count - 1 == 0)
            {
                arr[count - 1][1] = i;
            }
            else
            {
                arr[count - 1][1] = i - arr[count - 1][0];
            }
            flag = nums[i];
            count++;
        }
    }
    if (count > 0)
    {
        arr[--count][1] = numsSize - 1 - arr[count][0]+1;
    }
    else
        return 0;
    int length = 0;
    for (int i = 0; i + 1 <= count; i++)
    {
        if (nums[arr[i + 1][0]] - nums[arr[i][0]] == 1)
        {
            if (arr[i + 1][1] + arr[i][1] > length)
                length = arr[i + 1][1] + arr[i][1];
        }
    }
    return length;
}

int main()
{
    //char s[] = "Myself2 Me1 I4 and3";
    //printf("%s", sortSentence(s));

    int nums[] = { 1,2,2,3,4,5,1,1,1,1 };

    findLHS(nums, sizeof(nums) / sizeof(nums[0]));

    return 0;
}