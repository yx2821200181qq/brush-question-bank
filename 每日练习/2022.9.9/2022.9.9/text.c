#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

//int minOperations(char** logs, int logsSize) {
//    int count = 0;
//    for (int i = 0; i < logsSize; i++)
//    {
//        if (logs[i] != "../" && logs[i] != "./")
//        {
//            count++;
//        }
//        else if (logs[i] == "../" && count > 0)
//        {
//            count--;
//        }
//    }
//    return count;
//}

int minOperations(char** logs, int logsSize) {
    int count = 0;
    for (int i = 0; i < logsSize; i++)
    {
        if (strcmp(logs[i], '../') != 0 && strcmp(logs[i], './') != 0)
        {
            printf("%s", logs[i]);
            count++;
        }
        else if (strcmp(logs[i], '../') == 0 && count > 0)
        {
            count--;
        }
    }
    return count;
}

int main()
{
    char* logs[] = { "d1/","d2/","../","d21/","./" };
    int a = 5;
    minOperations(logs, a);
	return 0;
}