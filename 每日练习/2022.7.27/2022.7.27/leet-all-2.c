#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

struct ListNode {
	int val;
    struct ListNode* next;
};

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) 
{
	int a1 = 0;
	int a2 = 0;

	int flag = 0;
	while (l1->next != NULL)
	{
		a1 += l1->val * pow(10, flag);
		l1 = l1->next;
		flag++;
	}

	while (l2->next != NULL)
	{
		a2 += l2->val * pow(10, flag);
		l2 = l2->next;
		flag++;
	}

	int a3 = a1 + a2;

	struct ListNode* l3;
	
	while (a3 / 10 != 0)
	{
		l3->val = a3 % 10;
		a3 = a3 / 10;
		l3 = l3->next;
	}
}

int main()
{

	return 0;
}