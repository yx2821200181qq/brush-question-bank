#include <iostream>
using namespace std;

class C
{
public:
    C()
    {
        cout << "C Construct" << endl;
    }
    ~C()
    {
        cout << "C Deconstruct" << endl;
    }
};

class Base
{
public:
    Base(int a)
    {
        cout << "Base Construct\n";
    }
    ~Base()
    {
        cout << "Base Destruct\n";
    }
};

class Derived : public Base
{
private:
    int a;
    C c;
public:
    Derived()
        :Base(1)
        ,a(2)
    {
        cout << "Derived Construct\n";
    }

    ~Derived()
    {
        cout << "Derived Destruct\n";
    }
};

int main()
{
    Derived d;

    return 0;
}

class Solution {
    size_t length = 35;
private:
    void GetPositive(int n, string& s)
    {
        for (int i = 0; i < 32; i++)
        {
            if ((n >> i) & 1 == 1)
            {
                s[length - i - 1] = '1';
            }
        }
    }
    string GetNegative(string& sp, string& sn)
    {
        int flag = length - 1;
        int index = 0;
        for (int i = flag; i >= 0; i--)
        {
            if (sp[i] == '1')
            {
                if (sn[i] == '0' && (flag - i) % 2 == 0) // 二进制下标为双（正数），若负二进制该位置为0，直接修改
                {
                    sn[i] = '1';
                }
                else if (sn[i] == '0' && (flag - i) % 2 == 1) // 二进制下标为单（负数），若负二进制该位置为0，修改该位置和下一个位置
                {
                    sn[i] = '1';
                    sn[i - 1] = '1';
                }
                else if (sn[i] == '1' && (flag - i) % 2 == 0) // 若该位置已经被占，并且是正数，修改该位置及接下来两个位置，保证最终所得数一致
                {
                    sn[i] = '0';
                    sn[i - 1] = '1';
                    sn[i - 2] = '1';
                }
                else // 若该位置已经被占，并且是负数，表示该位置及接下来的位置已被修改（上一条），此时修改该位置即可（此位置表示负数，假设为8，那该位置此时表示-8，下一个位置为16，删除此位置曾直接加8）
                {
                    sn[i] = '0';
                }
            }
        }
        for (int i = 0; i < length; i++)
        {
            if (sn[i] != '0')
            {
                index = i;
                break;
            }
        }
        return sn.substr(index);
    }
public:
    string baseNeg2(int n) {
        if (n == 0) return string("0");
        string spositive(length, '0');
        string snegative(length, '0');
        GetPositive(n, spositive);
        string starget = GetNegative(spositive, snegative);
        return starget;
    }
};

//int main()
//{
//    Solution s;
//    s.baseNeg2(108);
//
//    return 0;
//}