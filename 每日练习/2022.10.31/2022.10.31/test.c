#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int removeDuplicates(int* nums, int numsSize) {
    int head = 0, trail = 0;
    while (trail < numsSize)
    {
        if (nums[head] == nums[trail])
        {
            trail++;
        }
        else
        {
            nums[++head] = nums[trail++];
        }
    }
    return head + 1;
}

void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
    int end1 = m - 1, end2 = n - 1;
    int j = nums1Size - 1;
    while (end1 >= 0 && end2 >= 0)
    {
        if (nums1[end1] > nums2[end2])
        {
            nums1[j--] = nums1[end1--];
        }
        else
        {
            nums1[j--] = nums2[end2--];
        }
    }
    while (end2>=0)
    {
        nums1[j--] = nums2[end2--];
    }
}

struct ListNode* reverseList(struct ListNode* head) {
    int size = 1;
    int* arr = (int*)malloc(sizeof(int) * size);
    int i = 0;
    struct ListNode* rear = head;
    while (rear)
    {
        arr[i++] = rear->val;
        arr = (int*)realloc(arr, (size + 1) * sizeof(int));
        size = size + 1;
        rear = rear->next;
    }
    rear = head;
    while (rear)
    {
        rear->val = arr[--i];
        rear = rear->next;
    }
    return head;
}

int main()
{
    int nums1[6] = { 1,2,3,0,0,0 };
    int nums2[3] = {2,5,6};
    merge(nums1, 6, 3, nums2, 3, 3);
	return 0;
}