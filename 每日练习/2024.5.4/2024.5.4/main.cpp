#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution1 {
private:
    bool Judge(char c, int a)
    {
        vector<char> letter = { 'a', 'e','i', 'o', 'u' };
        for (auto it : letter)
        {
            if (it == c + a) return true;
        }
        return false;
    }
public:
    bool isValid(string word) {
        if (word.size() < 3) return false;
        vector<int> flag(3, 0);
        for (auto it : word)
        {
            if (it >= '0' && it <= '9') flag[0] = 1;
            else if (it >= 'A' && it <= 'Z')
            {
                if (Judge(it, 'a' - 'A')) flag[1] = 1;
                else flag[2] = 1;
            }
            else if (it >= 'a' && it <= 'z')
            {
                if (Judge(it, 0)) flag[1] = 1;
                else flag[2] = 1;
            }
            else return false;
        }
        for (int i = 1; i < 3; i++)
        {
            if (flag[i] != 1) return false;
        }
        return true;
    }
};

class S {
    void Work(string& s, vector<int>& v, int index, int n)
    {
        for (int i = index; i < n; i++)
        {
            v[s[i] - 'a' + 1]++;
        }
    }
public:
    int minAnagramLength(string s) {
        vector<vector<int>> v(1000, vector<int>(27, 0));
        int n = 0;
        for (int i = 1; i <= s.size() / 2; i++)
        {
            if (s.size() % i != 0) continue;
            n = 0;
            for (int j = 0; j < s.size(); j += i)
            {
                Work(s, v[n], j, i + j);
                n++;
            }
            bool flag = true;
            for (int j = 0; j < n - 1; j++)
            {
                for (int h = 1; h < 27; h++)
                {
                    if (v[j][h] != v[j + 1][h])
                    {
                        flag = false;
                        break;
                    }
                }
                if (!flag) break;
            }
            if (flag) return i;
            for (int j = 0; j < n; j++)
            {
                for (int h = 1; h < 27; h++)
                {
                    v[j][h] = 0;
                }
            }
        }
        return s.size();
    }
};

class Solution2 {
    void Work(string& s, vector<int>& v, int index, int n)
    {
        for (int i = index; i < n; i++)
        {
            v[s[i] - 'a' + 1]++;
        }
    }
public:
    int minAnagramLength(string s) {
        int n = 0;
        for (int i = 1; i <= s.size() / 2; i++)
        {
            if (s.size() % i != 0) continue;
            n = 0;
            vector<vector<int>> v;
            for (int j = 0; j < s.size(); j += i)
            {
                vector<int> temp(27, 0);
                Work(s, temp, j, i + j);
                n++;
                v.push_back(temp);
            }
            bool flag = true;
            for (int j = 0; j < n - 1; j++)
            {
                for (int h = 1; h < 27; h++)
                {
                    if (v[j][h] != v[j + 1][h])
                    {
                        flag = false;
                        break;
                    }
                }
                if (!flag) break;
            }
            if (flag) return i;
        }
        return s.size();
    }
};

class Solution {
    int Work(string& s, string& str, int k)
    {

        int n = 0;
        for (int i = 0; i < s.size(); i += k)
        {
            string temp(s.begin() + i, s.begin() + i + k);
            if (str.compare(temp) == 0) n++;
        }
        return n;
    }
    bool Judge(string& s, vector<string>& v)
    {
        for (auto& it : v)
        {
            if (it.compare(s) == 0) return false;
        }
        return true;
    }
public:
    int minimumOperationsToMakeKPeriodic(string word, int k) {
        int max = INT_MIN;
        vector<string> vs;
        for (int i = 0; i < word.size(); i += k)
        {
            string str(word.begin() + i, word.begin() + i + k);
            if (Judge(str, vs)) vs.push_back(str);
            else continue;
            int n = Work(word, str, k);
            max = max < n ? n : max;
        }
        return word.size() / k - max;
    }
};

int minimumOperationsToMakeKPeriodic(string word, int k) {
    int Max = INT_MIN;
    unordered_map<string, int> mp;
    for (int i = 0; i < word.size(); i += k)
    {
        string temp = word.substr(i, k);
        mp[temp]++;
    }
    for (auto& it : mp)
    {
        Max = max(Max, it.second);
    }
    return word.size() / k - Max;
}

int main()
{
    Solution1 s;
    cout << s.isValid("234Adas") << endl;

	return 0;
}