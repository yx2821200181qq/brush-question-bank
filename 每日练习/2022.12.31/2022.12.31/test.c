#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main()
{
    char* argv = "longlonglongistoolong";
    int argc = strlen(argv);
    // 请在此输入您的代码
    int arr[26] = { 0 };
    for (int i = 0; i < argc; i++)
    {
        int cur = argv[i] - 'a';
        arr[cur]++;
    }
    int max = 0;
    char ch;
    for (int i = 0; i < 26; i++)
    {
        if (max < arr[i])
        {
            max = arr[i];
            ch = i + 'a';
        }
    }
    printf("%c\n", ch);
    printf("%d", max);
    return 0;
}

//int main() {
//    char str;
//    int arr[4] = { 0 };
//    while (scanf("%c", &str) != EOF)
//    {
//        if (str == '\0')
//            break;
//        if ((str >= 'a' && str <= 'z') || (str >= 'A' && str <= 'Z'))
//            arr[0]++;
//        else if (str == ' ')
//            arr[1]++;
//        else if (str >= '0' && str <= '9')
//            arr[2]++;
//        else
//            arr[3]++;
//    }
//    printf("%d\n%d\n%d\n%d", arr[0], arr[1], arr[2], arr[3]);
//
//    return 0;
//}