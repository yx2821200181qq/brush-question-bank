#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int maxLengthBetweenEqualCharacters(char* s) {
//    int a = strlen(s);
//    char* end = &s[a - 1];
//    char* start = s;
//    int max = -1;
//    for (int i = 0; i < a - 1; i++)
//    {
//        while (start < end)
//        {
//            if (*start == *end)
//            {
//                if (max < end - start - 1)
//                    max = end - start - 1;
//                break;
//            }
//            end--;
//        }
//        start++;
//        end = &s[a - 1];
//    }
//    printf("%d", max);
//    return 0;
//}

////匿名结构体类型
//struct
//{
//    int a;
//    char b;
//    float c;
//}x;
//
//struct
//{
//    int a;
//    char b;
//    float c;
//}a[20],*p;

//typedef struct Node Nd;
//
//typedef struct Node
//{
//    int data;
//    Nd* next;
//}Node;
////这样写是否可以？
//
////struct student
////{
////    char name[20];//姓名
////    int age;//年龄
////    char sex[5];//性别
////}Stu = { "赵六",19,"男" };
//
//struct student
//{
//    char* name;//姓名
//    int age;//年龄
//    char* sex;//性别
//}Stu;
//
//int main()
//{
//    //maxLengthBetweenEqualCharacters("scayofdzca");
//    /*int arr[10] = { -1 };*/
//
//    //p = &x;
//    //char arr[10];
//    //printf("%s\n", strcpy(arr, "hello bit"));
//    //Nd b = { 1,NULL };
//    //Nd a = { 12,&b };
//    //while (a.data != NULL)
//    //{
//    //    printf("%d ", a.data);
//    //    a = *(a.next);
//    //}
//
//    struct student s1 = { .age = 18,.name = "李四",.sex = "女" };
//    printf("%d %s %s\n", s1.age, s1.name, s1.sex);
//
//    struct student s;
//    s.age = 10;
//    s.name = "张三";
//    s.sex = "男";
//    printf("%d %s %s\n", s.age, s.name, s.sex);
//
//	return 0;
//}
#include<stddef.h>
//struct text
//{
//	int a;
//	char b;
//}text;

//struct S1
//{
//	char a;
//	int b;
//	char c;
//};

//struct S3
//{
//	double d;
//	char c;
//	int i;
//};
//
//struct S2
//{
//	char c1;
//	char c2;
//	int i;
//};
//
//struct S4
//{
//	char c1;
//	struct S3 s3;
//	double d;
//};
//
//int main()
//{
//	struct S2 s2;
//	//printf("%d\n", offsetof(struct S1, a));
//	//printf("%d\n", offsetof(struct S1, b));
//	//printf("%d\n", offsetof(struct S1, c));
//
//	//printf("%d\n", sizeof(struct S1));
//
//	//printf("%d\n", sizeof(struct S4));
//
//	return 0;
//}

//struct text
//{
//	char c;
//	int i;
//};

//#include <stdio.h>
//#pragma pack(8)//设置默认对齐数为8
//struct S1
//{
//    char c1;
//    int i;
//    char c2;
//};
//#pragma pack()//取消设置的默认对齐数，还原为默认
//#pragma pack(1)//设置默认对齐数为1
//struct S2
//{
//    char c1;
//    int i;
//    char c2;
//};
//#pragma pack()//取消设置的默认对齐数，还原为默认
//int main()
//{
//    //输出的结果是什么？
//    printf("%d\n", sizeof(struct S1));
//    printf("%d\n", sizeof(struct S2));
//    return 0;
//}

// int lengthOfLastWord(char * s){
//     int a = strlen(s) - 1;
//     //char* end = s+a-1;
//     int count = 0;
//     while(s[a] == ' ')
//         a--;
//     while(a>=0 && s[a]!=' ')
//     {
//         a--;
//         count++;
//     }
//     return count;
// }

int lengthOfLastWord(char* s) {
    int a = strlen(s) - 1;
    char* end = s + a;
    int count = 0;
    while (*end == ' ')
    {
        end--;
        a--;
    }
    while (a != 0 && *end != ' ')
    {
        a--;
        end--;
        count++;
    }
    if (a == 0 && *end != ' ')
        count++;
    return count;
}