#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

//int f(int n)
//{
//	if (n == 1)
//		return 1;
//	if (n == 2)
//		return 2;
//	return f(n - 1) + f(n - 2);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	printf("%d", f(n));
//
//	return 0;
//}

int ways[1 << 15][16] = { 0 }, cnt = 0;

void dfs(int n, int* arr, int i)
{
    if (i == n)
    {
        //for (int j = 0; j < n; j++)
        //{
        //    if (arr[j] == 1)
        //        printf("%d ", j+1);
        //}
        //printf("\n");
        for (int j = 1; j <= n; j++)
        {
            ways[cnt][j] = arr[j - 1];
        }
        cnt++;
        return;
    }
    arr[i] = 1;
    dfs(n, arr, ++i);
    arr[--i] = 0;
    dfs(n, arr, ++i);
}

int main()
{
    int n = 0;
    scanf("%d", &n);
    int* arr = (int*)calloc(n, sizeof(int));
    int i = 0;
    dfs(n, arr, i);
    for (i = 0; i < cnt; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (ways[i][j] == 1)
                printf("%d ", j);
        }
        printf("\n");
    }

    return 0;
}