#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdbool.h>

char* RemoveStr(char* target)
{
    char* str = (char*)malloc(sizeof(char) * 200);
    int cur = 0;
    while (*target)
    {
        if (*target != '#')
        {
            str[cur] = *target;
            cur++;
        }
        else if(cur > 0)
        {
            cur--;
        }
        target++;
    }
    str[cur] = '\0';
    return str;
}

//int backspaceCompare(char* s, char* t) {
//    s = RemoveStr(s);
//    t = RemoveStr(t);
//
//    //while (*s && *t)
//    //{
//    //    if (*s != *t)
//    //        break;
//    //    s++;
//    //    t++;
//    //}
//    //return *s - *t == 0;
//    return strcmp(s, t, 0) == 0;
//}


bool backspaceCompare(char* s, char* t) {
    int slen = strlen(s) - 1;
    int tlen = strlen(t) - 1;
    int sflag = 0, tflag = 0;
    while (slen >= 0 && tlen >= 0)
    {
        while (slen >= 0)
        {
            if (s[slen] == '#')
            {
                slen--;
                sflag++;
            }
            else if (sflag > 0)
            {
                slen--;
                sflag--;
            }
            else
                break;
        }
        while (tlen >= 0)
        {
            if (t[tlen] == '#')
            {
                tlen--;
                tflag++;
            }
            else if (tflag > 0)
            {
                tlen--;
                tflag--;
            }
            else
                break;
        }
        if (tlen < 0 || slen < 0)
            break;
        if (t[tlen] != s[slen])
            break;
        tlen--;
        slen--;
    }
    if (slen < 0 && tlen < 0)
    {
        return true;
    }
    return false;
}

int main()
{
    char* s = "ab##";
    char* t = "c#d#";

    printf("%d", backspaceCompare(s, t));
    return 0;
}

//int main()
//{
//    int arr1[] = { 1,2,3,4,5 };
//    int arr2[] = { 1,2,3,0,0 };
//    int ret = memcmp(arr1, arr2, 13);
//    printf("%d", ret);
//
//    return 0;
//}