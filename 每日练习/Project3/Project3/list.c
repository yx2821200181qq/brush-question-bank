#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#define ERROR 0
typedef char ElemType;
//单链表节点定义
typedef struct Node {
	ElemType data;
	Node* next;
}Node, * LinkList;

//初始化单链表
void InitList(LinkList* L) {
	L = (LinkList)malloc(sizeof(Node));
	(*L)->next = NULL;
}

//头插法
void HeadInsert(LinkList* L) {
	Node* s;
	char x;
	InitList(L);
	x = getchar();

	while (x != '$') {
		s = (LinkList)malloc(sizeof(Node));
		s->data = x;
		s->next = (*L)->next;
		(*L)->next = s;
		x = getchar();
	}
}

//尾插法
void TailInsert(LinkList* L) {
	char x;
	InitList(L);
	Node* s, * r = L;
	x = getchar();
	while (x != '$') {
		s = (LinkList)malloc(sizeof(Node));
		s->data = x;
		r->next = s;
		r = s;
		x = getchar();
	}
	r->next = NULL;

}

//遍历单链表
void PrintList(LinkList* L) {
	Node* s;

	s = (*L)->next;

	while (s != NULL) {
		printf("%c", s->data);
		s = s->next;
	}
	printf("\n");
}

int main() {
	LinkList L;

	printf("请输入需要存放的字符:(头插法)");
	HeadInsert(L);
	PrintList(L);
	printf("请输入需要存放的字符:(尾插法)");
	TailInsert(L);
	PrintList(L);
}