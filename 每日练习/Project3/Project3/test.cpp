#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <functional>

class NativeWindow {
public:
    typedef std::function<void(int)> KeyFunc;
    void set_key_callback(KeyFunc func) {
        m_key_callback = func;
    }
    void run() {
        if (m_key_callback) {
            m_key_callback(42);
        }
    }
private:
    KeyFunc m_key_callback;
};

class MyWindow {
public:
    MyWindow() {
        m_window = new NativeWindow();
        m_window->set_key_callback([this](int key) {
            this->on_key(key);
            });
        // OK: capturing lambda to std::function

        m_window->set_key_callback(std::bind(&MyWindow::on_key, this, std::placeholders::_1));
        // OK: works, but not as clear as capturing lambda
    }
    ~MyWindow() {
        delete m_window;
    }
    void run() {
        m_window->run();
    }
    void on_key(int key) {
        std::cout << "key: " << key << "\n";
    }

private:
    NativeWindow* m_window;
};


int main() {
    MyWindow my_window;
    my_window.run();
    return 0;
}