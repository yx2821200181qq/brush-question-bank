#include <iostream>
#include <vector>
using namespace std;

// 原地删除有序数组中的重复项
int removeDuplicates(vector<int>& nums) {
    int num = 0, left = 0, right = 0;
    int flag = INT_MIN;
    int n = nums.size();
    while (right < n)
    {
        if (flag != nums[right])
        {
            flag = nums[right];
            num = 1;
            nums[left++] = nums[right++];
            continue;
        }
        if (++num > 2)
        {
            right++;
            continue;
        }
        nums[left++] = nums[right++];
    }
    return left;
}

// 买股票的最佳时机 I
int maxProfit(vector<int>& prices) {
    int n = prices.size();
    vector<vector<int>> f(n, vector<int>(2, 0));
    f[0][0] = 0 - prices[0];
    f[0][1] = 0;
    for (int i = 1; i < n; i++)
    {
        f[i][0] = max(f[i - 1][0], 0 - prices[i]);
        f[i][1] = max(f[i - 1][1], prices[i] + f[i - 1][0]);
    }
    return f[n - 1][1];
}

// 买股票的最佳时机 Ⅱ
int maxProfit(vector<int>& prices) {
    int n = prices.size();
    vector<vector<int>> f(n, vector<int>(2, 0));
    f[0][0] = 0 - prices[0];
    f[0][1] = 0;
    for (int i = 1; i < n; i++)
    {
        f[i][0] = max(f[i - 1][0], f[i - 1][1] - prices[i]);
        f[i][1] = max(f[i - 1][1], prices[i] + f[i - 1][0]);
    }
    return f[n - 1][1];
}

int main()
{
    vector<int> prices = { 7,1,5,3,6,4 };
    maxProfit(prices);

    return 0;
}