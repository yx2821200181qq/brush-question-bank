#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int numberOfEmployeesWhoMetTarget(vector<int>& hours, int target) {
        int n = 0;
        for (auto it : hours)
        {
            if (it >= target) n++;
        }
        return n;
    }
};

int main()
{

	return 0;
}