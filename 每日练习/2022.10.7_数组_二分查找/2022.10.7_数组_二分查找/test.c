#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int searchInsert(int* nums, int numsSize, int target) {
//    for (int i = 0; i <= numsSize - 1 - i; i++)
//    {
//        if (nums[i] == target)
//        {
//            return i;
//        }
//        if (nums[numsSize - i - 1] == target)
//        {
//            return numsSize - i - 1;
//        }
//        if (nums[i] > target)
//        {
//            return i;
//        }
//        if (nums[i] <target && nums[i + 1] > target)
//        {
//            return i + 1;
//        }
//        if (nums[numsSize - i - 1] < target)
//        {
//            return numsSize - i;
//        }
//    }
//    return -1;
//}

int* searchRange(int* nums, int numsSize, int target, int* returnSize) {
    int left = 0, right = numsSize - 1;
    *returnSize = 2;
    int* arr = (int*)malloc(2 * sizeof(int));
    while (left <= right)
    {
        int center = left + ((right - left) >> 1);
        if (nums[center] == target)
        {
            int left1 = center - 1;
            int right1 = center + 1;
            while (left1 >= 0 && nums[left1] == target)
            {
                left1--;
            }
            while (right1 <= numsSize - 1 && nums[right1] == target)
            {
                right1++;
            }
            arr[0] = left1 + 1;
            arr[1] = right1 - 1;
            return arr;
        }
        else if (nums[center] > target)
        {
            right = center - 1;
        }
        else
        {
            left = center + 1;
        }
    }
    arr[0] = -1;
    arr[1] = -1;
    return arr;
}

int main()
{
    int nums[] = { 2,2 };
    int returnSize[2] = { 0 };
    searchRange(nums, sizeof(nums)/sizeof(nums[0]), 2, returnSize);

	return 0;
}