#include <iostream>
#include <vector>
#include <queue>
using namespace std;

class Solution {
    typedef pair<int, int> pii;
private:
    template<class T>
    struct mycompare
    {
        bool operator()(T& p1, T& p2)
        {
            if (p1.first > p2.first) return true;
            if (p1.first < p2.first) return false;
            if (p1.second > p2.second) return true;
            return false;
        }
    };
    
public:
    long long totalCost(vector<int>& costs, int k, int candidates) {
        priority_queue<pii, vector<pii>, greater<pii>> pq;
        long long num = 0;
        int left = candidates - 1, right = costs.size() - candidates;
        for (int i = 0; i <= left && i < costs.size(); i++)
        {
            pq.push(make_pair(costs[i], i));
        }
        for (int i = costs.size() - 1; i >= right && i > left; i--)
        {
            pq.push(make_pair(costs[i], i));
        }
        for (int i = 0; i < k; i++)
        {
            pii temp = pq.top();
            pq.pop();
            num += temp.first;
            if (temp.second <= left && left + 1 < right)
            {
                pq.push(make_pair(costs[++left], left));
            }
            if (temp.second >= right && left < right - 1)
            {
                pq.push(make_pair(costs[--right], right));
            }
        }
        return num;
    }
};

void test(vector<int> v)
{
    int num = 0;
    sort(v.begin(), v.end(), less<int>());
    for (int i = 0; i < 32; i++)
    {
        num += v[i];
    }
    cout << num << endl;
}

int main()
{
    vector<int> v = { 28,35,21,13,21,72,35,52,74,92,25,65,77,1,73,32,43,68,8,100,84,80,14,88,42,53,98,69,64,40,60,23,99,83,5,21,76,34 };
    Solution s;
    cout << s.totalCost(v, 32, 12) << endl;
    test(v);

	return 0;
}