#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int ReWord(int* arr)
{
    for (int i = 0; i < 4; i++)
    {
        if (arr[i] != arr[8 - 1 - i])
        {
            return 0;
        }
    }
    return 1;
}

int AB(int* arr)
{
    if (arr[0] != arr[1] && arr[0] == arr[2] && arr[5] == arr[7] && arr[0] == arr[5] && arr[1] == arr[3] && arr[1] == arr[4] && arr[4] == arr[6])
    {
        return 1;
    }
    return 0;
}

int main(int argc, char* argv[])
{
    // 请在此输入您的代码
    int data = 0;
    scanf("%d", &data);
    int arr[8] = { 0 };
    int temp = data;
    for (int i = 7; i >= 0; i--)
    {
        arr[i] = temp % 10;
        temp = temp / 10;
    }
    int flag1 = 0, flag2 = 0;

    while ((flag1 == 0 || flag2 == 0) && data < 100000000)
    {
        data++;
        arr[7]++;
        for (int i = 7; arr[i] > 9; i--)
        {
            arr[i] = arr[i] % 10;
            arr[i - 1]++;
        }
        if (flag1 == 0 && ReWord(arr))
        {
            flag1 = data;
        }
        if (flag2 == 0 && AB(arr))
        {
            flag2 = data;
        }
    }
    printf("%d\n%d", flag1, flag2);
    return 0;
}

//int maxDepth(char * s){
//    int max = 0;
//
//    int bracket1 = 0;
//    int bracket2 = 0;
//    int len = strlen(s);
//    int flag1 = 0;
//    for(int i=0;i<len;i++)
//    {
//        if(s[i] == ')')
//        {
//            flag1 = 1;
//            bracket1++;
//        }
//        if(s[i] == '(')
//        {
//            if(flag1==1)
//            {
//                bracket1--;
//                bracket2--;
//                flag1 == 0;
//            }
//            bracket2++;
//        }
//        if(bracket1 == bracket2)
//        {
//            printf("%d %d\n",bracket1,bracket2);
//            if(bracket1 > max)
//                max = bracket1;
//            bracket1 = bracket2 = 0;
//            flag1 = 0;
//        }
//    }
//    return max;
//}

//char* makeGood(char* s) {
//    int len = strlen(s);
//    if (len <= 1)
//        return s;
//
//    for (int i = 0; i < len - 1; i++)
//    {
//        if (s[i] + 32 == s[i + 1] || s[i] - 32 == s[i + 1])
//        {
//            for (int j = i + 2; j <= len; j++)
//            {
//                s[j-2] = s[j];
//            }
//            len = strlen(s);
//            i = -1;
//        }
//    }
//    return s;
//}
//
//int main()
//{
//    char s[] = "leEeetcode";
//    printf("%s", makeGood(s));
//
//	return 0;
//}