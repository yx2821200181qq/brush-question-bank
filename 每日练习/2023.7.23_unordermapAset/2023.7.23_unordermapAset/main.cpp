#define _CRT_SECURE_NO_WARNINGS 1
#include<unordered_map>
#include<unordered_set>
using namespace std;

//961. 在长度 2N 的数组中找出重复 N 次的元素
int repeatedNTimes(vector<int>& nums) {
    unordered_map<int, int> um;
    for (auto it : nums)
        um[it]++;
    size_t N = nums.size() / 2;
    int tar = 0;
    for (auto& it : um)
    {
        if (it.second == N)
        {
            tar = it.first;
            break;
        }
    }
    return tar;
}

