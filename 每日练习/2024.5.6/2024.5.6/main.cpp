#include<iostream>
#include <vector>
#include <set>
using namespace std;

vector<int> decrypt(vector<int>& code, int k) {
    if (k == 0) return vector<int>(code.size(), 0);
    vector<int> tar;
    int left, right, num = 0;
    if (k > 0)
    {
        left = 1;
        right = k;
    }
    else
    {
        left = code.size() + k;
        right = code.size() - 1;
    }
    for (int i = left; i <= right; i++)
    {
        num += code[i];
    }
    for (int i = 0; i < code.size(); i++)
    {
        if (i == 0) tar.push_back(num);
        else
        {
            num -= code[left];
            left++;
            right++;
            if (left == code.size()) left = 0;
            if (right == code.size()) right = 0;
            num += code[right];
            tar.push_back(num);
        }
    }
    return tar;
}

class RandomizedSet {
public:
    RandomizedSet() {
    }

    bool insert(int val) {
        if (s.find(val) != s.end()) return false;
        s.insert(val);
        return true;
    }

    bool remove(int val) {
        if (s.find(val) == s.end())
            return false;
        s.erase(val);
    }

    int getRandom() {
        int a = rand() % s.size();
        set<int>::iterator it = s.begin();
        while (a--)
        {
            it++;
        }
        return *it;
    }
private:
    set<int> s;
};
